package com.cpsserver;

import com.turkcelltech.jac.ASN1Integer;
import com.turkcelltech.jac.OctetString;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.timeout.IdleStateHandler;
import io.netty.util.concurrent.Future;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Server implements Runnable {
	private static final Logger logger = Logger.getLogger(Server.class.getName());

	private int maxClientNum = 10;
	private int port = 8989;
	private int timeout = 20;
	private static final int READ_IDEL_TIME_OUT = 20;
	private static final int WRITE_IDEL_TIME_OUT = 25;
	private static final int ALL_IDEL_TIME_OUT = 30;
	private ChannelFuture f;

	public Server(int port) {
		this.port = port;
	}

	public void run() {
		H2Data.InitDB();
		EventLoopGroup group = new NioEventLoopGroup();
		try {
			ServerBootstrap b = new ServerBootstrap();
			b.group(group).channel(NioServerSocketChannel.class).option(ChannelOption.SO_BACKLOG, 1024)
					.handler(new LoggingHandler(LogLevel.INFO)).localAddress(this.port)
					.childHandler(new ChannelInitializer<SocketChannel>() {
						@Override
						protected void initChannel(SocketChannel ch) throws Exception {
							int MAX_FRAME_LENGTH = 2147483647;
							int LENGTH_FIELD_LENGTH = 4;
							int LENGTH_FIELD_OFFSET = 4;
							int LENGTH_ADJUSTMENT = 0;
							int INITIAL_BYTES_TO_STRIP = 8;

							ch.pipeline()
									.addLast(new ChannelHandler[] { new LengthFieldBasedFrameDecoder(MAX_FRAME_LENGTH,
											LENGTH_FIELD_OFFSET, LENGTH_FIELD_LENGTH, LENGTH_ADJUSTMENT,
											INITIAL_BYTES_TO_STRIP, false) });
							ch.pipeline().addLast(new ChannelHandler[] { new Asn1Decode() });
							ch.pipeline().addLast(new ChannelHandler[] { new Asn1Encode() });
							ch.pipeline().addLast(new ChannelHandler[] { new IdleStateHandler(READ_IDEL_TIME_OUT,
									WRITE_IDEL_TIME_OUT, ALL_IDEL_TIME_OUT, TimeUnit.SECONDS) });
							ch.pipeline().addLast(new ChannelHandler[] { new ServerHandler2() });
							//ch.pipeline().addLast(new ChannelHandler[] { new TimeoutHandler() });
						}
					});
			this.f = b.bind().sync();
			logMessage(Server.class.getName() + "started and listen on 锟斤拷" + this.f.channel().localAddress());
			this.f.channel().closeFuture().sync();
		} catch (Exception e) {
			logMessage("Start Server failed!");
			e.printStackTrace();
			return;
		} finally {
			try {
				group.shutdownGracefully().sync();
				H2Data.CloseDB();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public static void getAllQueMessagesFromDB() throws SQLException {
		// 锟斤拷询
		ResultSet rs = H2Data.executeQuery("SELECT * FROM QueMsg");
		// 锟斤拷锟斤拷锟斤拷锟斤拷锟�
		while (rs.next()) {
			System.out.println(
					rs.getString("id") + "," + rs.getString("tliHdr_version") + "," + rs.getString("tliHdr_priority"));
		}
	}

	public static void getAllNumberInfoFromDB() throws SQLException {
		// 锟斤拷询
		ResultSet rs = H2Data.executeQuery("SELECT * FROM NumberInfo");
		// 锟斤拷锟斤拷锟斤拷锟斤拷锟�
		while (rs.next()) {
			System.out.println(rs.getString("number") + "," + rs.getString("status") + "," + rs.getString("addtime"));
		}
	}
	public static void getAllCRFromDB() throws SQLException {
		// 锟斤拷询
		ResultSet rs = H2Data.executeQuery("SELECT * FROM CustomerRecord");
		// 锟斤拷锟斤拷锟斤拷锟斤拷锟�
		while (rs.next()) {
			System.out.println(rs.getString("NUM") + "," + rs.getString("RO") + ",ED:" + rs.getString("ED")+ "," + rs.getString("SO")
			  + "," + rs.getString("ASTA")+ "," + rs.getString("ANET")+ ",SEFD:" + rs.getString("SEFD")+ ",NEWRO:" + rs.getString("NEWRO")
			  + ",INTERC:" + rs.getString("INTERC"));
		}
	}
	public static void getAllCR8FromDB() throws SQLException {
		// 锟斤拷询
		ResultSet rs = H2Data.executeQuery("SELECT * FROM CRCNT8");
		// 锟斤拷锟斤拷锟斤拷锟斤拷锟�
		while (rs.next()) {
			System.out.println(rs.getString("NUM") + "," + rs.getString("LN") );
		}
	}
	public static void getAllCR9FromDB() throws SQLException {
		// 锟斤拷询
		ResultSet rs = H2Data.executeQuery("SELECT * FROM CRCNT9");
		// 锟斤拷锟斤拷锟斤拷锟斤拷锟�
		while (rs.next()) {
			System.out.println(rs.getString("NUM") + "," + rs.getString("TEL") + "," + rs.getString("LNS"));
		}
	}
	public static void getAllCR11FromDB() throws SQLException {
		// 锟斤拷询
		ResultSet rs = H2Data.executeQuery("SELECT * FROM CRCNT11");
		// 锟斤拷锟斤拷锟斤拷锟斤拷锟�
		while (rs.next()) {
			System.out.println(rs.getString("NUM") + "," + rs.getString("V") );
		}
	}
	public static void getAllCR12FromDB() throws SQLException {
		// 锟斤拷询
		ResultSet rs = H2Data.executeQuery("SELECT * FROM CRCNT12");
		// 锟斤拷锟斤拷锟斤拷锟斤拷锟�
		while (rs.next()) {
			System.out.println(rs.getString("NUM") + "," + rs.getString("TYPE")+ "," + rs.getString("LBL")+ "," + rs.getString("DEF") );
		}
	}
	public static void getCR12CountFromDB() throws SQLException {
		// 锟斤拷询
		String ss = H2Data.ExecuteScalar("SELECT count(*) as cc FROM CRCNT12");
			System.out.println("count"+ss );
	}
	

	public void closeServer() {
		System.out.println("----start close server");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.messageId.setValue("ABCDEFG".getBytes());
		obj.tliHdr.messageCode.setValue(5L);
		obj.tliHdr.destNodeName.setValue("".getBytes());
		obj.tliHdr.srcNodeName.setValue("".getBytes());
		obj.tliHdr.errorCode.setValue(0L);

		obj.dataAppl.setValue("".getBytes());
		try {
			ServerHandler2.channelGroup.writeAndFlush(obj);
			System.out.println("----write all channels asn.1 message:GNT");
			this.f.channel().close().sync();
		} catch (Exception e) {
			System.out.println("----GNT wrong");
			e.printStackTrace();
		}
	}

	public void sendRASI() {
		//Report Application Status Information
		System.out.println("----start close server");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.messageId.setValue("ABCDEFG".getBytes());
		obj.tliHdr.messageCode.setValue(5L);
		obj.tliHdr.destNodeName.setValue("".getBytes());
		obj.tliHdr.srcNodeName.setValue("".getBytes());
		obj.tliHdr.errorCode.setValue(0L);

		String dataAppl = "REPT-ASI:,date,time:::::VERS=\"TA-NWT-001247-090\"";
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());		
		try {
			ServerHandler2.channelGroup.writeAndFlush(obj);
			System.out.println("----write Report Application Status Information");
			this.f.channel().close().sync();
		} catch (Exception e) {
			System.out.println("----Report Application Status Information wrong");
			e.printStackTrace();
		}
	}
	private void logMessage(String msg) {
		if ((msg != null) && (!msg.equals(""))) {
			System.out.println(msg + "\r\n");
			logger.log(Level.INFO, null, msg);
		}
	}

	public static void main(String[] args) throws IOException {
		int port = 8989;
		if (args.length >= 2) {
			for (int i = 0; i < args.length; i++) {
				if (args[i].toLowerCase().equals("-port")) {
					port = Integer.parseInt(args[(i + 1)]);
				}
			}
		}

		Server server = new Server(port);
		new Thread(server, ">>>this thread 0").start();
		System.out.println("----type message to show all messages");
		System.out.println("----type number to show all numbers");
		System.out.println("----type cr to show all Customer Records");
		System.out.println("----type cr8 to show all Customer Record's CNT8");
		System.out.println("----type cr9 to show all Customer Record's CNT9");
		System.out.println("----type test352 to Report Application Status Information");
		System.out.println("----type quit to exit program");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String message = br.readLine();

		while (true) {
			if (message.equals("quit"))
				break;
			else if (message.equals("number")) {
				try {
					getAllNumberInfoFromDB();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (message.equals("cr")) {
				try {
					getAllCRFromDB();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (message.equals("cr8")) {
				try {
					getAllCR8FromDB();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (message.equals("cr9")) {
				try {
					getAllCR9FromDB();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (message.equals("cr11")) {
				try {
					getAllCR11FromDB();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (message.equals("cr12")) {
				try {
					getAllCR12FromDB();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (message.equals("cr12count")) {
				try {
					getCR12CountFromDB();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (message.equals("message")) {
				try {
					getAllQueMessagesFromDB();
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else if (message.equals("test352")) { //Report Application Status Information
				try {
					server.sendRASI();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			message = br.readLine();
		}

		server.closeServer();
		System.exit(0);
	}
}