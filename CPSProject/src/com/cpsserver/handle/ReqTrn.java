package com.cpsserver.handle;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.cpsserver.ErrClass;
import com.cpsserver.H2Data;
import com.cpsserver.YlDcmMsg;
import com.cpsserver.YlUtil;
import com.cpsserver.Function;

import io.netty.channel.ChannelHandlerContext;

public class ReqTrn {
	public void Handle(ChannelHandlerContext ctx, YlDcmMsg obj, String dataAppl, String[] dataList, int count,
			String sNumList) {
		System.out.println(
				"----read asn.1 message from " + new String(obj.tliHdr.destNodeName.getValue()) + ":>>" + dataAppl);
		String[] dataInfo6 = Function.MySplit(dataList[6], ",", "\"");
		String[] datetimeInfo1 = Function.MySplit(dataList[1], ",", "\"");
		String sId = "";
		String sRo = "";
		String sNum = "";
		String sRotrn = "";
		String sQt = "";
		String sDate = "", sTime = "";
		sDate = datetimeInfo1[1];
		sTime = datetimeInfo1[2];
		String sDestNodeName = new String(obj.tliHdr.destNodeName.getValue());
		List<ErrClass> errList = new ArrayList<ErrClass>();
		List<String> numList = new ArrayList<String>();
		ErrClass err = Function.isDatetime(sDate, sTime);
		if (err != null) {
			errList.add(err);
		}

		if (dataList.length > 0 && !dataList[dataList.length - 1].endsWith(";")) {
			ErrClass errClass1 = new ErrClass();
			errClass1.setERR("0001");
			errClass1.setVERR(";REQUIRED");
			errList.add(errClass1);
		}

		// for data6
		for (int j6 = 0; j6 < dataInfo6.length; j6++) {
			String s6 = dataInfo6[j6].trim();
			String[] d6 = Function.MySplit(s6, "=", "\"");
			if (s6.indexOf("ID=") == 0) {
				sId = d6[1];
			} else if (s6.indexOf("RO=") == 0) {
				sRo = d6[1];
			} else if (s6.indexOf("ROTRN=") == 0) {
				sRotrn = d6[1];
			} else if (s6.indexOf("NUM=") == 0) {
				sNum = d6[1].replace(";", "");
				if (!Function.isNumeric(sNum)) {
					ErrClass errNumClass = new ErrClass();
					errNumClass.setERR("1000");
					errNumClass.setVERR("NUM:" + sNum);
					errList.add(errNumClass);
				}
			} else {
				ErrClass errClass = new ErrClass();
				errClass.setERR("03");
				if (d6.length > 1) {
					errClass.setVERR(String.format("%s=%s", d6[0].replace(";", ""), d6[1].replace(";", "")));
				} else {
					errClass.setVERR(s6);
				}
				errList.add(errClass);
			}
		}
		// get qt and num list
		for (int j = 0; j < dataList.length; j++) {
			String ss = dataList[j].trim();
			String[] dataInfo2 = Function.MySplit(ss, "=", "\"");
			if (ss.indexOf("NUM=") == 0) {
				String[] dataInfo3 = Function.MySplit(ss, ",", "\"");
				for (int k = 0; k < dataList.length; k++) {
						String sTemp = dataInfo3[k].trim().replace("NUM=", "");
						numList.add(sTemp);
				}
			}
		}
		//check qt
		if (!sQt.equals("") && Integer.parseInt(sQt)>10) {
			ErrClass errClass2 = new ErrClass();
			errClass2.setERR("05");
			errClass2.setVERR("verr");
			errList.add(errClass2);
		} else if (Integer.valueOf(sQt)==0) {
				ErrClass errClass2 = new ErrClass();
				errClass2.setERR("05");
				errClass2.setVERR("verr");
				errList.add(errClass2);
		} else if (!sQt.equals("") && numList.size()==0) {
			ErrClass errClass2 = new ErrClass();
			errClass2.setERR("02");
			errClass2.setVERR("verr");
			errList.add(errClass2);
		} else if (!sQt.equals(String.valueOf(numList.size()))) {  //check qt and numbers
			ErrClass errClass2 = new ErrClass();
			errClass2.setERR("04");
			errClass2.setVERR("verr");
			errList.add(errClass2);
		}

		// check id
		if (sId.equals("")) {
			ErrClass errClass2 = new ErrClass();
			errClass2.setERR("02");
			errClass2.setVERR("ID:MISSING");
			errList.add(errClass2);
		} else if (H2Data.ExecuteScalar("select count(*) as cc from USERINFO where id='" + sId + "'").equals("0")) {
			ErrClass errClass2 = new ErrClass();
			errClass2.setERR("0101");
			errClass2.setVERR("ID:" + sId);
			errList.add(errClass2);
		}
		
		//a ROTRN tag and NUM tag must not be on the same message
		if (!sRotrn.equals("") && numList.size()>0 ) {
			ErrClass errClass2 = new ErrClass();
			errClass2.setERR("04");
			errClass2.setVERR("verr");
			errList.add(errClass2);
		}

		// error
		if (errList.size() > 0) {
			dataAppl = String.format("RSP-TRN:,%s,%s:::DENIED,01::ID=%s,RO=%s,CRO=cro,NUM=%s",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum});
			dataAppl += ":CNT=" + String.format("%02d", errList.size());
			for (int iList = 0; iList < errList.size(); iList++) {
				ErrClass ec = errList.get(iList);
				dataAppl += ":ERR=" + ec.getERR() + ",VERR=\"" + ec.getVERR() + "\"";
			}
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		}
		
		if (sDestNodeName.equals("test329")) {
			dataAppl = String.format("RSP-TRN:,%s,%s:::DENIED,01::ID=%s,RO=%s:QT=01:ROTRN=rotrn,RONM=ronm,NUM=num:CNT=cnt:ERR=09,VERR=verr;",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum});
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test333")) {
			dataAppl = String.format("RSP-TRN:,%s,%s:::COMPLD,11::ID=%s,RO=%s:QT=09:ROTRN=rotrn,RONM=ronm,NUM=num1,TRN=trn:ROTRN=rotrn,RONM=ronm,NUM=num2,TRN=trn:"
					+ "ROTRN=rotrn,RONM=ronm,NUM=num3,TRN=trn:ROTRN=rotrn,RONM=ronm,NUM=num4,TRN=trn:"
					+ "ROTRN=rotrn,RONM=ronm,NUM=num6,TRN=trn:ROTRN=rotrn,RONM=ronm,NUM=num7,TRN=trn:"
					+ "ROTRN=rotrn,RONM=ronm,NUM=num8,TRN=trn:ROTRN=rotrn,RONM=ronm,NUM=num9,TRN=trn:"
					+ "ROTRN=rotrn,RONM=ronm,NUM=num10,TRN=trn:CNT=02:ERR=08,VERR=num5,ERR=06;",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum});
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test334")) {
			dataAppl = String.format("RSP-TRN:,%s,%s:::COMPLD,11::ID=%s,RO=%s:QT=10:ROTRN=rotrn,RONM=ronm,NUM=num1,TRN=trn:ROTRN=rotrn,RONM=ronm,NUM=num2,TRN=trn:"
					+ "ROTRN=rotrn,RONM=ronm,NUM=num3,TRN=trn:ROTRN=rotrn,RONM=ronm,NUM=num4,TRN=trn:"
					+ "ROTRN=rotrn,RONM=ronm,NUM=num5,TRN=trn:ROTRN=rotrn,RONM=ronm,NUM=num6,TRN=trn:"
					+ "ROTRN=rotrn,RONM=ronm,NUM=num7,TRN=trn:ROTRN=rotrn,RONM=ronm,NUM=num8:"
					+ "ROTRN=rotrn,RONM=ronm,NUM=num9,TRN=trn:ROTRN=rotrn,RONM=ronm,NUM=num10,TRN=trn"
					+ ": CNT=01:ERR=09,VERR=num8;",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum});
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test335")) {
			dataAppl = String.format("RSP-TRN:,%s,%s:::COMPLD,11::ID=%s,RO=%s:QT=09:"
					+ "ROTRN=rotrn,RONM=ronm,NUM=num1,TRN=trn:ROTRN=rotrn,RONM=ronm,NUM=num2,TRN=trn:"
					+ "ROTRN=rotrn,RONM=ronm,NUM=num3,TRN=trn:ROTRN=rotrn,RONM=ronm,NUM=num4,TRN=trn:"
					+ "ROTRN=rotrn,RONM=ronm,NUM=num5,TRN=trn:ROTRN=rotrn,RONM=ronm,NUM=num6,TRN=trn:"
					+ "ROTRN=rotrn,RONM=ronm,NUM=num7,TRN=trn:ROTRN=rotrn,RONM=ronm,NUM=num9,TRN=trn:"
					+ "ROTRN=rotrn,RONM=ronm,NUM=num10:CNT=03:ERR=09,VERR=num8:ERR=08,VERR=num10,ERR=06;",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum});
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test336")) {
			dataAppl = String.format("RSP-TRN:,%s,%s:::DENIED,01::ID=%s,RO=%s,ROTRN=%s=01:ERR=06,VERR=verr;",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sRotrn});
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test337")) {
			dataAppl = String.format("RSP-TRN:,%s,%s:::DENIED,01::ID=%s,RO=%s,ROTRN=%s=01:ERR=07,VERR=verr;",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sRotrn});
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test339")) {
			dataAppl = String.format("RSP-TRN:,%s,%s:::DENIED,01::ID=%s,RO=%s:QT=11:"
					+ "NUM=num1:NUM=num2:NUM=num3:NUM=num4:NUM=num5:"
					+ "NUM=num6:NUM=num7:NUM=num8:NUM=num9:NUM=num10:NUM=num11:"
					+ "CNT=01,ERR=05,VERR=verr;",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sRotrn});
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test340")) {
			dataAppl = String.format("RSP-TRN:,%s,%s:::DENIED,01::ID=%s,RO=%s:QT=01:"
					+ "ROTRN=rotrn,RONM=ronm,NUM=num1:CNT=02,ERR=04,VERR=verr,ERR=04,VERR=verr;",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sRotrn});
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (numList.size()>0 ) {
			sNum =String.format("QT=%02d",numList.size());
			for (int k = 0; k < numList.size(); k++) {
				sNum += String.format(":ROTRN=rotrn,RONM=ronm,NUM=%s,TRN=trn",numList.get(k));
			}
			dataAppl = String.format(
					"RSP-TRN:,%s,%s:::COMPLD,00::ID=%s,RO=%s:%s;",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum});
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else {
			dataAppl = String.format(
					"RSP-TRN:,%s,%s:::COMPLD,00::ID=%s,RO=%s,ROTRN=%s,RONM=ronm,TRN=trn;",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sRotrn});
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		}
	}

}
