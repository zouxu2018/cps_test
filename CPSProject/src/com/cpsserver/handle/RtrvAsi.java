package com.cpsserver.handle;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.cpsserver.ErrClass;
import com.cpsserver.Function;
import com.cpsserver.H2Data;
import com.cpsserver.YlDcmMsg;
import com.cpsserver.YlUtil;

import io.netty.channel.ChannelHandlerContext;

public class RtrvAsi {
	public void Handle(ChannelHandlerContext ctx, YlDcmMsg obj, String dataAppl, String[] dataList, int count,
			String sNumList) {
			dataAppl = String.format("RSP-ASI:,%s,%s:::COMPLD,00::VERS=\"GR-1247-CORE-0300\";",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date())});
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
	}

}
