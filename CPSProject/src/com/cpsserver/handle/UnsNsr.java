package com.cpsserver.handle;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.cpsserver.ErrClass;
import com.cpsserver.H2Data;
import com.cpsserver.YlDcmMsg;
import com.cpsserver.YlUtil;
import com.cpsserver.Function;

import io.netty.channel.ChannelHandlerContext;

public class UnsNsr {
	public void Handle(ChannelHandlerContext ctx, YlDcmMsg obj, String dataAppl, String[] dataList, int count) {
		System.out.println(
				"----read asn.1 message from " + new String(obj.tliHdr.destNodeName.getValue()) + ":>>" + dataAppl);
		String[] dataInfo6 = Function.MySplit(dataList[6], ",", "\"");
		String[] datetimeInfo1 = Function.MySplit(dataList[1], ",", "\"");
		String sMid = "";
		String sRo = "";
		String sNum = "";
		String sDate = "", sTime = "";
		sDate = datetimeInfo1[1];
		sTime = datetimeInfo1[2];
		String sDestNodeName = new String(obj.tliHdr.destNodeName.getValue());
		List<ErrClass> errList = new ArrayList<ErrClass>();
		List<String> numList = new ArrayList<String>();
		ErrClass err = Function.isDatetime(sDate, sTime);
		if (err != null) {
			errList.add(err);
		}

		if (dataList.length > 0 && !dataList[dataList.length - 1].endsWith(";")) {
			ErrClass errClass1 = new ErrClass();
			errClass1.setERR("0001");
			errClass1.setVERR(";REQUIRED");
			errList.add(errClass1);
		}

		// for data6
		for (int j6 = 0; j6 < dataInfo6.length; j6++) {
			String s6 = dataInfo6[j6].trim();
			String[] d6 = Function.MySplit(s6, "=", "\"");
			if (s6.indexOf("MID=") == 0) {
				sMid = d6[1];
			} else if (s6.indexOf("RO=") == 0) {
				sRo = d6[1];
			} else if (s6.indexOf("CNT=") == 0) {
				sNum = d6[1].replace(";", "");
				if (!Function.isNumeric(sNum)) {
					ErrClass errNumClass = new ErrClass();
					errNumClass.setERR("1000");
					errNumClass.setVERR("NUM:" + sNum);
					errList.add(errNumClass);
				}
			} else {
				ErrClass errClass = new ErrClass();
				errClass.setERR("03");
				if (d6.length > 1) {
					errClass.setVERR(String.format("%s=%s", d6[0].replace(";", ""), d6[1].replace(";", "")));
				} else {
					errClass.setVERR(s6);
				}
				errList.add(errClass);
			}
		}
		//check ro
		err = isRo(sNum,sRo);
		if (err!=null) {
			errList.add(err);
		}

		//get data
		String sCr="";
//		String sCr = getCustomerRecord(sNum);
//		if (sCr.equals("")) {
//			err = new ErrClass();
//			err.setERR("15");
//			err.setVERR("NO DAP DATA");
//			errList.add(err);
//		}

		
		// error
		if (errList.size() > 0) {
			return;
		}
		
		//update number status
		// get Num list
		for (int m = 0; m < dataList.length; m++) {
			String s0 = dataList[m];
			String[] dataInfo0 = s0.split("=");
			if (s0.indexOf("CNT=") == 0) {
				count = Integer.parseInt(dataInfo0[1]);
			} else if (s0.indexOf("NUM=") == 0) {
				sNum="";
				String sStat="";
				String sLact="";
				String sErr="";
				String[] values = dataInfo0[1].split(",");
				for (int j = 0; j < values.length; j++) {
					String s1 = values[j];
					String[] value1 = s1.split("=");
					if (s1.indexOf("NUM=") == 0) {
						sNum = value1[1];
					} else if (s1.indexOf("STAT=") == 0) {
						sStat = value1[1];
					} else if (s1.indexOf("LACT=") == 0) {
						sLact = value1[1];
					} else if (s1.indexOf("ERR=") == 0) {
						sErr = value1[1];
					}
				}
				//don't know how to handle sNum,sStat,sLact and sErr
			}
		}
	}

	public static ErrClass isRo(String sNum, String sRo) {
		ErrClass errClass = null;
		if (sNum.equals("") || sRo.equals("")) {
			return errClass;
		} else {
			String ss = H2Data.ExecuteScalar("select RO from CustomerRecord where NUM='" + sNum + "'");
			if (!ss.equals(sRo)) {
				errClass = new ErrClass();
				errClass.setERR("06");
				errClass.setVERR("RO:" + sRo);
			}
		}
		return errClass;
	}
	public static ErrClass isEd(String sEd) {
		ErrClass errClass = null;
		if (sEd.equals("NOW")) {
				errClass = new ErrClass();
				errClass.setERR("11");
				errClass.setVERR("ED=NOW");
		}
		return errClass;
	}
	public static String getCustomerRecord(String sNum,String sEd,String sEt) {
		String sCr = "";
		ResultSet rs = H2Data.executeQuery("SELECT * FROM DAP where NUM='" + sNum + "' and concat(ED,ET)>='"+ sEd + sEt +"'");
		// only one record
		try {
			if (rs.next()) {
				sCr += "STAT=" + getCrStat(rs.getString("STATUS")) + ",APP=app";
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return sCr;
	}
	public static String getCrStat(String sStatus) {
		if (sStatus.equals("ACTIVE")) {
			return "01";
		} else if (sStatus.equals("SENDING")) {
			return "02";
		} else if (sStatus.equals("PENDING")) {
			return "03";
		} else if (sStatus.equals("DISCONNECT")) {
			return "04";
		} else if (sStatus.equals("OLD")) {
			return "05";
		} else if (sStatus.equals("FAILED")) {
			return "06";
		} else {
			return sStatus;
		}
	}


}
