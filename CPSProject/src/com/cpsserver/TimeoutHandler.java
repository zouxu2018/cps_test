package com.cpsserver;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import java.io.PrintStream;

public class TimeoutHandler extends ChannelInboundHandlerAdapter
{
	//this is for client timeout
  public void userEventTriggered(ChannelHandlerContext ctx, Object evt)
    throws Exception
  {
    if ((evt instanceof IdleStateEvent)) {
      IdleStateEvent event = (IdleStateEvent)evt;
      String type = "";
      if (event.state() == IdleState.READER_IDLE) {
        type = "read idle";
//        YlDcmMsg obj = NewGDMessage(false);
//        try {
//			// this.lastSendMessage = ((YlDcmMsg) obj.clone());
//			ctx.write(obj);
//			ctx.flush();
//			System.out.println("----write asn.1 message:GD");
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
      }
      else if (event.state() == IdleState.WRITER_IDLE)
        type = "write idle";
      else if (event.state() == IdleState.ALL_IDLE) {
        type = "all idle";
      }

      System.out.println(ctx.channel().remoteAddress() + " TimeoutHandler message, timeout type:" + type);
    } else {
      super.userEventTriggered(ctx, evt);
    }
  }
}