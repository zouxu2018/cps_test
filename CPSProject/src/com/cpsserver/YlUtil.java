package com.cpsserver;

import java.text.SimpleDateFormat;
import java.util.Date;

public class YlUtil
{
  public static String dateToString(Date time)
  {
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    String ctime = formatter.format(time);
    return ctime;
  }

  public static String timeToString(Date time) {
    SimpleDateFormat formatter = new SimpleDateFormat("HH-mm-ss");
    String ctime = formatter.format(time);
    return ctime + "-CST";
  }
}