package com.cpsserver;

import com.turkcelltech.jac.ASN1Integer;

public class ErrorCode extends ASN1Integer
{
  public static final long mNoError = 0L;
  public static final long mbadSource = 1L;
  public static final long mbadDestination = 2L;
  public static final long mbadVersion = 3L;

  public ErrorCode()
  {
  }

  public ErrorCode(String name)
  {
    setName(name);
  }

  public ErrorCode(long value)
  {
    setValue(value);
  }

  public ErrorCode(String name, long value)
  {
    setValue(value);
    setName(name);
  }

  public void setTo_mNoError_0()
  {
    setValue(0L);
  }

  public void setTo_mbadSource_1() {
    setValue(1L);
  }

  public void setTo_mbadDestination_2() {
    setValue(2L);
  }

  public void setTo_mbadVersion_3() {
    setValue(3L);
  }
}