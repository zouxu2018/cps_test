package com.cpsserver;

public class ErrClass {
	public ErrClass() {
	}

	private String ERR;

	public String getERR() {
		return ERR;
	}

	public void setERR(String eRR) {
		ERR = eRR;
	}

	private String VERR;

	public String getVERR() {
		return VERR;
	}

	public void setVERR(String vERR) {
		VERR = vERR;
	}
}
