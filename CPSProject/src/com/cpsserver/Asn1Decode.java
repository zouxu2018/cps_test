package com.cpsserver;

import com.chaosinmotion.asn1.BerInputStream;
import com.turkcelltech.jac.ASN1Integer;
import com.turkcelltech.jac.OctetString;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;

public class Asn1Decode extends ByteToMessageDecoder
{
  public void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out)
    throws Exception
  {
    try
    {
      ByteArrayOutputStream outStream = new ByteArrayOutputStream();

      while (in.readableBytes() > 0) {
        outStream.write(in.readByte());
      }
      outStream.flush();

      BerInputStream bis = new BerInputStream(new ByteArrayInputStream(outStream.toByteArray()));
      printHex(outStream.toByteArray());
      try {
        YlDcmMsg msg = new YlDcmMsg();
        msg.decode(bis);
        System.out.println("decode asn.1 message");
        printYlDcmMsg(msg);
        out.add(msg);
      } catch (Exception e2) {
        System.out.println("decode error:" + e2.toString());
      }
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
  }

  public static void printHex(byte[] coded) {
    System.out.println("Hex :");
    String hexDigits = "0123456789ABCDEF";
    for (int i = 0; i < coded.length; i++) {
      int c = coded[i];

      if (c < 0)
        c += 256;
      int hex1 = c & 0xF;
      int hex2 = c >> 4;
      System.out.print(hexDigits.substring(hex2, hex2 + 1));
      System.out.print(hexDigits.substring(hex1, hex1 + 1) + " ");
    }
    System.out.println();
  }
  public static void printYlDcmMsg(YlDcmMsg msg) {
    System.out.println("YlDcmMsg=>");
    System.out.println("tliHdr.version=>" + msg.tliHdr.version.toString());
    System.out.println("tliHdr.priority=>" + msg.tliHdr.priority.toString());
    System.out.println("tliHdr.messageId=>" + new String(msg.tliHdr.messageId.getValue()));
    System.out.println("tliHdr.messageCode=>" + msg.tliHdr.messageCode.toString());
    System.out.println("tliHdr.destNodeName=>" + new String(msg.tliHdr.destNodeName.getValue()));
    System.out.println("tliHdr.srcNodeName=>" + new String(msg.tliHdr.srcNodeName.getValue()));
    System.out.println("tliHdr.errorCode=>" + msg.tliHdr.errorCode.toString());
    System.out.println("dataAppl=>" + new String(msg.dataAppl.getValue()));
  }
}