package com.cpsserver;

import com.turkcelltech.jac.OctetString;
import com.turkcelltech.jac.Sequence;
import java.io.PrintStream;

public class YlDcmMsg extends Sequence
  implements Cloneable
{
  public YlTliHdr tliHdr = new YlTliHdr("tliHdr");
  public OctetString dataAppl = new OctetString("dataAppl");

  public YlDcmMsg()
  {
    setUpElements();
  }

  public YlDcmMsg(String name)
  {
    super(name);
    setUpElements();
  }

  protected void setUpElements() {
    super.addElement(this.tliHdr);
    super.addElement(this.dataAppl);
  }

  public Object clone()
  {
    YlDcmMsg o = null;
    try {
      o = (YlDcmMsg)super.clone();
      o.tliHdr = ((YlTliHdr)this.tliHdr.clone());
      o.dataAppl = this.dataAppl;
    } catch (CloneNotSupportedException e) {
      System.out.println(e.toString());
    }
    return o;
  }
}