package com.cpsserver;

import com.chaosinmotion.asn1.BerOutputStream;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import java.io.ByteArrayOutputStream;

public class Asn1Encode extends MessageToByteEncoder<YlDcmMsg>
{
  public void encode(ChannelHandlerContext ctx, YlDcmMsg msg, ByteBuf out)
    throws Exception
  {
    try
    {
      ByteArrayOutputStream outStream = new ByteArrayOutputStream();
      BerOutputStream outBs = new BerOutputStream(outStream);
      msg.encode(outBs);
      out.writeByte(126);
      out.writeByte(126);
      out.writeByte(126);
      out.writeByte(126);
      out.writeInt(outStream.toByteArray().length);
      out.writeBytes(outStream.toByteArray());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}