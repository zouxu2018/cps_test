package com.cpsserver;

import com.cpsserver.handle.ReqDap;
import com.cpsserver.handle.ReqTrn;
import com.turkcelltech.jac.ASN1Integer;
import com.turkcelltech.jac.OctetString;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.util.ReferenceCountUtil;
import io.netty.util.concurrent.GlobalEventExecutor;
import java.io.PrintStream;
import java.net.InetSocketAddress;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.text.SimpleDateFormat;

public class ServerHandler2 extends ChannelInboundHandlerAdapter {
	public static ChannelGroup channelGroup = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

	private static final Logger logger = Logger.getLogger(Server.class.getName());
	private YlDcmMsg lastSendMessage;

	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		this.lastSendMessage = null;
		try {
			YlDcmMsg obj = (YlDcmMsg) msg;

			if (obj.tliHdr.messageCode.getValue() == 1) {
				System.out.println(
						"----read asn.1 message from " + new String(obj.tliHdr.destNodeName.getValue()) + ":GD");
				if (new String(obj.tliHdr.srcNodeName.getValue()).equals("")) {
					obj.tliHdr.srcNodeName.setValue("Server".getBytes());
					ctx.write(obj);
					ctx.flush();
					this.lastSendMessage = ((YlDcmMsg) obj.clone());
					return;
				}
			} else if (obj.tliHdr.messageCode.getValue() == 19) {
				String dataAppl = new String(obj.dataAppl.getValue());
				
				String[] dataList = Function.MySplit(dataAppl, ":", "\"");
				String sNumList = "";
				int count = 1;
				// get Num list
				for (int m = 0; m < dataList.length; m++) {
					String s0 = dataList[m];
					String[] dataInfo0 = s0.split("=");
					if (s0.indexOf("QT=") == 0) {
						count = Integer.parseInt(dataInfo0[1]);
					} else if (s0.indexOf("NUM=") == 0) {
						sNumList = sNumList + dataInfo0[1] + ",";
					}
				}
				if (!sNumList.equals(""))
					sNumList = sNumList.substring(0, sNumList.length() - 1);

				if ((dataList[0].equals("REQ-TEST")) && (dataList.length >= 7)) {
					System.out.println("----read asn.1 message from " + new String(obj.tliHdr.destNodeName.getValue())
							+ ":REQ-TEST");

					Random random = new Random();
					int result = random.nextInt(2);
					if (result == 0) {
						dataAppl = String.format("RSP-TEST:,%s,%s::%s:COMPLD,00::\"%s\"",
								new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()),
										dataList[3], dataList[6] });
						obj.dataAppl.setValue(dataAppl.getBytes());
					} else {
						dataAppl = String.format("RSP-TEST:,%s,%s::%s:DENIED,01::\"%s\"",
								new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()),
										dataList[3], "failed request" });
						obj.dataAppl.setValue(dataAppl.getBytes());
					}
					ctx.writeAndFlush(obj);
//					this.lastSendMessage = ((YlDcmMsg) obj.clone());
					return;
				}
				if ((dataList[0].equals("REQ-TEST")) && (dataList.length < 7)) {
					System.out.println("----read asn.1 message from " + new String(obj.tliHdr.destNodeName.getValue())
							+ ":invalid UAL header REQ-TEST message:" + dataAppl);

					dataAppl = String.format("UNR:,%s,%s::%s:DENIED,01::\"%s\"",
							new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "",
									"invalid header message" });
					obj.dataAppl.setValue(dataAppl.getBytes());
					ctx.writeAndFlush(obj);
					return;
				}
				if (dataList[0].equals("S2S-RSP")) {
					System.out.println("----read asn.1 message from " + new String(obj.tliHdr.destNodeName.getValue())
							+ ":S2S-RSP    s2s message>>" + dataList[6]);
				} else if (dataList[0].equals("TRA")) {
					System.out.println("----read asn.1 message from " + new String(obj.tliHdr.destNodeName.getValue())
							+ ":TRA    TRA message>>" + dataAppl);
				} else if (dataList[0].equals("TSA")) {
					System.out.println("----read asn.1 message from " + new String(obj.tliHdr.destNodeName.getValue())
							+ ":TSA    TSA message>>" + dataAppl);
				} else if (dataList[0].equals("REQ-NSC")) {
					handleNumReqNsc(ctx, obj, dataAppl, dataList, count, sNumList);
				} else if (dataList[0].equals("REQ-CRA")) {
					handleNumReqCra(ctx, obj, dataAppl, dataList, count, sNumList);
				} else if (dataList[0].equals("REQ-CRC")) {
					handleNumReqCrc(ctx, obj, dataAppl, dataList, count, sNumList);
				} else if (dataList[0].equals("REQ-CRQ")) {
					handleNumReqCrq(ctx, obj, dataAppl, dataList, count, sNumList);
				} else if (dataList[0].equals("REQ-CRV")) {
					handleNumReqCrv(ctx, obj, dataAppl, dataList, count, sNumList);
				} else if (dataList[0].equals("REQ-SCP")) {
					handleNumReqScp(ctx, obj, dataAppl, dataList, count, sNumList);
				} else if (dataList[0].equals("REQ-TRN")) {
					ReqTrn reqtrn = new ReqTrn();
					reqtrn.Handle(ctx, obj, dataAppl, dataList, count, sNumList);
				} else if (dataList[0].equals("REQ-DAP")) {
					ReqDap reqdap = new ReqDap();
					reqdap.Handle(ctx, obj, dataAppl, dataList, count, sNumList);
				} else if (dataList[0].equals("REQ-NSR")) {
					System.out.println("----read asn.1 message from " + new String(obj.tliHdr.destNodeName.getValue())
							+ ":NUMBER SEARCH (REQ-NSR)>>" + dataAppl);
					String[] dataInfo = dataList[6].split(",");
					String sId = "";
					String sRo = "";
					String sNpa = "";
					String sNxx = "";
					String sAc = "";
					String sNum = "";
					String sLine = "";
					String sNcon = "";
					String sCtel = "";
					String sDestNodeName = new String(obj.tliHdr.destNodeName.getValue());
					for (int j = 0; j < dataInfo.length; j++) {
						String ss = dataInfo[j];
						String[] dataInfo2 = ss.split("=");
						if (ss.indexOf("ID=") == 0) {
							sId = dataInfo2[1];
						} else if (ss.indexOf("RO=") == 0) {
							sRo = dataInfo2[1];
						} else if (ss.indexOf("NPA=") == 0) {
							sNpa = dataInfo2[1];
						} else if (ss.indexOf("NXX=") == 0) {
							sNxx = dataInfo2[1];
						} else if (ss.indexOf("AC=") == 0) {
							sAc = dataInfo2[1];
						} else if (ss.indexOf("LINE=") == 0) {
							sLine = dataInfo2[1];
						} else if (ss.indexOf("NUM=") == 0) {
							sNum = dataInfo2[1].replace("\"", "");
						} else if (ss.indexOf("NCON=") == 0) {
							sNcon = dataInfo2[1];
						} else if (ss.indexOf("CTEL=") == 0) {
							sCtel = dataInfo2[1];
						} else if (ss.indexOf("QT=") == 0) {
							count = Integer.parseInt(dataInfo2[1].replace(";", ""));
						}
					}
					if (sAc.equals("S")) {
						// simulation error
						if (sNxx.equals("999") && sLine.equals("9995") && count == 10) {
							count = 5;
							String[] Numbers = getSpareNumbers(sNpa, sNxx, sLine, sNum, count);
							String sNumbers = "";
							for (int j = 0; j < Numbers.length; j++) {
								sNumbers = sNumbers + "NUM=" + Numbers[j] + ":";
								H2Data.addNumber(Numbers[j], "spare");
							}
							sNumbers = sNumbers.substring(0, sNumbers.length() - 1);

							dataAppl = String.format("RSP-NSR:,%s,%s:::COMPLD,11::ID=%s,RO=%s,ERR=17:CNT=%02d:%s;",
									new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()),
											sId, sRo, count, sNumbers });
							obj.dataAppl.setValue(dataAppl.getBytes());
							ctx.writeAndFlush(obj);
							return;
						} else if (sDestNodeName.equals("test53")) {
							// test53
							count = 1;
							String[] Numbers = getRSpareNumbers(sNpa, sNxx, sLine, sNumList, count);
							String sNumbers = "";
							for (int j = 0; j < Numbers.length; j++) {
								sNumbers = sNumbers + "NUM=" + Numbers[j] + ":";
								H2Data.addNumber(Numbers[j], "spare");
							}
							sNumbers = sNumbers.substring(0, sNumbers.length() - 1);

							dataAppl = String.format(
									"RSP-NSR:,%s,%s:::DENIED,01::ID=%s,RO=%s:CNT=02:ERR=19,VERR=\"11\": ERR=24,VERR=\"X\";",
									new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()),
											sId, sRo });
							obj.dataAppl.setValue(dataAppl.getBytes());
							ctx.writeAndFlush(obj);
							return;
						} else if (sDestNodeName.equals("test56")) {
							// test56
							dataAppl = String.format("RSP-NSR:,%s,%s:::DENIED,01::ID=%s,RO=%s:CNT=01:ERR=10;",
									new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()),
											sId, sRo });
							obj.dataAppl.setValue(dataAppl.getBytes());
							ctx.writeAndFlush(obj);
							return;
						} else if ((sNum.indexOf("*") >= 0 || sNum.indexOf("&") >= 0) && !sNpa.equals("")) {
							// test72
							dataAppl = String.format("RSP-NSR:,%s,%s:::DENIED,01::ID=%s,RO=%s:CNT=01:ERR=34;",
									new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()),
											sId, sRo });
							obj.dataAppl.setValue(dataAppl.getBytes());
							ctx.writeAndFlush(obj);
							return;
						} else if (stringNumbers(sNum, "&") == 1) {
							// Test Case # 73 Search a Spare Number Using one &
							// in Num
							dataAppl = String.format("RSP-NSR:,%s,%s:::DENIED,01::ID=%s,RO=%s:CNT=01:ERR=35;",
									new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()),
											sId, sRo });
							obj.dataAppl.setValue(dataAppl.getBytes());
							ctx.writeAndFlush(obj);
							return;
						} else {
							// search number
							String[] Numbers = getSpareNumbers(sNpa, sNxx, sLine, sNum, count);
							String sNumbers = "";
							for (int j = 0; j < Numbers.length; j++) {
								sNumbers = sNumbers + "NUM=" + Numbers[j] + ":";
								H2Data.addNumber(Numbers[j], "spare");
							}
							sNumbers = sNumbers.substring(0, sNumbers.length() - 1);

							dataAppl = String.format("RSP-NSR:,%s,%s:::COMPLD,00::ID=%s,RO=%s:CNT=%02d:%s;",
									new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()),
											sId, sRo, count, sNumbers });
							obj.dataAppl.setValue(dataAppl.getBytes());
							ctx.writeAndFlush(obj);
							return;
						}
					} else if (sAc.equals("Q")) {
						if (sNum.replace("\"", "").replace(";", "").equals("1234567890")) {
							// spare number
							dataAppl = String.format("RSP-NSR:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,STAT=\"SPARE \";",
									new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()),
											sId, sRo, sNum });
							obj.dataAppl.setValue(dataAppl.getBytes());
							ctx.writeAndFlush(obj);
							return;
						} else if (sNum.replace("\"", "").replace(";", "").equals("1234567891")) {
							// spare number
							dataAppl = String.format(
									"RSP-NSR:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,DU=du,SE=se,STAT=\"DISCONN\",CRO=cro,NCON=ncon,CTEL=ctel,NOTES=notes;",
									new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()),
											sId, sRo, sNum });
							obj.dataAppl.setValue(dataAppl.getBytes());
							ctx.writeAndFlush(obj);
							return;
						} else if (sNum.replace("\"", "").replace(";", "").equals("1234567892")) {
							// spare number
							dataAppl = String.format(
									"RSP-NSR:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,RU=ru,SE=se,STAT=\"RESERVE\",CRO=cro;",
									new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()),
											sId, sRo, sNum });
							obj.dataAppl.setValue(dataAppl.getBytes());
							ctx.writeAndFlush(obj);
							return;
						} else if (sNum.replace("\"", "").replace(";", "").equals("1234567893")) {
							// spare number
							dataAppl = String.format(
									"RSP-NSR:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,RU=ru,STAT=\"UNAVAIL\";",
									new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()),
											sId, sRo, sNum });
							obj.dataAppl.setValue(dataAppl.getBytes());
							ctx.writeAndFlush(obj);
							return;
						} else if (sNum.replace("\"", "").replace(";", "").equals("1234567894")) {
							// spare number
							dataAppl = String.format("RSP-NSR:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,STAT=\"ASSIGNE\";",
									new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()),
											sId, sRo, sNum });
							obj.dataAppl.setValue(dataAppl.getBytes());
							ctx.writeAndFlush(obj);
							return;
						} else if (sNum.replace("\"", "").replace(";", "").equals("1234567895")) {
							// spare number
							dataAppl = String.format(
									"RSP-NSR:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,SE=se,CRO=cro,STAT=\"ASSIGNE\";",
									new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()),
											sId, sRo, sNum });
							obj.dataAppl.setValue(dataAppl.getBytes());
							ctx.writeAndFlush(obj);
							return;
						} else if (sNum.replace("\"", "").replace(";", "").equals("1234567896")) {
							// spare number
							dataAppl = String.format(
									"RSP-NSR:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,SE=se,CRO=cro,STAT=\"WORKING\";",
									new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()),
											sId, sRo, sNum });
							obj.dataAppl.setValue(dataAppl.getBytes());
							ctx.writeAndFlush(obj);
							return;
						} else if (sNum.replace("\"", "").replace(";", "").equals("1234567897")) {
							// spare number
							dataAppl = String.format(
									"RSP-NSR:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,SE=se,CRO=cro,STAT=\"TRANSIT\";",
									new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()),
											sId, sRo, sNum });
							obj.dataAppl.setValue(dataAppl.getBytes());
							ctx.writeAndFlush(obj);
							return;
						}
					} else if (sAc.equals("R")) {
						// simulation error
						if (sDestNodeName.equals("test51") && count == 10) {
							// test51
							count = 10;
							String[] Numbers = getRSpareNumbers(sNpa, sNxx, sLine, sNumList, count);
							String sNumbers = "";
							for (int j = 0; j < Numbers.length; j++) {
								sNumbers = sNumbers + "NUM=" + Numbers[j] + ":";
								H2Data.addNumber(Numbers[j], "reserved");
							}
							sNumbers = sNumbers.substring(0, sNumbers.length() - 1);

							dataAppl = String.format("RSP-NSR:,%s,%s:::COMPLD,10::ID=%s,RO=%s,ERR=12:CNT=%02d:%s;",
									new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()),
											sId, sRo, count, sNumbers });
							obj.dataAppl.setValue(dataAppl.getBytes());
							ctx.writeAndFlush(obj);
							return;
						} else if (sDestNodeName.equals("test52") && count == 10) {
							// test52
							count = 1;
							String[] Numbers = getRSpareNumbers(sNpa, sNxx, sLine, sNumList, count);
							String sNumbers = "";
							for (int j = 0; j < Numbers.length; j++) {
								sNumbers = sNumbers + "NUM=" + Numbers[j] + ":";
								H2Data.addNumber(Numbers[j], "reserved");
							}
							sNumbers = sNumbers.substring(0, sNumbers.length() - 1);

							dataAppl = String.format("RSP-NSR:,%s,%s:::DENIED,01::ID=%s,RO=%s,%s:CNT=%02d:ERR=12;",
									new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()),
											sId, sRo, sNumbers, count });
							obj.dataAppl.setValue(dataAppl.getBytes());
							ctx.writeAndFlush(obj);
							return;
						} else if (sDestNodeName.equals("test55") && count == 10) {
							// test55,Number Reservation - Warning - Nearing
							// Limi
							count = 10;
							String[] Numbers = getRSpareNumbers(sNpa, sNxx, sLine, sNumList, count);
							String sNumbers = "";
							for (int j = 0; j < Numbers.length; j++) {
								sNumbers = sNumbers + "NUM=" + Numbers[j] + ":";
								H2Data.addNumber(Numbers[j], "reserved");
							}
							sNumbers = sNumbers.substring(0, sNumbers.length() - 1);

							dataAppl = String.format("RSP-NSR:,%s,%s:::COMPLD,10::ID=%s,RO=%s,ERR=31:CNT=%02d:%s;",
									new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()),
											sId, sRo, count, sNumbers });
							obj.dataAppl.setValue(dataAppl.getBytes());
							ctx.writeAndFlush(obj);
							return;
						} else if (sNum.replace("\"", "").equals("8000*00000") && count == 10) {
							count = 5;
							String[] Numbers = getRSpareNumbers(sNpa, sNxx, sLine, sNumList, count);
							String sNumbers = "";
							for (int j = 0; j < Numbers.length; j++) {
								sNumbers = sNumbers + "NUM=" + Numbers[j] + ":";
								H2Data.addNumber(Numbers[j], "reserved");
							}
							sNumbers = sNumbers.substring(0, sNumbers.length() - 1);

							dataAppl = String.format("RSP-NSR:,%s,%s:::COMPLD,11::ID=%s,RO=%s,ERR=30:CNT=%02d:%s;",
									new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()),
											sId, sRo, count, sNumbers });
							obj.dataAppl.setValue(dataAppl.getBytes());
							ctx.writeAndFlush(obj);
							return;
						} else if (sCtel.replace("\"", "").equals("BADNUMBER") && sNxx.equals("BAD")) {
							// Number Reservation - Error Various Fields Invalid
							dataAppl = String.format(
									"RSP-NSR:,%s,%s:::DENIED,01::ID=%s,RO=%s:CNT=06:ERR=19,VERR=\"00\":ERR=24,VERR=\"Z\""
											+ ":ERR=22,VERR=\"\":ERR=09,VERR=\"BADNUMBER\":ERR=23,VERR=\"BAD\":ERR=21,VERR=\"GLOP\";",
									new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()),
											sId, sRo });
							obj.dataAppl.setValue(dataAppl.getBytes());
							ctx.writeAndFlush(obj);
							return;
						} else {
							// search number
							String[] Numbers = getRSpareNumbers(sNpa, sNxx, sLine, sNumList, count);
							String sNumbers = "";
							for (int j = 0; j < Numbers.length; j++) {
								sNumbers = sNumbers + "NUM=" + Numbers[j] + ":";
								H2Data.addNumber(Numbers[j], "reserved");
							}
							sNumbers = sNumbers.substring(0, sNumbers.length() - 1);

							dataAppl = String.format("RSP-NSR:,%s,%s:::COMPLD,00::ID=%s,RO=%s:CNT=%02d:%s;",
									new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()),
											sId, sRo, count, sNumbers });
							obj.dataAppl.setValue(dataAppl.getBytes());
							ctx.writeAndFlush(obj);
							return;
						}
					} else {

					}
				}

			}

			ReferenceCountUtil.release(msg);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static int stringNumbers(String string, String a) {
		int i = 0;
		String[] array = string.split(a);
		if (array != null) {
			i = array.length - 1;
		}
		return i;
	}

	public String[] getRSpareNumbers(String sNpa, String sNxx, String sLine, String sNum, int count) {
		if (!sNum.equals(""))
			return getRSpareNumbers(sNum, count);

		String numbers = "";
		Random rand = new Random();
		for (int j = 0; j < count; j++) {
			String sNpaNew = sNpa;
			String sNxxNew = sNxx;
			String sLineNew = sLine.replace(";", "");
			if (sNpaNew.equals("")) {
				int k = rand.nextInt(899) + 100;
				sNpaNew = String.format("%d", k);
			}
			if (sNxxNew.equals("")) {
				int k = rand.nextInt(899) + 100;
				sNxxNew = String.format("%d", k);
			}

			if (sLineNew.equals("")) {
				int k = rand.nextInt(8999) + 1000;
				numbers = numbers + String.format("%s%s%d,", sNpaNew, sNxxNew, k);
			} else {
				numbers = numbers + String.format("%s%s%s,", sNpaNew, sNxxNew, sLineNew);
			}
		}
		numbers = numbers.substring(0, numbers.length() - 1);
		return numbers.split(",");
	}

	private void handleNumReqNsc(ChannelHandlerContext ctx, YlDcmMsg obj, String dataAppl, String[] dataList, int count,
			String sNumList) {
		System.out.println("----read asn.1 message from " + new String(obj.tliHdr.destNodeName.getValue())
				+ ":NUMBER STATUS CHANGE (REQ-NSC)>>" + dataAppl);
		String[] dataInfo = dataList[6].split(",");
		String sId = "";
		String sRo = "";
		String sNpa = "";
		String sNxx = "";
		String sAc = "";
		String sNum = "";
		String sLine = "";
		String sNcon = "";
		String sCtel = "";
		String sRu = "";
		String sNewRo = "";
		String sNotes = "";
		String sDestNodeName = new String(obj.tliHdr.destNodeName.getValue());
		for (int j = 0; j < dataInfo.length; j++) {
			String ss = dataInfo[j];
			String[] dataInfo2 = ss.split("=");
			if (ss.indexOf("ID=") == 0) {
				sId = dataInfo2[1];
			} else if (ss.indexOf("RO=") == 0) {
				sRo = dataInfo2[1];
			} else if (ss.indexOf("NPA=") == 0) {
				sNpa = dataInfo2[1];
			} else if (ss.indexOf("NXX=") == 0) {
				sNxx = dataInfo2[1];
			} else if (ss.indexOf("AC=") == 0) {
				sAc = dataInfo2[1];
			} else if (ss.indexOf("LINE=") == 0) {
				sLine = dataInfo2[1];
			} else if (ss.indexOf("NUM=") == 0) {
				sNum = dataInfo2[1].replace(";", "");
			} else if (ss.indexOf("NCON=") == 0) {
				sNcon = dataInfo2[1];
			} else if (ss.indexOf("CTEL=") == 0) {
				sCtel = dataInfo2[1].replace(";", "");
			} else if (ss.indexOf("RU=") == 0) {
				sRu = dataInfo2[1];
			} else if (ss.indexOf("NEWRO=") == 0) {
				sNewRo = dataInfo2[1];
			} else if (ss.indexOf("NOTES=") == 0) {
				sNotes = dataInfo2[1].replace(";", "");
			} else if (ss.indexOf("QT=") == 0) {
				count = Integer.parseInt(dataInfo2[1]);
			}
		}
		if (sAc.equals("C")) {
			// simulation Successful
			if (sDestNodeName.equals("test57")) {
				// test57
				dataAppl = String.format(
						"RSP-NSC:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,RU=%s,NEWRO=%s,SE=se,STAT=\"RESERVE\",NCON=%s,CTEL=%s,NOTES=%s;",
						new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
								sRu, sNewRo, sNcon, sCtel, sNotes });
				obj.dataAppl.setValue(dataAppl.getBytes());
				ctx.writeAndFlush(obj);
				return;
			} else if (sDestNodeName.equals("test58")) {
				// test57
				dataAppl = String.format(
						"RSP-NSC:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,RU=%s,NEWRO=%s,SE=se,STAT=\"RESERVE\";",
						new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
								sRu, sNewRo });
				obj.dataAppl.setValue(dataAppl.getBytes());
				ctx.writeAndFlush(obj);
				return;
			} else if (sDestNodeName.equals("test59")) {
				// test57
				dataAppl = String.format(
						"RSP-NSC:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,NCON=%s,CTEL=%s,NOTES=%s,SE=se,STAT=\"RESERVE\";",
						new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
								sNcon, sCtel, sNotes });
				obj.dataAppl.setValue(dataAppl.getBytes());
				ctx.writeAndFlush(obj);
				return;
			} else if (sDestNodeName.equals("test62")) {
				// test62
				dataAppl = String.format("RSP-NSC:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,STAT=stat,NCON=%s,CTEL=%s;",
						new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
								sNcon, sCtel });
				obj.dataAppl.setValue(dataAppl.getBytes());
				ctx.writeAndFlush(obj);
				return;
			} else if (sDestNodeName.equals("test63")) {
				// test62
				dataAppl = String.format("RSP-NSC:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,STAT=stat,NCON=%s,CTEL=%s;",
						new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
								sNcon, sCtel });
				obj.dataAppl.setValue(dataAppl.getBytes());
				ctx.writeAndFlush(obj);
				return;
			} else if (sDestNodeName.equals("test64")) {
				// Notes (NOTES) Change test64
				dataAppl = String.format(
						"RSP-NSC:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,STAT=stat,NCON=%s,CTEL=%s,NOTES=%s;",
						new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
								sNcon, sCtel, sNotes });
				obj.dataAppl.setValue(dataAppl.getBytes());
				ctx.writeAndFlush(obj);
				return;
			} else if (sDestNodeName.equals("test65")) {
				// Notes (NOTES) Change test65
				dataAppl = String.format("RSP-NSC:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s:CNT=01:ERR=18;", new Object[] {
						YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum });
				obj.dataAppl.setValue(dataAppl.getBytes());
				ctx.writeAndFlush(obj);
				return;
			} else {

			}
		} else if (sAc.equals("S")) {
			//
			if (sDestNodeName.equals("test60")) {
				// Spare A Reserved Number test60
				dataAppl = String.format("RSP-NSC:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,STAT=\"SPARE\";",
						new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
								sRu, sNewRo, sNcon, sCtel, sNotes });
				obj.dataAppl.setValue(dataAppl.getBytes());
				ctx.writeAndFlush(obj);
				return;
			} else if (sDestNodeName.equals("test61")) {
				// Spare a transitional DIAL# test61
				dataAppl = String.format("RSP-NSC:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,STAT=\"SPARE\";",
						new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
								sRu, sNewRo, sNcon, sCtel, sNotes });
				obj.dataAppl.setValue(dataAppl.getBytes());
				ctx.writeAndFlush(obj);
				return;
			} else if (sDestNodeName.equals("test67")) {
				// Test Case # 67 Spare A Number - Error - Working Number
				dataAppl = String.format("RSP-NSC:,%s,%s:::DENIED,01::ID=%s,RO=%s,NUM=%s:CNT=01:ERR=16,VERR=\"num\";",
						new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo,
								sNum });
				obj.dataAppl.setValue(dataAppl.getBytes());
				ctx.writeAndFlush(obj);
				return;
			} else if (sDestNodeName.equals("test68")) {
				// Test Case # 68 Spare A Number - Error - Reserved By Another
				// RESP ORG
				dataAppl = String.format("RSP-NSC:,%s,%s:::DENIED,01::ID=%s,RO=%s,NUM=%s:CNT=01:ERR=12,VERR=%s;",
						new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
								sRo });
				obj.dataAppl.setValue(dataAppl.getBytes());
				ctx.writeAndFlush(obj);
				return;
			} else if (sDestNodeName.equals("test69")) {
				// Test Case # 69 Spare A Number - Error - Transitional, Other
				// RESP ORG
				dataAppl = String.format("RSP-NSC:,%s,%s:::DENIED,01::ID=%s,RO=%s,NUM=%s:CNT=01: ERR=12,VERR=BANJ1;",
						new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo,
								sNum });
				obj.dataAppl.setValue(dataAppl.getBytes());
				ctx.writeAndFlush(obj);
				return;
			} else if (!sNum.equals("")
					&& (!sNum.subSequence(0, 1).equals("\"") || sNum.subSequence(sNum.length() - 1, 1).equals("\""))) {
				dataAppl = String.format(
						"RSP-NSC:,%s,%s:::DENIED,01::ID=%s,RO=%s,NUM=%s:CNT=01:ERR=05,VERR=\"ABCDEFG\";", new Object[] {
								YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum });
				obj.dataAppl.setValue(dataAppl.getBytes());
				ctx.writeAndFlush(obj);
				return;
			} else {

			}
		}
	}

	private void handleNumReqCra(ChannelHandlerContext ctx, YlDcmMsg obj, String dataAppl, String[] dataList, int count,
			String sNumList) {
		System.out.println("----read asn.1 message from " + new String(obj.tliHdr.destNodeName.getValue())
				+ ":UPDATE CUSTOMER RECORD (REQ-CRA)>>" + dataAppl);
		String[] dataInfo = dataList[6].split(",");
		String sId = "";
		String sRo = "";
		String sNpa = "";
		String sNxx = "";
		String sAc = "";
		String sNum = "";
		String sLine = "";
		String sNcon = "";
		String sCtel = "";
		String sRu = "";
		String sNewRo = "";
		String sNotes = "";
		String sEd = "";
		String sEt = "";
		String sDestNodeName = new String(obj.tliHdr.destNodeName.getValue());
		for (int j = 0; j < dataInfo.length; j++) {
			String ss = dataInfo[j];
			String[] dataInfo2 = ss.split("=");
			if (ss.indexOf("ID=") == 0) {
				sId = dataInfo2[1];
			} else if (ss.indexOf("RO=") == 0) {
				sRo = dataInfo2[1];
			} else if (ss.indexOf("NPA=") == 0) {
				sNpa = dataInfo2[1];
			} else if (ss.indexOf("NXX=") == 0) {
				sNxx = dataInfo2[1];
			} else if (ss.indexOf("AC=") == 0) {
				sAc = dataInfo2[1];
			} else if (ss.indexOf("LINE=") == 0) {
				sLine = dataInfo2[1];
			} else if (ss.indexOf("NUM=") == 0) {
				sNum = dataInfo2[1].replace(";", "");
			} else if (ss.indexOf("NCON=") == 0) {
				sNcon = dataInfo2[1];
			} else if (ss.indexOf("CTEL=") == 0) {
				sCtel = dataInfo2[1].replace(";", "");
			} else if (ss.indexOf("ED=") == 0) {
				sEd = dataInfo2[1].replace(";", "");
			} else if (ss.indexOf("ET=") == 0) {
				sEt = dataInfo2[1].replace(";", "");
			} else if (ss.indexOf("RU=") == 0) {
				sRu = dataInfo2[1];
			} else if (ss.indexOf("NEWRO=") == 0) {
				sNewRo = dataInfo2[1];
			} else if (ss.indexOf("NOTES=") == 0) {
				sNotes = dataInfo2[1].replace(";", "");
			} else if (ss.indexOf("QT=") == 0) {
				count = Integer.parseInt(dataInfo2[1]);
			}
		}
		if (sDestNodeName.equals("test94")) {
			// test94
			dataAppl = String.format("RSP-CRA:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test95")) {
			// test95
			dataAppl = String.format("RSP-CRA:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test96")) {
			// test96
			dataAppl = String.format("RSP-CRA:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test97")) {
			// test97
			dataAppl = String.format("RSP-CRA:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test98")) {
			// test98
			dataAppl = String.format("RSP-CRA:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test99")) {
			// test99
			dataAppl = String.format("RSP-CRA:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test100")) {
			// test100
			dataAppl = String.format("RSP-CRA:,%s,%s:::DENIED,01::ID=%s,RO=%s:CNT=01:ERR=72;",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test101")) {
			// test101
			dataAppl = String.format("RSP-CRA:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test102")) {
			// test101
			dataAppl = String.format("RSP-CRA:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test103")) {
			// test100
			dataAppl = String.format("RSP-CRA:,%s,%s:::DENIED,01::ID=%s,RO=%s:CNT=01:ERR=61;",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test104")) {
			// test104
			dataAppl = String.format("RSP-CRA:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test105")) {
			// test105
			dataAppl = String.format("RSP-CRA:,%s,%s:::DENIED,01::ID=%s,RO=%s:CNT=01:ERR=61;",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test106")) {
			// test106
			dataAppl = String.format("RSP-CRA:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test107")) {
			// test107
			dataAppl = String.format("RSP-CRA:,%s,%s:::DENIED,01::ID=%s,RO=%s:CNT=01:ERR=10;",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test108")) {
			// test108
			dataAppl = String.format("RSP-CRA:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test109")) {
			// test109
			dataAppl = String.format("RSP-CRA:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test110")) {
			// test110
			dataAppl = String.format("RSP-CRA:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test111")) {
			// test111
			dataAppl = String.format("RSP-CRA:,%s,%s:::DENIED,01::ID=%s,RO=%s:CNT=01:ERR=91;",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test112")) {
			// test112
			dataAppl = String.format("RSP-CRA:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test113")) {
			// test113
			dataAppl = String.format("RSP-CRA:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test114")) {
			// test114
			dataAppl = String.format("RSP-CRA:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test115")) {
			// test115
			dataAppl = String.format("RSP-CRA:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test116")) {
			// test116
			dataAppl = String.format("RSP-CRA:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test117")) {
			// test117
			dataAppl = String.format("RSP-CRA:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test118")) {
			// test118
			dataAppl = String.format("RSP-CRA:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test119")) {
			// test119
			dataAppl = String.format("RSP-CRA:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test120")) {
			// test120
			dataAppl = String.format(
					"RSP-CRA:,%s,%s:::::RO=%s,ORIGRO=origro,MID=mid,NUM=%s,ED=%s,ET=%s,STAT=3,CRMSGSIZE=crmsgsize:CNT=cnt:SCP=scp,RES=01,DT=dt;",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sRo, sNum, sEd,
							sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test121")) {
			// test121
			dataAppl = String.format("RSP-CRA:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test122")) {
			// test122
			dataAppl = String.format("RSP-CRA:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test123")) {
			// test123
			dataAppl = String.format("RSP-CRA:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test124")) {
			// test124
			dataAppl = String.format("RSP-CRA:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test125")) {
			// test125
			dataAppl = String.format("RSP-CRA:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test126")) {
			// test125
			dataAppl = String.format("RSP-CRA:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test127")) {
			// test125
			dataAppl = String.format("RSP-CRA:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test128")) {
			// test128
			dataAppl = String.format("RSP-CRA:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test129")) {
			// test129
			dataAppl = String.format("RSP-CRA:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test130")) {
			// test130
			dataAppl = String.format("RSP-CRA:,%s,%s:::::COMPLD,11::ID=%s,RO=%s,NUM=%s,ED=%s, ET=%s:CNTA=cnta:ERR1=38;",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
							sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test131")) {
			// test131
			dataAppl = String.format("RSP-CRA:,%s,%s:::::COMPLD,11::ID=%s,RO=%s,NUM=%s,ED=%s, ET=%s:CNTA=cnta:ERR1=38;",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
							sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test132")) {
			// test132
			dataAppl = String.format("RSP-CRA:,%s,%s:::::COMPLD,11::ID=%s,RO=%s,NUM=%s,ED=%s, ET=%s:CNTA=cnta:ERR1=38;",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
							sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test133")) {
			// test133
			dataAppl = String.format("RSP-CRA:,%s,%s:::::DENIED,10::ID=%s,RO=%s,NUM=%s,ED=%s, ET=%s:CNTA=cnta:ERR1=38;",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
							sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test134")) {
			// test134
			dataAppl = String.format("RSP-CRA:,%s,%s:::::DENIED,10::ID=%s,RO=%s,NUM=%s,ED=%s, ET=%s:CNTA=cnta:ERR1=38;",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
							sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test135")) {
			// test135
			dataAppl = String.format(
					"RSP-CRA:,%s,%s:::::DENIED,10::ID=%s,RO=%s,NUM=%s,ED=%s, ET=%s:"
							+ "CNT=cnt:ERR=99,VERR=\"RO CHANGE NOW\";",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
							sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test136")) {
			// test136
			dataAppl = String.format("RSP-CRA:,%s,%s:::::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test137")) {
			// test137
			dataAppl = String.format("RSP-CRA:,%s,%s:::::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test138")) {
			// test138
			dataAppl = String.format("RSP-CRA:,%s,%s:::::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test139")) {
			// test139
			dataAppl = String.format("RSP-CRA:,%s,%s:::::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test140")) {
			// test140
			dataAppl = String.format("RSP-CRA:,%s,%s:::::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test141")) {
			// test141
			dataAppl = String.format("RSP-CRA:,%s,%s:::::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test142")) {
			// test142
			dataAppl = String.format("RSP-CRA:,%s,%s:::::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test143")) {
			// test143
			dataAppl = String.format("RSP-CRA:,%s,%s:::::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test144")) {
			// test144
			dataAppl = String.format("RSP-CRA:,%s,%s:::::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test145")) {
			// test145
			dataAppl = String.format("RSP-CRA:,%s,%s:::::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test146")) {
			// test146
			dataAppl = String.format("RSP-CRA:,%s,%s:::::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test147")) {
			// test147
			dataAppl = String.format("RSP-CRA:,%s,%s:::::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test148")) {
			// test148
			dataAppl = String.format("RSP-CRA:,%s,%s:::::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test149")) {
			// test149
			dataAppl = String.format("RSP-CRA:,%s,%s:::::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test150")) {
			// test150
			dataAppl = String.format("RSP-CRA:,%s,%s:::::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test151")) {
			// test151
			dataAppl = String.format(
					"RSP-CRA:,%s,%s:::::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s:CNT=01:ERR=99,VERR=verr;",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
							sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test152")) {
			// test152
			dataAppl = String.format("RSP-CRA:,%s,%s:::::COMPLD,00::ID=%s,RO=%s,NUM=%s, ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test153")) {
			// test153
			dataAppl = String.format(
					"RSP-CRA:,%s,%s:::::DENIED,10::ID=%s,RO=%s,NUM=%s, ED=%s,ET=%s:CNT=01:ERR=36,VERR=\"NEEDS INTERC\";",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
							sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test154")) {
			// test154
			dataAppl = String.format(
					"RSP-CRA:,%s,%s:::::DENIED,10::ID=%s,RO=%s,NUM=%s, ED=%s,ET=%s:CNT=01:ERR=04,VERR=\"SO:so\";",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
							sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test155")) {
			// test155
			dataAppl = String.format(
					"RSP-CRA:,%s,%s:::::DENIED,10::ID=%s,RO=%s,NUM=%s, ED=%s,ET=%s:CNT=01:ERR=41,VERR=\"NUMBER IS SPARE\";",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
							sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test156")) {
			// test156
			dataAppl = String.format(
					"RSP-CRA:,%s,%s:::::DENIED,01::ID=%s,RO=%s,NUM=%s, ED=%s,ET=%s:CNT=10:ERR=05,VERR=\"ED:01/02/96\": ERR=05,VERR=\"ET:10:30A/C\":"
							+ "ERR=05,VERR=\"SF:232344\": ERR=05,VERR=\"INTERC:ATX\": ERR=05,VERR=\"INTRAC:OTC\":ERR=05,VERR=\"CU:NNNNNNNNN\": ERR=05,VERR=\"RAO:123\":"
							+ "ERR=05,VERR=\"ABN:312345678903\": ERR=05,VERR=\"DD:02/02/96\": ERR=05,VERR=\"HDD:N\";",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
							sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test157")) {
			// test157
			dataAppl = String.format(
					"RSP-CRA:,%s,%s:::::DENIED,01::ID=%s,RO=%s,NUM=%s, ED=%s,ET=%s::CNT=05:ERR=08,VERR=\"RO:BANJ1\": ERR=09,"
							+ "VERR=\"AC:W\":ERR=05,VERR=\"NUM:XZZZZ35\":ERR=05,VERR=\"ED:11=01/96\":ERR=05,VERR=\"ET:NOW\";",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
							sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test158")) {
			// test158
			dataAppl = String.format(
					"RSP-CRA:,%s,%s:::::DENIED,01::ID=%s,RO=%s,NUM=%s, ED=%s,ET=%s:CNT=09:ERR=34,VERR=\"NO OTC FOR INTER\": "
							+ "ERR=99,VERR=\"NO OTX FOR INTRA\":ERR=05,VERR=\"CU:W\":ERR=05,VERR=\"BILL:W\":"
							+ "ERR=05,VERR=\"DAU:Q\":ERR=05,VERR=\"DAT:R:ERR=05,VERR=\"DD:13/01/96\":ERR=05,VERR=\"HDD:W\":ERR=05,VERR=\"LI:WW\";",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
							sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test159")) {
			// test159
			dataAppl = String.format(
					"RSP-CRA:,%s,%s:::::DENIED,01::ID=id,RO=ro,NUM=\"6723938\", ED=\"11/01/96\", ET=\"10:30A/C\":CNT=03:ERR=74,"
							+ "VERR=\"415/9999999999\": ERR=49,VERR=\"BAD DAU:Y/LI:BL\":ERR=44,VERR=\"TEL:8006723939\";",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
							sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test160")) {
			// test160
			dataAppl = String.format(
					"RSP-CRA:,%s,%s:::::DENIED,01::ID=%s,RO=%s,NUM=%s, ED=%s, ET=%s:CNT=01:ERR=05,VERR=\"NUM:ABCDEFG,ED=\"\";",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
							sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test161")) {
			// test161
			dataAppl = String.format(
					"RSP-CRA:,%s,%s:::::DENIED,01::ID=%s,RO=%s,NUM=%s, ED=%s, ET=%s:CNT=02:ERR=05,VERR=\"ALAT:AL\":ERR=05,VERR=\"ALAT:AT\";",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
							sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test162")) {
			// test162
			dataAppl = String.format(
					"RSP-CRA:,%s,%s:::::DENIED,01::ID=%s,RO=%s,NUM=%s, ED=%s, ET=%s:CNT=01:ERR=69,VERR=\"NO REF/EINT REQ\";",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
							sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test164")) {
			// test164
			dataAppl = String.format(
					"RSP-CRA:,%s,%s:::::DENIED,01::ID=%s,RO=%s,NUM=%s:CNT=01:ERR=57,VERR=\"NEEDS ED/ET\";",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test165")) {
			// test165
			dataAppl = "RSP-CRA:,date,time:::DENIED,01::ID=MGIXXX01,RO=MGI01,NUM=\"DIAL#\", ED=\"effective date\","
					+ "ET=\"effective time\":CNT=1:ERR=86, VERR=\"ALBL:NOT ALLOWED\";";
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test166")) {
			// test166
			dataAppl = String.format(
					"RSP-CRA:,%s,%s:::DENIED,01::ID=%s,RO=%s,NUM=%s, ED=%s,ET=%s:CNT=001:ERR=99,VERR=\"NO SPLIT CARRIER\";",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
							sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test167")) {
			// test167
			dataAppl = String.format(
					"RSP-CRA:,%s,%s:::DENIED,01::ID=%s,RO=%s,NUM=%s, ED=%s,ET=%s:CNT=001:ERR=99,VERR=\"NO POTS ALLOWED\";",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
							sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else {

		}
	}

	private void handleNumReqCrc(ChannelHandlerContext ctx, YlDcmMsg obj, String dataAppl, String[] dataList, int count,
			String sNumList) {
		System.out.println("----read asn.1 message from " + new String(obj.tliHdr.destNodeName.getValue())
				+ ":UPDATE CUSTOMER RECORD (REQ-CRC)>>" + dataAppl);
		String[] dataInfo6 = Function.MySplit(dataList[6], ",", "\"");
		String sId = "";
		String sRo = "";
		String sNpa = "";
		String sNxx = "";
		String sAc = "";
		String sNum = "";
		String sLine = "";
		String sNcon = "";
		String sCtel = "";
		String sRu = "";
		String sNewRo = "";
		String sNotes = "";
		String sEd = "";
		String sEt = "";
		String sCu = "";
		String sIec = "", sIac = "", sAbn = "", sDau = "", sDat = "", sDd = "", sHdd = "", sLi = "", sRao = "",
				sSf = "", sAgent = "", sCus = "", sLa = "", sCbi = "", sTelco = "", sAlbl = "", sAac = "", sAlat = "",
				sAnet = "", sAsta = "", sSo = "", sNote = "", sLn = "", sCnt9Data = "", sSefd = "", sInterc = "",
				sRefer = "", sEint = "", sDcsn = "", sPec = "", sPac = "", sNode = "", sCnt11Data = "";
		int iCnt11RealCount = 0;
		String sDestNodeName = new String(obj.tliHdr.destNodeName.getValue());
		List<ErrClass> errList = new ArrayList<ErrClass>();
		List<Cnt12Class> cnt12List = new ArrayList<Cnt12Class>();
		List<String> cnt13List = new ArrayList<String>();
		if (dataList.length > 0 && !dataList[dataList.length - 1].endsWith(";")) {
			ErrClass errClass1 = new ErrClass();
			errClass1.setERR("0001");
			errClass1.setVERR(";REQUIRED");
			errList.add(errClass1);
		}

		// for :
		int iIacCount = 0;
		for (int j = 0; j < dataList.length; j++) {
			String ss = dataList[j].trim();
			String[] dataInfo2 = Function.MySplit(ss, "=", "\"");
			if (ss.indexOf("IEC=") == 0) {
				sIec = dataInfo2[1].replace(";", "");
			} else if (ss.indexOf("IAC=") == 0) {
				iIacCount++;
				sIac = dataInfo2[1].replace(";", "");
				if (iIacCount > 1) {
					String[] sIacList = sIac.split(",");
					ErrClass errClass = new ErrClass();
					errClass.setERR("0029");
					errClass.setVERR("IAC:" + sIacList[1].replace("\"", ""));
					errList.add(errClass);
				}
			} else if (ss.indexOf("ALBL=") == 0) {
				sAlbl = dataInfo2[1].replace(";", "");
				// check exist or not
				String[] data_albl = sAlbl.split(",");
				for (int jj = 1; jj < data_albl.length; jj++) {
					String s2 = data_albl[jj].trim();
					ErrClass alblError = isExistedAlbl(s2, sNum);
					if (alblError != null) {
						errList.add(alblError);
						break;
					}
				}
			} else if (ss.indexOf("PAC=") == 0) {
				sPac = dataInfo2[1].replace(";", "");
			} else if (ss.indexOf("NODE=") == 0) {
				sNode = dataInfo2[1].replace(";", "");
				ErrClass nodeError = isNode(sNode);
				if (nodeError != null) {
					errList.add(nodeError);
				}
			} else if (ss.indexOf("PEC=") == 0) {
				sPec = dataInfo2[1].replace(";", "");
			} else if (ss.indexOf("UNREC") >= 0) {
				String[] sTempList2 = ss.split("UNREC=");
				ErrClass errClass = new ErrClass();
				errClass.setERR("0003");
				errClass.setVERR("UNREC:" + sTempList2[1]);
				errList.add(errClass);
			} else if (ss.indexOf("AAC=") == 0) {
				sAac = dataInfo2[1].replace(";", "");
				if (sAac.indexOf("CNT4") < 0) {
					String[] sAacList = sAac.split(",");
					ErrClass errClass = new ErrClass();
					errClass.setERR("0030");
					errClass.setVERR("AAC:" + sAacList[0]);
					errList.add(errClass);
				}

			} else if (ss.indexOf("ALAT=") == 0) {
				sAlat = dataInfo2[1].replace(";", "");
			} else if (ss.indexOf("ANET=") == 0) {
				sAnet = dataInfo2[1];
			} else if (ss.indexOf("ASTA=") == 0) {
				sAsta = dataInfo2[1];
			} else if (ss.indexOf("ABN=") == 0) {
				String[] data_abn = ss.split(",");
				for (int jj = 0; jj < data_abn.length; jj++) {
					String s2 = data_abn[jj].trim();
					String[] dataInfo3 = s2.split("=");
					if (s2.indexOf("ABN=") == 0) {
						sAbn = dataInfo3[1].replace(";", "");
					} else if (s2.indexOf("DAU=") == 0) {
						sDau = dataInfo3[1].replace(";", "");
					} else if (s2.indexOf("DAT=") == 0) {
						sDat = dataInfo3[1].replace(";", "");
					} else if (s2.indexOf("DD=") == 0) {
						sDd = dataInfo3[1].replace(";", "");
					} else if (s2.indexOf("HDD=") == 0) {
						sHdd = dataInfo3[1].replace(";", "");
						if (!isHdd(sHdd)) {
							ErrClass errClass = new ErrClass();
							errClass.setERR("1016");
							errClass.setVERR("HDD:" + sHdd);
							errList.add(errClass);
						}
					} else if (s2.indexOf("LI=") == 0) {
						sLi = dataInfo3[1].replace(";", "");
						if (!isLi(sLi)) {
							ErrClass errClass = new ErrClass();
							errClass.setERR("1018");
							errClass.setVERR("LI:" + sLi);
							errList.add(errClass);
						}
					} else if (s2.indexOf("RAO=") == 0) {
						sRao = dataInfo3[1].replace(";", "");
					} else if (s2.indexOf("SF=") == 0) {
						sSf = dataInfo3[1].replace(";", "");
						if (!isSf(sSf)) {
							ErrClass errClass = new ErrClass();
							errClass.setERR("1023");
							errClass.setVERR("SF:" + sSf);
							errList.add(errClass);
						}
					} else if (s2.indexOf("SO=") == 0) {
						sSo = dataInfo3[1].replace(";", "");
						if (sSo.length() >= 13) {
							String[] sSoList = sSo.split(",");
							ErrClass errClass = new ErrClass();
							errClass.setERR("1022");
							errClass.setVERR("SO:" + sSoList[1]);
							errList.add(errClass);
						}

					} else if (s2.indexOf("NEWRO=") == 0) {
						sNewRo = dataInfo3[1];
					} else if (s2.indexOf("NOTE=") == 0) {
						sNote = dataInfo3[1].replace(";", "");
					} else if (s2.indexOf("AGENT=") == 0) {
						sAgent = dataInfo3[1].replace(";", "");
					} else if (s2.indexOf("CUS=") == 0) {
						sCus = dataInfo3[1].replace(";", "");
					} else if (s2.indexOf("LA=") == 0) {
						sLa = dataInfo3[1].replace(";", "");
					} else if (s2.indexOf("CBI=") == 0) {
						sCbi = dataInfo3[1].replace(";", "");
					} else if (s2.indexOf("TELCO=") == 0) {
						sTelco = dataInfo3[1].replace(";", "");
					} else if (s2.indexOf("NCON=") == 0) {
						sNcon = dataInfo3[1];
					} else if (s2.indexOf("CTEL=") == 0) {
						sCtel = dataInfo3[1].replace(";", "");
					}
				}
			} else if (ss.indexOf("CNT8=") == 0) {
				String sCnt8 = dataInfo2[1].replace(";", "");
				int iCnt8Count = Integer.parseInt(sCnt8);
				sLn = "";
				for (int j8 = 0; j8 < iCnt8Count; j8++) {
					String s8 = dataList[j + 1 + j8].trim();
					String[] d8 = s8.split("=");
					if (!isLn(d8[1].replace(";", ""))) {
						ErrClass errClass = new ErrClass();
						errClass.setERR("1042");
						errClass.setVERR("LN:" + d8[1].replace(";", ""));
						errList.add(errClass);
					} else {
						sLn = sLn + d8[1].replace(";", "") + ",";
					}
				}
				if (!sLn.equals("")) {
					sLn = sLn.substring(0, sLn.length() - 1);
				}
			} else if (ss.indexOf("CNT11=") == 0) {
				String sCnt11 = dataInfo2[1].replace(";", "");
				int iCnt11Count = Integer.parseInt(sCnt11);
				sCnt11Data = "";
				iCnt11RealCount = 0;
				List<String> cnt11V1st = new ArrayList<String>();
				for (int j11 = 0; j11 < iCnt11Count; j11++) {
					String s11 = dataList[j + 1 + j11].trim();
					String[] d11 = s11.split("=");
					if (d11[0].equals("V")) {
						sCnt11Data = sCnt11Data + d11[1].replace(";", "") + ":";
						ErrClass cnt11vErr = isCnt11V(d11[1].replace("\"", "").replace(";", ""), cnt11V1st, sAc, sNum);
						if (cnt11vErr != null) {
							errList.add(cnt11vErr);
						} else {
							String[] list11 = d11[1].replace("\"", "").replace(";", "").split(",");
							s11 = list11[0] + "," + list11[1];
							cnt11V1st.add(s11);
						}
						iCnt11RealCount++;
					} else {
						break;
					}
				}
				if (!sCnt11Data.equals("")) {
					sCnt11Data = sCnt11Data.substring(0, sCnt11Data.length() - 1);
				}
				// add error
				if (iCnt11RealCount != iCnt11Count) {
					ErrClass errClass = new ErrClass();
					errClass.setERR("0011");
					errClass.setVERR(String.format("CNT11:%02d/V:%02d", iCnt11Count, iCnt11RealCount));
					errList.add(errClass);
				}

				ErrClass cnt11All = isCnt11All(cnt11V1st);
				if (cnt11All != null) {
					errList.add(cnt11All);
				}
			} else if (ss.indexOf("CNT9=") == 0) {
				String sCnt9 = dataInfo2[1].replace(";", "");
				int iCnt9Count = Integer.parseInt(sCnt9);
				sCnt9Data = "";
				for (int j9 = 0; j9 < iCnt9Count; j9++) {
					sCnt9Data = sCnt9Data + dataList[j + 1 + j9].trim() + ":";
				}
				sCnt9Data = sCnt9Data.substring(0, sCnt9Data.length() - 1);
			} else if (ss.indexOf("CNT12=") == 0) {
				String sCnt12 = dataInfo2[1].replace(";", "");
				int iCnt12Count = Integer.parseInt(sCnt12);

				String sType = "", sLbl = "", sDef = "", sSort = "";
				if (iCnt12Count < 1000) {
					for (int j12 = 0; j12 < iCnt12Count; j12++) {
						if (dataList[j + 1 + j12].trim().indexOf("TYPE=") < 0) {
							ErrClass erCnt12 = new ErrClass();
							erCnt12.setERR("1121");
							erCnt12.setVERR("TYPE MISSING");
							errList.add(erCnt12);
							break;
						}
						if (dataList[j + 1 + j12].trim().indexOf("CNT12=") > 0) {
							ErrClass erCnt12 = new ErrClass();
							erCnt12.setERR("0030");
							erCnt12.setVERR("TYPE:CNT12");
							errList.add(erCnt12);
							break;
						}

						String sTemp = dataList[j + 1 + j12].trim();
						String[] d12 = Function.MySplit(sTemp, ",", "\"");
						for (int k = 0; k < d12.length; k++) {
							String[] dataInfo12 = d12[k].split("=");
							if (d12[k].indexOf("TYPE=") == 0) {
								sType = dataInfo12[1].replace(";", "");
							} else if (d12[k].indexOf("LBL") == 0) {
								sLbl = dataInfo12[1].replace(";", "");
							} else if (d12[k].indexOf("SORT") == 0) {
								sSort = dataInfo12[1].replace(";", "");
							} else if (d12[k].indexOf("DEF=") == 0) {
								// many def
								String[] sDefList = d12[k].split("DEF=");
								for (int m = 0; m < sDefList.length; m++) {
									sDef = sDefList[m].replace("\"", "").replace(";", "");
									if (!sDef.equals("")) {
										// check and add into list
										ErrClass errCnt12 = isCnt12Type(sType);
										if (errCnt12 != null) {
											errList.add(errCnt12);
											break;
										}
										errCnt12 = isCnt12Lbl(sLbl);
										if (errCnt12 != null) {
											errList.add(errCnt12);
											break;
										}
										errCnt12 = isCnt12Def(sType, sDef);
										if (errCnt12 != null) {
											errList.add(errCnt12);
											break;
										}

										// check cnt13
										if (sType.equals("SD")) {
											String[] data13List = sDef.split(",");
											for (int l = 1; l < data13List.length; l++) {
												errCnt12 = isCnt13(sType, data13List[l], cnt13List);
												if (errCnt12 != null) {
													errList.add(errCnt12);
													break;
												} else {
													cnt13List.add(data13List[l]);
												}
											}
										}

										Cnt12Class data12 = new Cnt12Class();
										data12.setTYPE(sType);
										data12.setLBL(sLbl);
										data12.setSORT(sSort);
										data12.setDEF(sDef);
										cnt12List.add(data12);
									}
								}
							}
						}
						// one record might inclue many 'DEF='
						iCnt12Count -= getSubStrCount(dataList[j + 1 + j12].trim(), "DEF=") - 1;
					}
				} else {
					ErrClass erCnt12 = new ErrClass();
					erCnt12.setERR("0025");
					erCnt12.setVERR("verr");
					errList.add(erCnt12);
				}
			}
		}

		// for data6
		for (int j6 = 0; j6 < dataInfo6.length; j6++) {
			String s6 = dataInfo6[j6].trim();
			String[] d6 = Function.MySplit(s6, "=", "\"");
			if (s6.indexOf("ID=") == 0) {
				sId = d6[1];
			} else if (s6.indexOf("RO=") == 0) {
				sRo = d6[1];
			} else if (s6.indexOf("NUM=") == 0) {
				sNum = d6[1].replace(";", "");
				if (!Function.isNumeric(sNum)) {
					ErrClass errNumClass = new ErrClass();
					errNumClass.setERR("1000");
					errNumClass.setVERR("NUM:" + sNum);
					errList.add(errNumClass);
				}
			} else if (s6.indexOf("ED=") == 0) {
				sEd = d6[1].replace(";", "");
				if (getSubStrCount(s6, "\"") == 1) {
					ErrClass errClass = new ErrClass();
					errClass.setERR("0005");
					errClass.setVERR("ED:" + sEd);
					errList.add(errClass);
				} else if (!isEd(sEd)) {
					ErrClass errClass = new ErrClass();
					errClass.setERR("1001");
					errClass.setVERR("ED:" + sEd);
					errList.add(errClass);
				}
			} else if (s6.indexOf("ET=") == 0) {
				sEt = d6[1].replace(";", "");
			} else if (s6.indexOf("CU=") == 0) {
				sCu = d6[1].replace(";", "");
			} else if (s6.indexOf("AC=") == 0) {
				sAc = d6[1];
			} else if (s6.indexOf("SO") == 0) {
				sSo = d6[1];
			} else if (s6.indexOf("ASTA") == 0) {
				sAsta = d6[1];
			} else if (s6.indexOf("SEFD") == 0) {
				sSefd = d6[1];
			} else if (s6.indexOf("NEWRO") == 0) {
				sNewRo = d6[1];
			} else if (s6.indexOf("INTERC") == 0) {
				sInterc = d6[1];
			} else if (s6.indexOf("REFER") == 0) {
				sRefer = d6[1];
			} else if (s6.indexOf("EINT") == 0) {
				sEint = d6[1];
			} else if (s6.indexOf("DCSN") == 0) {
				sDcsn = d6[1];
			}
		}

		// check id
		if (H2Data.ExecuteScalar("select count(*) as cc from USERINFO where id='" + sId + "'").equals("0")) {
			ErrClass errClass2 = new ErrClass();
			errClass2.setERR("0101");
			errClass2.setVERR("ID:" + sId);
			errList.add(errClass2);
		}
		if (sAc.equals("N")) {
			if (!sCnt11Data.equals("") && sNode.equals("")) {
				ErrClass errNum = new ErrClass();
				errNum.setERR("0028");
				errNum.setVERR("NODE MISSING");
				errList.add(errNum);
			}

			// check num
			if (!H2Data.ExecuteScalar("select count(*) as cc from CustomerRecord where NUM='" + sNum + "'")
					.equals("0")) {
				ErrClass errNum = new ErrClass();
				errNum.setERR("0201");
				errNum.setVERR("TARGET REC EXIST");
				errList.add(errNum);
				errNum.setERR("0202");
				errNum.setVERR("AC=N NOT ALLOWED");
				errList.add(errNum);
			}
			// error
			if (errList.size() > 0) {
				dataAppl = String.format("RSP-CRC:,%s,%s:::DENIED,01::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s", new Object[] {
						YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
				dataAppl += ":CNT=" + String.format("%02d", errList.size());
				for (int iList = 0; iList < errList.size(); iList++) {
					ErrClass ec = errList.get(iList);
					dataAppl += ":ERR=" + ec.getERR() + ",VERR=\"" + ec.getVERR() + "\"";
				}
				obj.dataAppl.setValue(dataAppl.getBytes());
				ctx.writeAndFlush(obj);
				return;
			}

			// only for test ac=N
			if (sDestNodeName.equals("test281")) {
				dataAppl = String.format(
						"RSP-CRC:,%s,%s:::DENIED,10::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s:CNT=01:ERR=0099,VERR=\"SPLIT CARRIERS iec & iac ARE NOT SUPPORTED BY NETWORK $$. \";",
						new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
								sEd, sEt });
				obj.dataAppl.setValue(dataAppl.getBytes());
				ctx.writeAndFlush(obj);
				return;
			} else if (sDestNodeName.equals("test282")) {
				dataAppl = String.format(
						"RSP-CRC:,%s,%s:::DENIED,10::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s:CNT=01:ERR=0099,VERR=\"POTS nnnnnnnnnn & xxxxxxxxxx ARE NOT SUPPORTED BY NETWORK $$. \";",
						new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
								sEd, sEt });
				obj.dataAppl.setValue(dataAppl.getBytes());
				ctx.writeAndFlush(obj);
				return;
			}

			// insert into database
			// insert into CustomerRecord
			H2Data.execute("insert into CustomerRecord ("
					+ "NUM ,RO ,ED ,ET ,CU ,IEC ,IAC ,ABN ,DAU ,DAT ,DD ,HDD ,LI ,RAO ,SF "
					+ ",AGENT ,CUS ,LA ,CBI ,TELCO ,NCON ,CTEL ,ALBL ,AAC ,ALAT ,ANET ,ASTA "
					+ ",SO ,NEWRO ,NOTE,SEFD,INTERC,PAC,PEC,NODE" + ") VALUES ('" + sNum + "','" + sRo + "','" + sEd
					+ "','" + sEt + "','" + sCu + "','" + sIec + "','" + sIac + "','" + sAbn + "','" + sDau + "','"
					+ sDat + "','" + sDd + "','" + sHdd + "','" + sLi + "','" + sRao + "','" + sSf + "','" + sAgent
					+ "','" + sCus + "','" + sLa + "','" + sCbi + "','" + sTelco + "','" + sNcon + "','" + sCtel + "','"
					+ sAlbl + "','" + sAac + "','" + sAlat + "','" + sAnet + "','" + sAsta + "','" + sSo + "','"
					+ sNewRo + "','" + sNote + "','" + sSefd + "','" + sInterc + "','" + sPac + "','" + sPec + "'"
					+ ",'" + sNode + "'" + ") ");

			H2Data.execute("delete from CRCNT3 where NUM='" + sNum + "'");
			H2Data.execute("delete from CRCNT8 where NUM='" + sNum + "'");
			H2Data.execute("delete from CRCNT9 where NUM='" + sNum + "'");
			H2Data.execute("delete from CRCNT11 where NUM='" + sNum + "'");
			H2Data.execute("delete from CRCNT12 where NUM='" + sNum + "'");
			// insert into CRCNT3
			if (!sAlbl.equals("")) {
				String[] sList = sLn.split(",");
				for (int iAlbl = 1; iAlbl < sList.length; iAlbl++) {
					String sTemp = sList[iAlbl];
					H2Data.execute("insert into CRCNT3 (" + "NUM ,ALBL" + ") VALUES ('" + sNum + "','" + sTemp + "') ");
				}
			}
			// insert into CRCNT8
			if (!sLn.equals("")) {
				String[] sList = sLn.split(",");
				for (int iLn = 0; iLn < sList.length; iLn++) {
					String sTemp = sList[iLn];
					H2Data.execute("insert into CRCNT8 (" + "NUM ,LN" + ") VALUES ('" + sNum + "','" + sTemp + "') ");
				}
			}
			// insert into CRCNT9
			if (!sCnt9Data.equals("")) {
				String[] sList = sCnt9Data.split(":");
				for (int iCrCnt9 = 0; iCrCnt9 < sList.length; iCrCnt9++) {
					String sTemp = sList[iCrCnt9];
					String[] d9 = sTemp.split(",");
					String sTel = "", sLns = "", sCity = "", sFso = "", sLsis = "", sLso = "", sSfg = "", sStn = "",
							sUts = "";
					for (int k = 0; k < d9.length; k++) {
						String[] dataInfo9 = d9[k].split("=");
						if (d9[k].indexOf("TEL=") == 0) {
							sTel = dataInfo9[1].replace(";", "");
						} else if (d9[k].indexOf("LNS=") == 0) {
							sLns = dataInfo9[1].replace(";", "");
						} else if (d9[k].indexOf("CITY=") == 0) {
							sCity = dataInfo9[1].replace(";", "");
						} else if (d9[k].indexOf("FSO=") == 0) {
							sFso = dataInfo9[1].replace(";", "");
						} else if (d9[k].indexOf("LSIS=") == 0) {
							sLsis = dataInfo9[1].replace(";", "");
						} else if (d9[k].indexOf("LSO=") == 0) {
							sLso = dataInfo9[1].replace(";", "");
						} else if (d9[k].indexOf("SFG=") == 0) {
							sSfg = dataInfo9[1].replace(";", "");
						} else if (d9[k].indexOf("STN=") == 0) {
							sStn = dataInfo9[1].replace(";", "");
						} else if (d9[k].indexOf("UTS=") == 0) {
							sUts = dataInfo9[1].replace(";", "");
						}
					}
					H2Data.execute("insert into CRCNT9 (" + "NUM,TEL,LNS,CITY,FSO,LSIS,LSO,SFG,STN,UTS) VALUES (" + "'"
							+ sNum + "','" + sTel + "','" + sLns + "','" + sCity + "','" + sFso + "','" + sLsis + "','"
							+ sLso + "','" + sSfg + "','" + sStn + "','" + sUts + "') ");
				}
			}

			// insert into CRCNT12
			if (cnt12List.size() > 0) {
				for (int iCrCnt12 = 0; iCrCnt12 < cnt12List.size(); iCrCnt12++) {
					Cnt12Class data12 = cnt12List.get(iCrCnt12);
					H2Data.execute("insert into CRCNT12 (" + "NUM,`TYPE`,`SORT`,LBL,`DEF`) VALUES (" + "'" + sNum
							+ "','" + data12.getTYPE() + "','" + data12.getSORT() + "','" + data12.getLBL() + "','"
							+ data12.getDEF() + "') ");
				}
			}

			// insert into CRCNT11
			if (!sCnt11Data.equals("")) {
				String[] sList = Function.MySplit(sCnt11Data, ":", "\"");
				for (int iCnt11 = 0; iCnt11 < sList.length; iCnt11++) {
					String sTemp = sList[iCnt11];
					H2Data.execute("insert into CRCNT11 (" + "NUM ,V" + ") VALUES ('" + sNum + "','" + sTemp + "') ");
				}
			}

		} else if (sAc.equals("D")) {
			// update CustomerRecord
			String sSql = "update CustomerRecord set STATUS='disconnect' ,";
			if (!sRefer.equals("")) {
				sSql = sSql + " REFER='" + sRefer + "',";
			}
			if (!sSefd.equals("")) {
				sSql = sSql + " SEFD='" + sSefd + "',";
			}
			if (!sEint.equals("")) {
				sSql = sSql + " EINT='" + sEint.replace("'", "''") + "',";
			}
			if (!sPac.equals("")) {
				sSql = sSql + " PAC='" + sPac.replace("'", "''") + "',";
			}
			if (!sNode.equals("")) {
				sSql = sSql + " NODE='" + sNode.replace("'", "''") + "',";
			}
			if (!sPec.equals("")) {
				sSql = sSql + " PEC='" + sPec.replace("'", "''") + "',";
			}
			if (!sEd.equals("")) {
				sSql = sSql + " ED='" + sEd.replace("'", "''") + "',";
			}
			sSql = sSql.substring(0, sSql.length() - 1);

			sSql = sSql + " where NUM='" + sNum + "'";
			H2Data.execute(sSql);
			if (sDestNodeName.equals("test194")) {
				dataAppl = String.format("RSP-CRC:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
						YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			} else if (sDestNodeName.equals("test195")) {
				dataAppl = String.format("RSP-CRC:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
						YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			} else {
				dataAppl = String.format("RSP-CRC:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
						YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			}
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sAc.equals("R")) {
			// update CustomerRecord
			String sSql = "update CustomerRecord set STATUS='sending' ,";
			if (!sSo.equals("")) {
				sSql = sSql + " SO='" + sSo + "',";
			}
			if (!sSefd.equals("")) {
				sSql = sSql + " SEFD='" + sSefd + "',";
			}
			if (!sAsta.equals("")) {
				sSql = sSql + " ASTA='" + sAsta.replace("'", "''") + "',";
			}
			sSql = sSql.substring(0, sSql.length() - 1);

			sSql = sSql + " where NUM='" + sNum + "'";
			H2Data.execute(sSql);
			dataAppl = String.format("RSP-CRC:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sAc.equals("T")) {

			// error
			if (errList.size() > 0) {
				dataAppl = String.format("RSP-CRC:,%s,%s:::DENIED,01::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s", new Object[] {
						YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
				dataAppl += ":CNT=" + String.format("%02d", errList.size());
				for (int iList = 0; iList < errList.size(); iList++) {
					ErrClass ec = errList.get(iList);
					dataAppl += ":ERR=" + ec.getERR() + ",VERR=\"" + ec.getVERR() + "\"";
				}
				obj.dataAppl.setValue(dataAppl.getBytes());
				ctx.writeAndFlush(obj);
				return;
			}

			// update CustomerRecord
			String sSql = "update CustomerRecord set STATUS='transfer' ,";
			if (!sRo.equals("")) {
				sSql = sSql + " RO='" + sRo + "',";
			}
			if (!sEd.equals("")) {
				sSql = sSql + " ED='" + sEd + "',";
			}
			if (!sEt.equals("")) {
				sSql = sSql + " ET='" + sEt + "',";
			}
			if (!sSefd.equals("")) {
				sSql = sSql + " SEFD='" + sSefd + "',";
			}
			if (!sNote.equals("")) {
				sSql = sSql + " NOTE='" + sNote + "',";
			}
			if (!sSo.equals("")) {
				sSql = sSql + " SO='" + sSo + "',";
			}
			if (!sAsta.equals("")) {
				sSql = sSql + " ASTA='" + sAsta.replace("'", "''") + "',";
			}
			sSql = sSql.substring(0, sSql.length() - 1);

			sSql = sSql + " where NUM='" + sNum + "'";
			H2Data.execute(sSql);

			H2Data.execute("delete from CRCNT3 where NUM='" + sNum + "'");
			H2Data.execute("delete from CRCNT8 where NUM='" + sNum + "'");
			H2Data.execute("delete from CRCNT9 where NUM='" + sNum + "'");
			H2Data.execute("delete from CRCNT11 where NUM='" + sNum + "'");
			H2Data.execute("delete from CRCNT12 where NUM='" + sNum + "'");

			// insert into CRCNT3
			if (!sAlbl.equals("")) {
				String[] sList = sLn.split(",");
				for (int iAlbl = 1; iAlbl < sList.length; iAlbl++) {
					String sTemp = sList[iAlbl];
					H2Data.execute("insert into CRCNT3 (" + "NUM ,ALBL" + ") VALUES ('" + sNum + "','" + sTemp + "') ");
				}
			}

			// insert into CRCNT11
			if (!sCnt11Data.equals("")) {
				String[] sList = Function.MySplit(sCnt11Data, ":", "\"");
				for (int iCnt11 = 0; iCnt11 < sList.length; iCnt11++) {
					String sTemp = sList[iCnt11];
					H2Data.execute("insert into CRCNT11 (" + "NUM ,V" + ") VALUES ('" + sNum + "','" + sTemp + "') ");
				}
			}

			// insert into CRCNT12
			if (cnt12List.size() > 0) {
				for (int iCrCnt12 = 0; iCrCnt12 < cnt12List.size(); iCrCnt12++) {
					Cnt12Class data12 = cnt12List.get(iCrCnt12);
					H2Data.execute("insert into CRCNT12 (" + "NUM,`TYPE`,`SORT`,LBL,`DEF`) VALUES (" + "'" + sNum
							+ "','" + data12.getTYPE() + "','" + data12.getSORT() + "','" + data12.getLBL() + "','"
							+ data12.getDEF() + "') ");
				}
			}

			dataAppl = String.format("RSP-CRC:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sAc.equals("X")) {
			// update CustomerRecord
			String sSql = "delete from CustomerRecord where NUM='" + sNum + "'";
			H2Data.execute(sSql);
			sSql = "select count(*) as cc from CustomerRecord where NUM='" + sNum + "'";
			ResultSet rs = H2Data.executeQuery(sSql);
			// 锟斤拷锟斤拷锟斤拷锟斤拷锟�
			try {
				if (rs.next()) {
					if (rs.getInt("cc") > 0) {
						if (sDcsn.equals("N")) {
							dataAppl = String.format(
									"RSP-CRC:,%s,%s:::DENIED,01::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s:CNT=01:ERR=0099,VERR=verr;",
									new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()),
											sId, sRo, sNum, sEd, sEt });
						} else {
							dataAppl = String.format("RSP-CRC:,%s,%s:::DENIED,01::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;",
									new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()),
											sId, sRo, sNum, sEd, sEt });
						}

					} else {
						dataAppl = String.format("RSP-CRC:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;",
								new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId,
										sRo, sNum, sEd, sEt });
					}
				} else {
					dataAppl = String.format("RSP-CRC:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;",
							new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo,
									sNum, sEd, sEt });
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				dataAppl = String.format("RSP-CRC:,%s,%s:::DENIED,01::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
						YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			}

			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sAc.equals("C")) {
			// error
			if (errList.size() > 0) {
				dataAppl = String.format("RSP-CRC:,%s,%s:::DENIED,01::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s", new Object[] {
						YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
				dataAppl += ":CNT=" + String.format("%02d", errList.size());
				for (int iList = 0; iList < errList.size(); iList++) {
					ErrClass ec = errList.get(iList);
					dataAppl += ":ERR=" + ec.getERR() + ",VERR=\"" + ec.getVERR() + "\"";
				}
				obj.dataAppl.setValue(dataAppl.getBytes());
				ctx.writeAndFlush(obj);
				return;
			}

			// update CustomerRecord
			String sSql = "update CustomerRecord set ";
			if (!sSo.equals("")) {
				sSql = sSql + " SO='" + sSo + "',";
			}
			if (!sAsta.equals("")) {
				sSql = sSql + " ASTA='" + sAsta + "',";
			}
			if (!sAnet.equals("")) {
				sSql = sSql + " ANET='" + sAnet + "',";
			}
			if (!sSefd.equals("")) {
				sSql = sSql + " SEFD='" + sSefd + "',";
			}
			if (!sNewRo.equals("")) {
				sSql = sSql + " NEWRO='" + sNewRo + "',";
			}
			if (!sEd.equals("")) {
				sSql = sSql + " ED='" + sEd + "',";
			}
			if (!sInterc.equals("")) {
				sSql = sSql + " INTERC='" + sInterc + "',";
			}
			if (!sAac.equals("")) {
				sSql = sSql + " AAC='" + sAac + "',";
			}
			sSql = sSql.substring(0, sSql.length() - 1);

			sSql = sSql + " where NUM='" + sNum + "'";
			H2Data.execute(sSql);

			// insert into CRCNT12
			H2Data.execute("delete from CRCNT3 where NUM='" + sNum + "'");
			H2Data.execute("delete from CRCNT8 where NUM='" + sNum + "'");
			// H2Data.execute("delete from CRCNT9 where NUM='" + sNum + "'");
			H2Data.execute("delete from CRCNT11 where NUM='" + sNum + "'");
			H2Data.execute("delete from CRCNT12 where NUM='" + sNum + "'");

			// insert into CRCNT3
			if (!sAlbl.equals("")) {
				String[] sList = sLn.split(",");
				for (int iAlbl = 1; iAlbl < sList.length; iAlbl++) {
					String sTemp = sList[iAlbl];
					H2Data.execute("insert into CRCNT3 (" + "NUM ,ALBL" + ") VALUES ('" + sNum + "','" + sTemp + "') ");
				}
			}

			// insert into CRCNT12
			if (cnt12List.size() > 0) {
				for (int iCrCnt12 = 0; iCrCnt12 < cnt12List.size(); iCrCnt12++) {
					Cnt12Class data12 = cnt12List.get(iCrCnt12);
					H2Data.execute("insert into CRCNT12 (" + "NUM,`TYPE`,`SORT`,LBL,`DEF`) VALUES (" + "'" + sNum
							+ "','" + data12.getTYPE() + "','" + data12.getSORT() + "','" + data12.getLBL() + "','"
							+ data12.getDEF() + "') ");
				}
			}

			if (sDestNodeName.equals("test191")) {
				dataAppl = String.format(
						"RSP-CRC:,%s,%s:::DENIED,10::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s:CNTA=cnta:ERR1=9000;",
						new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
								sEd, sEt });
			} else if (sDestNodeName.equals("test193")) {
				dataAppl = String.format(
						"RSP-CRC:,%s,%s:::DENIED,01::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s:CNT=cnt:ERR=7615,VERR=锟斤拷ROCHANGE NOW锟斤拷;",
						new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
								sEd, sEt });
			} else if (sDestNodeName.equals("test192")) {
				dataAppl = String.format(
						"RSP-CRC:,%s,%s:::DENIED,10::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s:CNTA=cnta:ERR1=9000;",
						new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
								sEd, sEt });

			} else if (sEd.equals("\"NOW\"")) {
				dataAppl = String.format(
						"RSP-CRC:,%s,%s:::COMPLD,11::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s:CNTA=cnta:ERR1=9000;",
						new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
								sEd, sEt });
			} else {
				dataAppl = String.format("RSP-CRC:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
						YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			}
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else {
			ErrClass errACClass = new ErrClass();
			errACClass.setERR("0100");
			errACClass.setVERR("AC:" + sAc);
			errList.add(errACClass);
			// error
			if (errList.size() > 0) {
				dataAppl = String.format("RSP-CRC:,%s,%s:::DENIED,01::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s", new Object[] {
						YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
				dataAppl += ":CNT=" + String.format("%02d", errList.size());
				for (int iList = 0; iList < errList.size(); iList++) {
					ErrClass ec = errList.get(iList);
					dataAppl += ":ERR=" + ec.getERR() + ",VERR=\"" + ec.getVERR() + "\"";
				}
				obj.dataAppl.setValue(dataAppl.getBytes());
				ctx.writeAndFlush(obj);
				return;
			}

		}

		if (sDestNodeName.equals("test168")) {
			// test168
			dataAppl = String.format("RSP-CRC:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test169")) {
			// test169
			dataAppl = String.format("RSP-CRC:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test170")) {
			// test170
			dataAppl = String.format("RSP-CRC:,%s,%s:::DENIED,01::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s:ERR=0202;",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
							sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test171")) {
			// test171
			dataAppl = String.format("RSP-CRC:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test172")) {
			// test172
			dataAppl = String.format("RSP-CRC:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test173")) {
			// test173
			dataAppl = String.format("RSP-CRC:,%s,%s:::DENIED,01::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s:ERR=0308;",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
							sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test174")) {
			// test174
			dataAppl = String.format("RSP-CRC:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test175")) {
			// test175
			dataAppl = String.format("RSP-CRC:,%s,%s:::DENIED,01::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s:ERR=0502;",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
							sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test176")) {
			// test176
			dataAppl = String.format("RSP-CRC:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test177")) {
			// test177
			dataAppl = String.format("RSP-CRC:,%s,%s:::DENIED,01::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s:ERR=0409;",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
							sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test178")) {
			// test178
			dataAppl = String.format("RSP-CRC:,%s,%s:::DENIED,01::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s:ERR=0310;",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
							sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test179")) {
			// test179
			dataAppl = String.format("RSP-CRC:,%s,%s:::DENIED,01::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s:ERR=0310;",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
							sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test180")) {
			// test180
			dataAppl = String.format("RSP-CRC:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test181")) {
			// test181
			dataAppl = String.format("RSP-CRC:,%s,%s:::DENIED,01::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s:ERR=310;",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
							sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test182")) {
			// test182
			dataAppl = String.format("RSP-CRC:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test183")) {
			// test183
			dataAppl = String.format("RSP-CRC:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test184")) {
			// test183
			dataAppl = String.format("RSP-CRC:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else {
			dataAppl = String.format("RSP-CRC:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,ED=%s,ET=%s;", new Object[] {
					YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum, sEd, sEt });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		}
	}

	private void handleNumReqCrq(ChannelHandlerContext ctx, YlDcmMsg obj, String dataAppl, String[] dataList, int count,
			String sNumList) {
		System.out.println("----read asn.1 message from " + new String(obj.tliHdr.destNodeName.getValue())
				+ ":UPDATE CUSTOMER RECORD (REQ-CRQ)>>" + dataAppl);
		String[] dataInfo6 = Function.MySplit(dataList[6], ",", "\"");
		String sId = "";
		String sRo = "";
		String sNpa = "";
		String sNxx = "";
		String sAc = "";
		String sNum = "";
		String sLine = "";
		String sNcon = "";
		String sCtel = "";
		String sRu = "";
		String sNewRo = "";
		String sNotes = "";
		String sEd = "";
		String sEt = "";
		String sCu = "";
		String sIec = "", sIac = "", sAbn = "", sDau = "", sDat = "", sDd = "", sHdd = "", sLi = "", sRao = "",
				sSf = "", sAgent = "", sCus = "", sLa = "", sCbi = "", sTelco = "", sAlbl = "", sAac = "", sAlat = "",
				sAnet = "", sAsta = "", sSo = "", sNote = "", sLn = "", sCnt9Data = "", sSefd = "", sInterc = "",
				sRefer = "", sEint = "", sDcsn = "", sPec = "", sPac = "", sNode = "", sCnt11Data = "";
		int iCnt11RealCount = 0;
		String sDestNodeName = new String(obj.tliHdr.destNodeName.getValue());
		List<ErrClass> errList = new ArrayList<ErrClass>();
		List<Cnt12Class> cnt12List = new ArrayList<Cnt12Class>();
		List<String> cnt13List = new ArrayList<String>();
		if (dataList.length > 0 && !dataList[dataList.length - 1].endsWith(";")) {
			ErrClass errClass1 = new ErrClass();
			errClass1.setERR("0001");
			errClass1.setVERR(";REQUIRED");
			errList.add(errClass1);
		}

		// for :
		int iIacCount = 0;
		for (int j = 0; j < dataList.length; j++) {
			String ss = dataList[j].trim();
			String[] dataInfo2 = Function.MySplit(ss, "=", "\"");
			if (ss.indexOf("IEC=") == 0) {
				sIec = dataInfo2[1].replace(";", "");
			} else if (ss.indexOf("IAC=") == 0) {
				iIacCount++;
				sIac = dataInfo2[1].replace(";", "");
				if (iIacCount > 1) {
					String[] sIacList = sIac.split(",");
					ErrClass errClass = new ErrClass();
					errClass.setERR("0029");
					errClass.setVERR("IAC:" + sIacList[1].replace("\"", ""));
					errList.add(errClass);
				}
			} else if (ss.indexOf("ALBL=") == 0) {
				sAlbl = dataInfo2[1].replace(";", "");
				// check exist or not
				String[] data_albl = sAlbl.split(",");
				for (int jj = 1; jj < data_albl.length; jj++) {
					String s2 = data_albl[jj].trim();
					ErrClass alblError = isExistedAlbl(s2, sNum);
					if (alblError != null) {
						errList.add(alblError);
						break;
					}
				}
			} else if (ss.indexOf("PAC=") == 0) {
				sPac = dataInfo2[1].replace(";", "");
			} else if (ss.indexOf("NODE=") == 0) {
				sNode = dataInfo2[1].replace(";", "");
				ErrClass nodeError = isNode(sNode);
				if (nodeError != null) {
					errList.add(nodeError);
				}
			} else if (ss.indexOf("PEC=") == 0) {
				sPec = dataInfo2[1].replace(";", "");
			} else if (ss.indexOf("UNREC") >= 0) {
				String[] sTempList2 = ss.split("UNREC=");
				ErrClass errClass = new ErrClass();
				errClass.setERR("0003");
				errClass.setVERR("UNREC:" + sTempList2[1]);
				errList.add(errClass);
			} else if (ss.indexOf("AAC=") == 0) {
				sAac = dataInfo2[1].replace(";", "");
				if (sAac.indexOf("CNT4") < 0) {
					String[] sAacList = sAac.split(",");
					ErrClass errClass = new ErrClass();
					errClass.setERR("0030");
					errClass.setVERR("AAC:" + sAacList[0]);
					errList.add(errClass);
				}

			} else if (ss.indexOf("ALAT=") == 0) {
				sAlat = dataInfo2[1].replace(";", "");
			} else if (ss.indexOf("ANET=") == 0) {
				sAnet = dataInfo2[1];
			} else if (ss.indexOf("ASTA=") == 0) {
				sAsta = dataInfo2[1];
			} else if (ss.indexOf("ABN=") == 0) {
				String[] data_abn = ss.split(",");
				for (int jj = 0; jj < data_abn.length; jj++) {
					String s2 = data_abn[jj].trim();
					String[] dataInfo3 = s2.split("=");
					if (s2.indexOf("ABN=") == 0) {
						sAbn = dataInfo3[1].replace(";", "");
					} else if (s2.indexOf("DAU=") == 0) {
						sDau = dataInfo3[1].replace(";", "");
					} else if (s2.indexOf("DAT=") == 0) {
						sDat = dataInfo3[1].replace(";", "");
					} else if (s2.indexOf("DD=") == 0) {
						sDd = dataInfo3[1].replace(";", "");
					} else if (s2.indexOf("HDD=") == 0) {
						sHdd = dataInfo3[1].replace(";", "");
						if (!isHdd(sHdd)) {
							ErrClass errClass = new ErrClass();
							errClass.setERR("1016");
							errClass.setVERR("HDD:" + sHdd);
							errList.add(errClass);
						}
					} else if (s2.indexOf("LI=") == 0) {
						sLi = dataInfo3[1].replace(";", "");
						if (!isLi(sLi)) {
							ErrClass errClass = new ErrClass();
							errClass.setERR("1018");
							errClass.setVERR("LI:" + sLi);
							errList.add(errClass);
						}
					} else if (s2.indexOf("RAO=") == 0) {
						sRao = dataInfo3[1].replace(";", "");
					} else if (s2.indexOf("SF=") == 0) {
						sSf = dataInfo3[1].replace(";", "");
						if (!isSf(sSf)) {
							ErrClass errClass = new ErrClass();
							errClass.setERR("1023");
							errClass.setVERR("SF:" + sSf);
							errList.add(errClass);
						}
					} else if (s2.indexOf("SO=") == 0) {
						sSo = dataInfo3[1].replace(";", "");
						if (sSo.length() >= 13) {
							String[] sSoList = sSo.split(",");
							ErrClass errClass = new ErrClass();
							errClass.setERR("1022");
							errClass.setVERR("SO:" + sSoList[1]);
							errList.add(errClass);
						}

					} else if (s2.indexOf("NEWRO=") == 0) {
						sNewRo = dataInfo3[1];
					} else if (s2.indexOf("NOTE=") == 0) {
						sNote = dataInfo3[1].replace(";", "");
					} else if (s2.indexOf("AGENT=") == 0) {
						sAgent = dataInfo3[1].replace(";", "");
					} else if (s2.indexOf("CUS=") == 0) {
						sCus = dataInfo3[1].replace(";", "");
					} else if (s2.indexOf("LA=") == 0) {
						sLa = dataInfo3[1].replace(";", "");
					} else if (s2.indexOf("CBI=") == 0) {
						sCbi = dataInfo3[1].replace(";", "");
					} else if (s2.indexOf("TELCO=") == 0) {
						sTelco = dataInfo3[1].replace(";", "");
					} else if (s2.indexOf("NCON=") == 0) {
						sNcon = dataInfo3[1];
					} else if (s2.indexOf("CTEL=") == 0) {
						sCtel = dataInfo3[1].replace(";", "");
					}
				}
			} else if (ss.indexOf("CNT8=") == 0) {
				String sCnt8 = dataInfo2[1].replace(";", "");
				int iCnt8Count = Integer.parseInt(sCnt8);
				sLn = "";
				for (int j8 = 0; j8 < iCnt8Count; j8++) {
					String s8 = dataList[j + 1 + j8].trim();
					String[] d8 = s8.split("=");
					if (!isLn(d8[1].replace(";", ""))) {
						ErrClass errClass = new ErrClass();
						errClass.setERR("1042");
						errClass.setVERR("LN:" + d8[1].replace(";", ""));
						errList.add(errClass);
					} else {
						sLn = sLn + d8[1].replace(";", "") + ",";
					}
				}
				if (!sLn.equals("")) {
					sLn = sLn.substring(0, sLn.length() - 1);
				}
			} else if (ss.indexOf("CNT11=") == 0) {
				String sCnt11 = dataInfo2[1].replace(";", "");
				int iCnt11Count = Integer.parseInt(sCnt11);
				sCnt11Data = "";
				iCnt11RealCount = 0;
				List<String> cnt11V1st = new ArrayList<String>();
				for (int j11 = 0; j11 < iCnt11Count; j11++) {
					String s11 = dataList[j + 1 + j11].trim();
					String[] d11 = s11.split("=");
					if (d11[0].equals("V")) {
						sCnt11Data = sCnt11Data + d11[1].replace(";", "") + ":";
						ErrClass cnt11vErr = isCnt11V(d11[1].replace("\"", "").replace(";", ""), cnt11V1st, sAc, sNum);
						if (cnt11vErr != null) {
							errList.add(cnt11vErr);
						} else {
							String[] list11 = d11[1].replace("\"", "").replace(";", "").split(",");
							s11 = list11[0] + "," + list11[1];
							cnt11V1st.add(s11);
						}
						iCnt11RealCount++;
					} else {
						break;
					}
				}
				if (!sCnt11Data.equals("")) {
					sCnt11Data = sCnt11Data.substring(0, sCnt11Data.length() - 1);
				}
				// add error
				if (iCnt11RealCount != iCnt11Count) {
					ErrClass errClass = new ErrClass();
					errClass.setERR("0011");
					errClass.setVERR(String.format("CNT11:%02d/V:%02d", iCnt11Count, iCnt11RealCount));
					errList.add(errClass);
				}

				ErrClass cnt11All = isCnt11All(cnt11V1st);
				if (cnt11All != null) {
					errList.add(cnt11All);
				}
			} else if (ss.indexOf("CNT9=") == 0) {
				String sCnt9 = dataInfo2[1].replace(";", "");
				int iCnt9Count = Integer.parseInt(sCnt9);
				sCnt9Data = "";
				for (int j9 = 0; j9 < iCnt9Count; j9++) {
					sCnt9Data = sCnt9Data + dataList[j + 1 + j9].trim() + ":";
				}
				sCnt9Data = sCnt9Data.substring(0, sCnt9Data.length() - 1);
			} else if (ss.indexOf("CNT12=") == 0) {
				String sCnt12 = dataInfo2[1].replace(";", "");
				int iCnt12Count = Integer.parseInt(sCnt12);

				String sType = "", sLbl = "", sDef = "", sSort = "";
				if (iCnt12Count < 1000) {
					for (int j12 = 0; j12 < iCnt12Count; j12++) {
						if (dataList[j + 1 + j12].trim().indexOf("TYPE=") < 0) {
							ErrClass erCnt12 = new ErrClass();
							erCnt12.setERR("1121");
							erCnt12.setVERR("TYPE MISSING");
							errList.add(erCnt12);
							break;
						}
						if (dataList[j + 1 + j12].trim().indexOf("CNT12=") > 0) {
							ErrClass erCnt12 = new ErrClass();
							erCnt12.setERR("0030");
							erCnt12.setVERR("TYPE:CNT12");
							errList.add(erCnt12);
							break;
						}

						String sTemp = dataList[j + 1 + j12].trim();
						String[] d12 = Function.MySplit(sTemp, ",", "\"");
						for (int k = 0; k < d12.length; k++) {
							String[] dataInfo12 = d12[k].split("=");
							if (d12[k].indexOf("TYPE=") == 0) {
								sType = dataInfo12[1].replace(";", "");
							} else if (d12[k].indexOf("LBL") == 0) {
								sLbl = dataInfo12[1].replace(";", "");
							} else if (d12[k].indexOf("SORT") == 0) {
								sSort = dataInfo12[1].replace(";", "");
							} else if (d12[k].indexOf("DEF=") == 0) {
								// many def
								String[] sDefList = d12[k].split("DEF=");
								for (int m = 0; m < sDefList.length; m++) {
									sDef = sDefList[m].replace("\"", "").replace(";", "");
									if (!sDef.equals("")) {
										// check and add into list
										ErrClass errCnt12 = isCnt12Type(sType);
										if (errCnt12 != null) {
											errList.add(errCnt12);
											break;
										}
										errCnt12 = isCnt12Lbl(sLbl);
										if (errCnt12 != null) {
											errList.add(errCnt12);
											break;
										}
										errCnt12 = isCnt12Def(sType, sDef);
										if (errCnt12 != null) {
											errList.add(errCnt12);
											break;
										}

										// check cnt13
										if (sType.equals("SD")) {
											String[] data13List = sDef.split(",");
											for (int l = 1; l < data13List.length; l++) {
												errCnt12 = isCnt13(sType, data13List[l], cnt13List);
												if (errCnt12 != null) {
													errList.add(errCnt12);
													break;
												} else {
													cnt13List.add(data13List[l]);
												}
											}
										}

										Cnt12Class data12 = new Cnt12Class();
										data12.setTYPE(sType);
										data12.setLBL(sLbl);
										data12.setSORT(sSort);
										data12.setDEF(sDef);
										cnt12List.add(data12);
									}
								}
							}
						}
						// one record might inclue many 'DEF='
						iCnt12Count -= getSubStrCount(dataList[j + 1 + j12].trim(), "DEF=") - 1;
					}
				} else {
					ErrClass erCnt12 = new ErrClass();
					erCnt12.setERR("0025");
					erCnt12.setVERR("verr");
					errList.add(erCnt12);
				}
			}
		}

		// for data6
		for (int j6 = 0; j6 < dataInfo6.length; j6++) {
			String s6 = dataInfo6[j6].trim();
			String[] d6 = Function.MySplit(s6, "=", "\"");
			if (s6.indexOf("ID=") == 0) {
				sId = d6[1];
			} else if (s6.indexOf("RO=") == 0) {
				sRo = d6[1];
			} else if (s6.indexOf("NUM=") == 0) {
				sNum = d6[1].replace(";", "");
				if (!Function.isNumeric(sNum)) {
					ErrClass errNumClass = new ErrClass();
					errNumClass.setERR("1000");
					errNumClass.setVERR("NUM:" + sNum);
					errList.add(errNumClass);
				}
			} else if (s6.indexOf("ED=") == 0) {
				sEd = d6[1].replace(";", "");
				if (getSubStrCount(s6, "\"") == 1) {
					ErrClass errClass = new ErrClass();
					errClass.setERR("0005");
					errClass.setVERR("ED:" + sEd);
					errList.add(errClass);
				} else if (!isEd(sEd)) {
					ErrClass errClass = new ErrClass();
					errClass.setERR("1001");
					errClass.setVERR("ED:" + sEd);
					errList.add(errClass);
				}
			} else if (s6.indexOf("ET=") == 0) {
				sEt = d6[1].replace(";", "");
			} else if (s6.indexOf("CU=") == 0) {
				sCu = d6[1].replace(";", "");
			} else if (s6.indexOf("AC=") == 0) {
				sAc = d6[1];
			} else if (s6.indexOf("SO") == 0) {
				sSo = d6[1];
			} else if (s6.indexOf("ASTA") == 0) {
				sAsta = d6[1];
			} else if (s6.indexOf("SEFD") == 0) {
				sSefd = d6[1];
			} else if (s6.indexOf("NEWRO") == 0) {
				sNewRo = d6[1];
			} else if (s6.indexOf("INTERC") == 0) {
				sInterc = d6[1];
			} else if (s6.indexOf("REFER") == 0) {
				sRefer = d6[1];
			} else if (s6.indexOf("EINT") == 0) {
				sEint = d6[1];
			} else if (s6.indexOf("DCSN") == 0) {
				sDcsn = d6[1];
			}
		}

		// check id
		if (H2Data.ExecuteScalar("select count(*) as cc from USERINFO where id='" + sId + "'").equals("0")) {
			ErrClass errClass2 = new ErrClass();
			errClass2.setERR("0101");
			errClass2.setVERR("ID:" + sId);
			errList.add(errClass2);
		}
		// check CR
		ErrClass errNumExisted = isNumExisted(sNum);
		if (errNumExisted != null) {
			errList.add(errNumExisted);
		}

		// error
		if (errList.size() > 0) {
			dataAppl = String.format("RSP-CRQ:,%s,%s:::DENIED,01::ID=%s,RO=%s,NUM=%s,RED=red,RET=ret",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum });
			dataAppl += ":CNT=" + String.format("%02d", errList.size());
			for (int iList = 0; iList < errList.size(); iList++) {
				ErrClass ec = errList.get(iList);
				dataAppl += ":ERR=" + ec.getERR() + ",VERR=\"" + ec.getVERR() + "\"";
			}
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		}

		if (sDestNodeName.equals("test283")) {
			// test283
			dataAppl = String.format(
					"RSP-CRQ:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,RED=red,RET=ret,"
							+ "RONUM=ronum,MORE=more:CNT=cnt:ED=ed,ET=et,STAT=stat,APP=app;",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else {
			dataAppl = String.format(
					"RSP-CRQ:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,RED=red,RET=ret,"
							+ "RONUM=ronum,MORE=more:CNT=cnt:ED=ed,ET=et,STAT=stat,APP=app;",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		}
	}

	private void handleNumReqCrv(ChannelHandlerContext ctx, YlDcmMsg obj, String dataAppl, String[] dataList, int count,
			String sNumList) {
		System.out.println(
				"----read asn.1 message from " + new String(obj.tliHdr.destNodeName.getValue()) + ":>>" + dataAppl);
		String[] dataInfo6 = Function.MySplit(dataList[6], ",", "\"");
		String[] datetimeInfo1 = Function.MySplit(dataList[1], ",", "\"");
		String sId = "";
		String sRo = "";
		String sNum = "";
		String sEd = "";
		String sEt = "";
		String sRstat = "";
		String sDate = "", sTime = "";
		sDate = datetimeInfo1[1];
		sTime = datetimeInfo1[2];
		String sDestNodeName = new String(obj.tliHdr.destNodeName.getValue());
		List<ErrClass> errList = new ArrayList<ErrClass>();
		ErrClass err = Function.isDatetime(sDate, sTime);
		if (err != null) {
			errList.add(err);
		}

		if (dataList.length > 0 && !dataList[dataList.length - 1].endsWith(";")) {
			ErrClass errClass1 = new ErrClass();
			errClass1.setERR("0001");
			errClass1.setVERR(";REQUIRED");
			errList.add(errClass1);
		}

		// for data6
		for (int j6 = 0; j6 < dataInfo6.length; j6++) {
			String s6 = dataInfo6[j6].trim();
			String[] d6 = Function.MySplit(s6, "=", "\"");
			if (s6.indexOf("ID=") == 0) {
				sId = d6[1];
			} else if (s6.indexOf("RO=") == 0) {
				sRo = d6[1];
			} else if (s6.indexOf("RSTAT=") == 0) {
				sRstat = d6[1];
				ErrClass errClass = isRstat(sRstat);
				if (errClass != null) {
					errList.add(errClass);
				} else {
					errClass = isRstat(sRstat, sNum);
					if (errClass != null) {
						errList.add(errClass);
					}
				}
			} else if (s6.indexOf("NUM=") == 0) {
				sNum = d6[1].replace(";", "");
				if (!Function.isNumeric(sNum)) {
					ErrClass errNumClass = new ErrClass();
					errNumClass.setERR("1000");
					errNumClass.setVERR("NUM:" + sNum);
					errList.add(errNumClass);
				}
			} else if (s6.indexOf("ED=") == 0) {
				sEd = d6[1].replace(";", "");
				if (getSubStrCount(s6, "\"") == 1) {
					ErrClass errClass = new ErrClass();
					errClass.setERR("0005");
					errClass.setVERR("ED:" + sEd);
					errList.add(errClass);
				} else if (!isEd(sEd)) {
					ErrClass errClass = new ErrClass();
					errClass.setERR("1001");
					errClass.setVERR("ED:" + sEd);
					errList.add(errClass);
				}
			} else if (s6.indexOf("ET=") == 0) {
				sEt = d6[1].replace(";", "");
			} else {
				ErrClass errClass = new ErrClass();
				errClass.setERR("03");
				if (d6.length > 1) {
					errClass.setVERR(String.format("%s=%s", d6[0].replace(";", ""), d6[1].replace(";", "")));
				} else {
					errClass.setVERR(s6);
				}
				errList.add(errClass);
			}
		}

		if (!sEd.equals("") && !sEt.equals("") && !sRstat.equals("")) {
			ErrClass errClass2 = new ErrClass();
			errClass2.setERR("04");
			errClass2.setVERR("verr");
			errList.add(errClass2);
		}

		// check id
		if (sId.equals("")) {
			ErrClass errClass2 = new ErrClass();
			errClass2.setERR("02");
			errClass2.setVERR("ID:MISSING");
			errList.add(errClass2);
		} else if (H2Data.ExecuteScalar("select count(*) as cc from USERINFO where id='" + sId + "'").equals("0")) {
			ErrClass errClass2 = new ErrClass();
			errClass2.setERR("0101");
			errClass2.setVERR("ID:" + sId);
			errList.add(errClass2);
		}
		// check CR
		ErrClass errNumExisted = isNumExisted(sNum);
		if (errNumExisted != null) {
			errList.add(errNumExisted);
		}
		// error
		if (errList.size() > 0) {
			dataAppl = String.format("RSP-CRQ:,%s,%s:::DENIED,01::ID=%s,RO=%s,NUM=%s,RED=red,RET=ret",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum });
			dataAppl += ":CNT=" + String.format("%02d", errList.size());
			for (int iList = 0; iList < errList.size(); iList++) {
				ErrClass ec = errList.get(iList);
				dataAppl += ":ERR=" + ec.getERR() + ",VERR=\"" + ec.getVERR() + "\"";
			}
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		}

		// get CustomerRecord
		String sCr = getCustomerRecord(sNum);

		if (sDestNodeName.equals("test286")) {
			// test286
			dataAppl = String.format(
					"RSP-CRQ:,%s,%s:::COMPLD,00::ID=%s,RO=%s,CRO=cro,NUM=%s,RED=red,"
							+ "RET=ret,STAT=stat,APP=app,CNT=01:ERR=08,VERR=verr;",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test287")) {
			// test286
			dataAppl = String.format(
					"RSP-CRQ:,%s,%s:::COMPLD,00::ID=%s,RO=%s,CRO=cro,NUM=num,RSTAT=rstat,"
							+ "STAT=stat,APP=app:CNT=01:ERR=08,VERR=verr;",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test289")) {
			// test289
			dataAppl = String.format(
					"RSP-CRV:,%s,%s:::COMPLD,00::ID=%s,RO=%s,CRO=cro,NUM=num,ED=ed,ET=et;"
							+ "STAT=stat,APP=app:IEC=\"CNT1=01,iec1\":RCC=rcc,DD=dd,RED=red,RET=ret:"
							+ "ASTA=\"CNT7=03,NJ,NY,PA\":CNT9=04:TEL=\"7035731000\",LNS=0001,LSO=703573:"
							+ "TEL=\"7035731001\",LNS=0001,LSO=703573:TEL=\"7035731002\",LNS=0001,LSO=703573:TEL=\"DIAL num\",LNS=0001:"
							+ "NODE=\"CNT10=06,ST,SW,PC,DT,CA,TE\":CNT11=03:V=\"NJ,,,,iec1,7035731000\":V=\"NY,ON1,25,,iec1,7035731000\":"
							+ "V=\"NY,ON1,50A,01/01-09/30,iec1,7035731002\":Z=z,DS=ds,PEC=pec,CNT=01,ERR=09,VERR=verr;",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test290")) {
			// test290
			dataAppl = String.format(
					"RSP-CRV:,%s,%s:::COMPLD,11::ID=%s,RO=%s,CRO=cro,NUM=num,ED=ed,ET=et;"
							+ "STAT=stat,APP=app:IEC=\"CNT1=01,iec1\":RCC=rcc,DD=dd,RSTAT=rstat:"
							+ "ASTA=\"CNT7=03,NJ,NY,PA\":CNT9=04:TEL=\"7035731000\",LNS=0001,LSO=703573:"
							+ "TEL=\"7035731001\",LNS=0001,LSO=703573:TEL=\"7035731002\",LNS=0001,LSO=703573:"
							+ "TEL=\"DIALnum\",LNS=0001:NODE=\"CNT10=06,ST,SW,PC,DT,CA,TE\":CNT11=03:"
							+ "V=\"NJ,,,,iec1,7035731000\":V=\"NY,ON1,25,,iec1,7035731000\": V=\"NY,ON1,50A,01/01-09/30,iec1,7035731002\":"
							+ "Z=z,DS=ds,PEC=pec,CNT=01, ERR=09,VERR=verr;",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test291")) {
			// test291
			dataAppl = String.format(
					"RSP-CRV:,%s,%s:::DENIED,01::ID=%s,RO=%s,CRO=cro,NUM=%s,RED=red,"
							+ "RET=ret,STAT=stat,APP=app:CNT=01:ERR=08,VERR=verr;",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test292")) {
			// test292
			dataAppl = String.format(
					"RSP-CRV:,%s,%s:::DENIED,01::ID=%s,RO=%s,CRO=cro,NUM=%s,RED=red,"
							+ "RET=ret,STAT=stat,APP=app:CNT=01:ERR=08,VERR=verr;",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test297")) {
			dataAppl = String.format(
					"RSP-CRQ:,%s,%s:::COMPLD,11::ID=%s,RO=%s,NUM=%s,RED=red,RET=ret,"
							+ "RONUM=ronum,MORE=more:CNT=cnt:ED=ed,ET=et:%s:CNT=01:ERR=09,VERR=\"You are Involved\";",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
							sCr });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else if (sDestNodeName.equals("test302")) {
			dataAppl = String.format(
					"RSP-CRQ:,%s,%s:::COMPLD,11::ID=%s,RO=%s,NUM=%s,RED=red,RET=ret,"
							+ "RONUM=ronum,MORE=more:CNT=cnt:ED=ed,ET=et:%s:CNT=01:ERR=10,VERR=verr;",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
							sCr });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		} else {
			dataAppl = String.format(
					"RSP-CRQ:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,RED=red,RET=ret,"
							+ "RONUM=ronum,MORE=more:CNT=cnt:ED=ed,ET=et:%s;",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
							sCr });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		}
	}

	private void handleNumReqScp(ChannelHandlerContext ctx, YlDcmMsg obj, String dataAppl, String[] dataList, int count,
			String sNumList) {
		System.out.println(
				"----read asn.1 message from " + new String(obj.tliHdr.destNodeName.getValue()) + ":>>" + dataAppl);
		String[] dataInfo6 = Function.MySplit(dataList[6], ",", "\"");
		String[] datetimeInfo1 = Function.MySplit(dataList[1], ",", "\"");
		String sId = "";
		String sRo = "";
		String sNum = "";
		String sEd = "";
		String sEt = "";
		String sCritical = "";
		String sCnt = "", sScp = "", sDate = "", sTime = "", sAc = "";
		sDate = datetimeInfo1[1];
		sTime = datetimeInfo1[2];
		String sDestNodeName = new String(obj.tliHdr.destNodeName.getValue());
		List<ErrClass> errList = new ArrayList<ErrClass>();
		List<String> scpList = new ArrayList<String>();
		ErrClass err = Function.isDatetime(sDate, sTime);
		if (err != null) {
			errList.add(err);
		}

		if (dataList.length > 0 && !dataList[dataList.length - 1].endsWith(";")) {
			ErrClass errClass1 = new ErrClass();
			errClass1.setERR("0001");
			errClass1.setVERR(";REQUIRED");
			errList.add(errClass1);
		}

		// for data6
		for (int j6 = 0; j6 < dataInfo6.length; j6++) {
			String s6 = dataInfo6[j6].trim();
			String[] d6 = Function.MySplit(s6, "=", "\"");
			if (s6.indexOf("ID=") == 0) {
				sId = d6[1];
			} else if (s6.indexOf("RO=") == 0) {
				sRo = d6[1];
			} else if (s6.indexOf("AC=") == 0) {
				sAc = d6[1];
			} else if (s6.indexOf("NUM=") == 0) {
				sNum = d6[1].replace(";", "");
				if (!Function.isNumeric(sNum)) {
					ErrClass errNumClass = new ErrClass();
					errNumClass.setERR("1000");
					errNumClass.setVERR("NUM:" + sNum);
					errList.add(errNumClass);
				}
			} else if (s6.indexOf("ED=") == 0) {
				sEd = d6[1].replace(";", "");
				if (getSubStrCount(s6, "\"") == 1) {
					ErrClass errClass = new ErrClass();
					errClass.setERR("0005");
					errClass.setVERR("ED:" + sEd);
					errList.add(errClass);
				} else if (!isEd(sEd)) {
					ErrClass errClass = new ErrClass();
					errClass.setERR("1001");
					errClass.setVERR("ED:" + sEd);
					errList.add(errClass);
				}
			} else if (s6.indexOf("ET=") == 0) {
				sEt = d6[1].replace(";", "");
			} else if (s6.indexOf("CRITICAL=") == 0) {
				sCritical = d6[1].replace(";", "");
			} else {
				ErrClass errClass = new ErrClass();
				errClass.setERR("03");
				if (d6.length > 1) {
					errClass.setVERR(String.format("%s=%s", d6[0].replace(";", ""), d6[1].replace(";", "")));
				} else {
					errClass.setVERR(s6);
				}
				errList.add(errClass);
			}
		}
		// get cnt and scpid list
		for (int j = 0; j < dataList.length; j++) {
			String ss = dataList[j].trim();
			String[] dataInfo2 = Function.MySplit(ss, "=", "\"");
			if (ss.indexOf("CNT=") == 0) {
				sCnt = dataInfo2[1].replace(";", "");
				int iCnt = Integer.parseInt(sCnt);
				if (iCnt < 1000) {
					for (int jcnt = 0; jcnt < iCnt; jcnt++) {
						if (dataList[j + 1 + jcnt].trim().startsWith("SCP=")) {
							String sTemp = dataList[j + 1 + jcnt].trim().replace("SCP=", "");
							scpList.add(sTemp);
						}
					}
				}
			}
		}

		// check id
		if (sId.equals("")) {
			ErrClass errClass2 = new ErrClass();
			errClass2.setERR("02");
			errClass2.setVERR("ID:MISSING");
			errList.add(errClass2);
		} else if (H2Data.ExecuteScalar("select count(*) as cc from USERINFO where id='" + sId + "'").equals("0")) {
			ErrClass errClass2 = new ErrClass();
			errClass2.setERR("0101");
			errClass2.setVERR("ID:" + sId);
			errList.add(errClass2);
		}
		// check CR
		ErrClass errNumExisted = isNumExisted(sNum);
		if (errNumExisted != null) {
			errList.add(errNumExisted);
		}
		if (sAc.equals("R")) {
			ErrClass errScpExisted = isScpExisted(sNum, scpList);
			if (errScpExisted != null) {
				errList.add(errScpExisted);
			}
		} else if (sAc.equals("D")) {
			err = isScpExisted(sNum, scpList);
			if (err != null) {
				errList.add(err);
			}
		}

		// error
		if (errList.size() > 0) {
			dataAppl = String.format("RSP-SCP:,%s,%s:::DENIED,01::ID=%s,RO=%s,CRO=cro,NUM=%s,ED=%s,ET=%s",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
							sEd, sEt });
			dataAppl += ":CNT=" + String.format("%02d", errList.size());
			for (int iList = 0; iList < errList.size(); iList++) {
				ErrClass ec = errList.get(iList);
				dataAppl += ":ERR=" + ec.getERR() + ",VERR=\"" + ec.getVERR() + "\"";
			}
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			return;
		}

		if (sAc.equals("D")) {
			dataAppl = String.format(
					"RSP-SCP:,%s,%s:::COMPLD,00::ID=%s,RO=%s,CRO=cro,NUM=%s,ED=%s,ET=%s,CRSTAT=%s:"
							+ "%s:LCNT=lcnt:LED=led,LET=let:LSTAT=lstat;",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
							sEd, sEt, getCrStatForScp(sNum), getScpInfo(sNum, scpList) });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			for (int k = 0; k < scpList.size(); k++) {
				H2Data.executeUpdate(String.format("delete from CRSCP where NUM='%s' and SCP='%s' ", sNum,scpList.get(k)));
				dataAppl = String.format("UNS-SCP:,%s,%s:::::RO=%s,CRO=cro,NUM=%s,ED=%s,ET=%s:SCP=%s,RES=07;",
						new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sRo, sNum, sEd,
								sEt, scpList.get(k) });
				obj.dataAppl.setValue(dataAppl.getBytes());
				ctx.writeAndFlush(obj);
			}
			return;
		} else if (sAc.equals("R")) {
			dataAppl = String.format(
					"RSP-SCP:,%s,%s:::COMPLD,00::ID=%s,RO=%s,CRO=cro,NUM=%s,ED=%s,ET=%s,CRSTAT=%s:"
							+ "%s:LCNT=lcnt:LED=led,LET=let:LSTAT=lstat;",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
							sEd, sEt, getCrStatForScp(sNum), getScpInfo(sNum, scpList) });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			for (int k = 0; k < scpList.size(); k++) {
				dataAppl = String.format("UNS-SCP:,%s,%s:::::RO=%s,CRO=cro,NUM=%s,ED=%s,ET=%s:SCP=%s,RES=01;",
						new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sRo, sNum, sEd,
								sEt, scpList.get(k) });
				obj.dataAppl.setValue(dataAppl.getBytes());
				ctx.writeAndFlush(obj);
			}
			return;
		} else if (sAc.equals("A")) {
			dataAppl = String.format(
					"RSP-SCP:,%s,%s:::COMPLD,00::ID=%s,RO=%s,CRO=cro,NUM=%s,ED=%s,ET=%s,CRSTAT=%s:"
							+ "%s:LCNT=lcnt:LED=led,LET=let:LSTAT=lstat;",
					new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sId, sRo, sNum,
							sEd, sEt, getCrStatForScp(sNum), getScpInfo(sNum, scpList) });
			obj.dataAppl.setValue(dataAppl.getBytes());
			ctx.writeAndFlush(obj);
			for (int k = 0; k < scpList.size(); k++) {
				String sTemp = H2Data.ExecuteScalar(String.format("select count(*) as cc from CRSCP where NUM='%s' and SCP='%s' ", sNum,scpList.get(k)));
				if (sTemp.equals("0")) {
				dataAppl = String.format("UNS-SCP:,%s,%s:::::RO=%s,CRO=cro,NUM=%s,ED=%s,ET=%s:SCP=%s,RES=23;",
						new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sRo, sNum, sEd,
								sEt, scpList.get(k) });
				} else {
					dataAppl = String.format("UNS-SCP:,%s,%s:::::RO=%s,CRO=cro,NUM=%s,ED=%s,ET=%s:SCP=%s,RES=14;",
							new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), sRo, sNum, sEd,
									sEt, scpList.get(k) });
					}
				obj.dataAppl.setValue(dataAppl.getBytes());
				ctx.writeAndFlush(obj);
			}
			return;
		} else {
			// dataAppl = String.format(
			// "RSP-CRQ:,%s,%s:::COMPLD,00::ID=%s,RO=%s,NUM=%s,RED=red,RET=ret,"
			// + "RONUM=ronum,MORE=more:CNT=cnt:ED=ed,ET=et:%s;",
			// new Object[] { YlUtil.dateToString(new Date()),
			// YlUtil.timeToString(new Date()), sId, sRo, sNum,
			// sCr });
			// obj.dataAppl.setValue(dataAppl.getBytes());
			// ctx.writeAndFlush(obj);
			// return;
		}
	}

	public String[] getRSpareNumbers(String sNum, int count) {
		sNum = sNum.replace(";", "");
		if (sNum.indexOf("*") < 0)
			return sNum.split(",");
		String numbers = "";
		Random rand = new Random();

		for (int k = 0; k < count; k++) {
			char c[] = sNum.toCharArray();
			for (int i = 0; i < c.length; i++) {
				if (c[i] == '*') {
					int j = rand.nextInt(9) + 48;
					c[i] = (char) j;
				} else if (c[i] == '&') {
					int j = rand.nextInt(9) + 48;
					c[i] = (char) j;
				} else if (c[i] == 'N') {
				}
			}
			numbers = numbers + String.format("%s,", new String(c));
		}

		numbers = numbers.substring(0, numbers.length() - 1);
		return numbers.split(",");
	}

	public String[] getSpareNumbers(String sNpa, String sNxx, String sLine, String sNum, int count) {
		if (!sNum.equals(""))
			return getSpareNumbers(sNum, count);

		String numbers = "";
		Random rand = new Random();
		for (int j = 0; j < count; j++) {
			String sNpaNew = sNpa;
			String sNxxNew = sNxx;
			String sLineNew = sLine.replace(";", "");
			if (sNpaNew.equals("")) {
				int k = rand.nextInt(899) + 100;
				sNpaNew = String.format("%d", k);
			}
			if (sNxxNew.equals("")) {
				int k = rand.nextInt(899) + 100;
				sNxxNew = String.format("%d", k);
			}

			if (sLineNew.equals("")) {
				int k = rand.nextInt(8999) + 1000;
				numbers = numbers + String.format("%s%s%d,", sNpaNew, sNxxNew, k);
			} else {
				numbers = numbers + String.format("%s%s%s,", sNpaNew, sNxxNew, sLineNew);
			}
		}
		numbers = numbers.substring(0, numbers.length() - 1);
		return numbers.split(",");
	}

	public String[] getSpareNumbers(String sNum, int count) {
		sNum = sNum.replace(";", "");
		String numbers = "";
		Random rand = new Random();

		for (int k = 0; k < count; k++) {
			char c[] = sNum.toCharArray();
			for (int i = 0; i < c.length; i++) {
				if (c[i] == '*') {
					int j = rand.nextInt(9) + 48;
					c[i] = (char) j;
				} else if (c[i] == '&') {
					int j = rand.nextInt(9) + 48;
					c[i] = (char) j;
				} else if (c[i] == 'N') {
				}
			}
			numbers = numbers + String.format("%s,", new String(c));
		}

		numbers = numbers.substring(0, numbers.length() - 1);
		return numbers.split(",");
	}

	public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
		ctx.flush();
	}

	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		channelGroup.remove(ctx.channel());
		super.channelInactive(ctx);
		ctx.close();
	}

	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		logger.log(Level.SEVERE, null, "exceptionCaught锟斤拷" + cause.getMessage());
		// saved Queuing Messages When Link is Down
		InetSocketAddress insocket = (InetSocketAddress) ctx.channel().remoteAddress();
		String clientIP = insocket.getAddress().getHostAddress();
		System.out.println("test:saved Queuing Messages When " + clientIP + " Link is Down");
		// save to memory table
		if (this.lastSendMessage != null) {
			if (String.valueOf(this.lastSendMessage.dataAppl.getValue()).indexOf("Queuing=Yes") >= 0) {
				H2Data.executeUpdate(
						"INSERT INTO QueMsg (ip,tliHdr_version,tliHdr_priority,tliHdr_messageId,tliHdr_messageCode"
								+ ",tliHdr_destNodeName,tliHdr_srcNodeName,tliHdr_errorCode,dataAppl) " + " VALUES('"
								+ clientIP + "','" + String.valueOf(this.lastSendMessage.tliHdr.version.getValue())
								+ "','" + String.valueOf(this.lastSendMessage.tliHdr.priority.getValue()) + "','"
								+ String.valueOf(this.lastSendMessage.tliHdr.messageId.getValue()) + "','"
								+ String.valueOf(this.lastSendMessage.tliHdr.messageCode.getValue()) + "','"
								+ String.valueOf(this.lastSendMessage.tliHdr.destNodeName.getValue()) + "','"
								+ String.valueOf(this.lastSendMessage.tliHdr.srcNodeName.getValue()) + "','"
								+ String.valueOf(this.lastSendMessage.tliHdr.errorCode.getValue()) + "','"
								+ String.valueOf(this.lastSendMessage.dataAppl.getValue()) + "')");
			}
		}
		ctx.close();
	}

	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		System.out.println("==============channelActive==============");
		channelGroup.add(ctx.channel());
		String srcNodeName = "Server Message";
		String destNodeName = "";

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.messageId.setValue("ABCDEFG".getBytes());
		obj.tliHdr.messageCode.setValue(1L);
		obj.tliHdr.destNodeName.setValue(destNodeName.getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);

		obj.dataAppl.setValue(new byte[] { 1, 2, 3 });
		try {
			ctx.write(obj);
			ctx.flush();
			System.out.println("----write asn.1 message:GD");
		} catch (Exception e) {
			e.printStackTrace();
		}
		// send all queue messages
		InetSocketAddress insocket = (InetSocketAddress) ctx.channel().remoteAddress();
		String clientIP = insocket.getAddress().getHostAddress();
		ResultSet rs = H2Data.executeQuery("SELECT * FROM QueMsg where ip='" + clientIP + "'");
		// 锟斤拷锟斤拷锟斤拷锟斤拷锟�
		while (rs.next()) {
			obj = new YlDcmMsg();
			obj.tliHdr.version.setValue(Long.parseLong(rs.getString("tliHdr_version")));
			obj.tliHdr.priority.setValue(Integer.parseInt(rs.getString("tliHdr_priority")));
			obj.tliHdr.messageId.setValue(rs.getString("tliHdr_messageId").getBytes());
			obj.tliHdr.messageCode.setValue(Long.parseLong(rs.getString("tliHdr_messageCode")));
			obj.tliHdr.destNodeName.setValue(rs.getString("tliHdr_destNodeName").getBytes());
			obj.tliHdr.srcNodeName.setValue(rs.getString("tliHdr_srcNodeName").getBytes());
			obj.tliHdr.errorCode.setValue(Long.parseLong(rs.getString("tliHdr_errorCode")));

			obj.dataAppl.setValue(rs.getString("dataAppl").getBytes());
			try {
				ctx.write(obj);
				ctx.flush();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		H2Data.executeUpdate("DELETE FROM QueMsg where ip='" + clientIP + "'");
		System.out.println("----write queuing messages");

	}

	public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
		if ((evt instanceof IdleStateEvent)) {
			IdleStateEvent event = (IdleStateEvent) evt;
			String type = "";
			if (event.state() == IdleState.READER_IDLE) {
				type = "server handler2 handle get read idle";

				if ((this.lastSendMessage != null) && (this.lastSendMessage.tliHdr.messageCode.getValue() == 19)) {
					ctx.writeAndFlush(this.lastSendMessage);

					return;
				}
			} else if (event.state() == IdleState.WRITER_IDLE) {
				type = "server handler2 handle get write idle";
			} else if (event.state() == IdleState.ALL_IDLE) {
				type = "server handler2 handle get all idle";
			}
			System.out.println(ctx.channel().remoteAddress() + " timeout type:" + type);
		}

		super.userEventTriggered(ctx, evt);
	}


	public static int getSubStrCount(String string, String a) {
		int i = string.length() - string.replace(a, "").length();
		i /= a.length();
		return i;
	}

	public static boolean isEd(String date_string) {
		date_string = date_string.replace("\"", "");
		if (date_string.equals("NOW")) {
			return true;
		}
		if (date_string.equals("ed")) {
			return true;
		}
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
		boolean dateflag = true;
		// 锟斤拷锟斤拷要锟斤拷锟斤拷一锟斤拷锟届常锟斤拷息
		try {
			Date date = format.parse(date_string);
		} catch (Exception e) {
			dateflag = false;
		} finally {
			// 锟缴癸拷锟斤拷true ;失锟斤拷:false
		}
		return dateflag;
	}

	public static boolean isLn(String sLn) {

		if (sLn.equals("No Listing Name Provided")) {
			return true;
		} else if (sLn.equals("ln")) {
			return true;
		} else if (sLn.indexOf("\"") < 0) {
			return false;
		} else
			return true;
	}

	public static boolean isLi(String sLi) {
		if (sLi.equals("")) {
			return true;
		} else if (sLi.indexOf("X") >= 0) {
			return false;
		} else
			return true;
	}

	public static boolean isHdd(String sHdd) {
		if (sHdd.equals("")) {
			return true;
		} else if (sHdd.indexOf("X") >= 0) {
			return false;
		} else
			return true;
	}

	public static boolean isSf(String sSf) {
		if (sSf.equals("")) {
			return true;
		} else if (sSf.indexOf("*") >= 0) {
			return false;
		} else
			return true;
	}

	public static ErrClass isNode(String sNode) {
		String NodeType[] = { "TI", "DT", "DA", "LT", "ST", "AC", "NX", "SW", "PC", "CA", "AN", "TE", "SD", "TD" };
		List<String> list = Arrays.asList(NodeType);
		String[] valueList = sNode.replace("\"", "").split(",");
		int j = valueList.length;
		ErrClass errClass = null;
		if (sNode.equals("")) {
			return errClass;
		} else {
			for (int i = 1; i < valueList.length; i++) {
				if (list.contains(valueList[i])) {
					//
				} else {
					errClass = new ErrClass();
					errClass.setERR("1073");
					errClass.setVERR("BAD NOD TYPE:" + valueList[i]);
					return errClass;
				}
			}
			String ss = valueList[j - 1];
			if (ss.equals("SD")) {
				errClass = new ErrClass();
				errClass.setERR("1093");
				errClass.setVERR("SD - NO LAST NODE");
			}
			return errClass;
		}
	}

	public static ErrClass isCnt12Type(String sType) {
		String Cnt12Type[] = { "AC", "LT", "DT", "ST", "NX", "TI", "SD", "TD", "TE" };
		List<String> list = Arrays.asList(Cnt12Type);
		String[] valueList = sType.split(",");
		ErrClass errClass = null;
		if (sType.equals("")) {
			return errClass;
		} else {
			for (int i = 1; i < valueList.length; i++) {
				if (list.contains(valueList[i])) {
					//
				} else {
					errClass = new ErrClass();
					errClass.setERR("1108");
					errClass.setVERR("BAD LBL TYP:" + valueList[i]);
					return errClass;
				}
			}
			return errClass;
		}
	}

	public static ErrClass isCnt12Lbl(String sLbl) {
		ErrClass errClass = null;
		sLbl = sLbl.replace("\"", "");
		if (sLbl.startsWith("*")) {
			return errClass;
		} else {
			errClass = new ErrClass();
			errClass.setERR("1085");
			errClass.setVERR("LAD LBL:" + sLbl);
			return errClass;
		}
	}

	public static ErrClass isRstat(String sRstat) {
		ErrClass errClass = null;
		sRstat = sRstat.replace("\"", "");
		if (sRstat.equals("03")) {
			return errClass;
		} else if (sRstat.equals("04")) {
			return errClass;
		} else if (sRstat.equals("07")) {
			return errClass;
		} else {
			errClass = new ErrClass();
			errClass.setERR("17");
			errClass.setVERR("verr");
			return errClass;
		}
	}

	public static ErrClass isCnt12Def(String sType, String sDef) {
		ErrClass errClass = null;
		List<String> cnt12DefList = new ArrayList<String>();

		if (sDef.equals("")) {
			return errClass;
		} else {
			String[] list = sDef.split(",");
			boolean bOk = true;
			for (int i = 1; i < list.length; i++) {
				if (cnt12DefList.contains(list[i])) {
					bOk = false;
					errClass = new ErrClass();
					errClass.setERR("1092");
					errClass.setVERR("DUP:" + list[i]);
					break;
				} else {
					cnt12DefList.add(list[i]);
				}
				int k = list[i].length();
				if (sType.equals("TE") && (k != 6 && k != 10)) {
					errClass = new ErrClass();
					errClass.setERR("1107");
					errClass.setVERR("verr");
				}
			}
			return errClass;
		}
	}

	public static ErrClass isCnt13(String sLbl, String sCnt13, List<String> list) {
		ErrClass errClass = null;
		if (sCnt13.equals("")) {
			return errClass;
		} else {
			for (int i = 0; i < list.size(); i++) {
				if (list.contains(sCnt13)) {
					errClass = new ErrClass();
					errClass.setERR("9004");
					errClass.setVERR(sLbl + ":" + sCnt13);
					break;
				}
			}
			return errClass;
		}
	}

	public static ErrClass isNumExisted(String sNum) {
		ErrClass errNum = null;
		if (!sNum.equals("") && !H2Data
				.ExecuteScalar("select count(*) as cc from CustomerRecord where NUM='" + sNum + "'").equals("0")) {
			errNum = new ErrClass();
			errNum.setERR("07");
			errNum.setVERR("NO REC");
		}
		return errNum;
	}

	public static ErrClass isScpExisted(String sNum, List<String> scpList) {
		ErrClass errNum = null;
		for (int i = 0; i < scpList.size(); i++) {
			if (!sNum.equals("") && !H2Data.ExecuteScalar(
					"select count(*) as cc from CRSCP where NUM='" + sNum + "' and SCP='" + scpList.get(i) + "'")
					.equals("0")) {
				errNum = new ErrClass();
				errNum.setERR("25");
				errNum.setVERR("VERR");
				break;
			}
		}
		return errNum;
	}

	public static String getScpInfo(String sNum, List<String> scpList) {
		String sTemp = "";
		int iCount = 0;
		for (int i = 0; i < scpList.size(); i++) {
			ResultSet rs = H2Data
					.executeQuery("SELECT * FROM CRSCP where NUM='" + sNum + "' and SCP='" + scpList.get(i) + "'");

			// only one record
			try {
				while (rs.next()) {
					iCount++;
					sTemp += String.format(":SCP=%s,SCPSTAT=%s,INAOS=%s", rs.getString("SCP"), rs.getString("SCPSTAT"),
							rs.getString("INAOS"));
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (iCount > 0) {
			sTemp = String.format("SCNT=%2d", iCount);
		} else {
			sTemp = "";
		}
		return sTemp;
	}

	public static ErrClass isRstat(String sRstat, String sNum) {
		ErrClass errNum = null;
		sRstat = getRstatValue(sRstat);
		if (!sNum.equals("") && !H2Data
				.ExecuteScalar(
						"select count(*) as cc from CustomerRecord where NUM='" + sNum + "' and STAT='" + sRstat + "'")
				.equals("0")) {
			errNum = new ErrClass();
			errNum.setERR("08");
			errNum.setVERR("verr");
		}
		return errNum;
	}

	public static ErrClass isCnt11V(String sV1st, List<String> v1list, String sAc, String sNum) {
		ErrClass errClass = null;
		String[] list = sV1st.split(",");
		if (!list[0].equals("")) {
			if (v1list.contains(list[0] + "," + list[1])) {
				errClass = new ErrClass();
				errClass.setERR("4724");
				errClass.setVERR("");
				return errClass;
			} else if (!list[1].equals("OTHER") && !list[1].startsWith(list[0])) {
				errClass = new ErrClass();
				errClass.setERR("4729");
				errClass.setVERR("");
				return errClass;
			} else if (sAc.equals("C")) {
				// check listed on the CAD
				errClass = isExistedCad(list[2], sNum);
				if (errClass != null)
					return errClass;
				// check listed on the lad data
				errClass = isExistedLad(list[1], sNum);
				if (errClass != null)
					return errClass;
				// check been purchased
				if (list[1].equals("215")) {
					errClass = new ErrClass();
					errClass.setERR("4705");
					errClass.setVERR("");
					return errClass;
				}
				return errClass;

			} else
				return errClass;
		} else {
			errClass = new ErrClass();
			errClass.setERR("1106");
			errClass.setVERR("1ST COL IS BLANK");
			return errClass;
		}
	}

	public static ErrClass isExistedAlbl(String sAlbl, String sNum) {
		ErrClass errClass = null;
		String sql = "select count(*) as cc from CRCNT3 where NUM<>'" + sNum + "' and ALBL='" + sAlbl + "'";
		if (!H2Data.ExecuteScalar(sql).equals("0")) {
			errClass = new ErrClass();
			errClass.setERR("1122");
			errClass.setVERR("ALBL:NOT ALLOWED");
		}
		return errClass;
	}

	public static ErrClass isExistedCad(String sTel, String sNum) {
		ErrClass errClass = null;
		String sql = "select count(*) as cc from CRCNT9 where NUM='" + sNum + "' and TEL='" + sTel + "'";
		if (H2Data.ExecuteScalar(sql).equals("0")) {
			errClass = new ErrClass();
			errClass.setERR("4711");
			errClass.setVERR("");
		}
		return errClass;
	}

	public static ErrClass isExistedLad(String sLad, String sNum) {
		ErrClass errClass = null;
		if (sLad.startsWith("*SIX")) {
			String sql = "select count(*) as cc from CRCNT12 where NUM='" + sNum + "' and LBL='" + sLad + "'";
			if (H2Data.ExecuteScalar(sql).equals("0")) {
				errClass = new ErrClass();
				errClass.setERR("4706");
				errClass.setVERR("");
			}
		}
		return errClass;
	}

	public static ErrClass isCnt11All(List<String> list) {
		ErrClass errClass = null;
		for (int i = 0; i < list.size(); i++) {
			String sTemp = list.get(i);
			String sNxx = sTemp.split(",")[0];
			boolean bOk = false;
			for (int j = 0; j < list.size(); j++) {
				if (list.get(j).startsWith(sNxx + ",OTHER,")) {
					bOk = true;
					break;
				}
			}
			if (!bOk) {
				errClass = new ErrClass();
				errClass.setERR("4714");
				errClass.setVERR("");
				break;
			}
		}
		return errClass;
	}

	public static String getCnt8Record(String sNum) {
		String sCnt8 = "";
		ResultSet rs = H2Data.executeQuery("SELECT * FROM CRCNT8 where NUM='" + sNum + "'");
		int iCount = 0;
		// only one record
		try {
			while (rs.next()) {
				iCount++;
				sCnt8 += ":LN=" + rs.getString("LN");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (iCount > 0) {
			sCnt8 = String.format("CNT8:%2d", iCount);
		}
		return sCnt8;
	}

	public static String getCnt9Record(String sNum) {
		String sCnt9 = "";
		ResultSet rs = H2Data.executeQuery("SELECT * FROM CRCNT9 where NUM='" + sNum + "'");
		int iCount = 0;
		// only one record
		try {
			while (rs.next()) {
				iCount++;
				sCnt9 += ":TEL=" + rs.getString("TEL");
				sCnt9 += ",LNS=" + rs.getString("LNS");
				sCnt9 += ",LSO=" + rs.getString("LSO");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (iCount > 0) {
			sCnt9 = String.format("CNT9:%2d", iCount);
		}
		return sCnt9;
	}

	public static String getCnt11Record(String sNum) {
		String sCnt11 = "";
		ResultSet rs = H2Data.executeQuery("SELECT * FROM CRCNT11 where NUM='" + sNum + "'");
		int iCount = 0;
		// only one record
		try {
			while (rs.next()) {
				iCount++;
				sCnt11 += ":V=" + rs.getString("V");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (iCount > 0) {
			sCnt11 = String.format("CNT11:%2d", iCount);
		}
		return sCnt11;
	}

	public static String getCnt12Record(String sNum) {
		String sCnt12 = "";
		ResultSet rs = H2Data.executeQuery("SELECT * FROM CRCNT12 where NUM='" + sNum + "'");
		int iCount = 0;
		// only one record
		try {
			while (rs.next()) {
				iCount++;
				sCnt12 += String.format("TYPE=%s,LBL=\"%s\",DEF=\"%s\"", rs.getString("TYPE"), rs.getString("LBL"),
						rs.getString("DEF"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (iCount > 0) {
			sCnt12 = String.format("CNT12:%2d", iCount);
		}
		return sCnt12;
	}

	public static String getCnt14Record(String sNum) {
		String sCnt14 = "";
		ResultSet rs = H2Data.executeQuery("SELECT * FROM CRCNT14 where NUM='" + sNum + "'");
		int iCount = 0;
		// only one record
		try {
			while (rs.next()) {
				iCount++;
				sCnt14 += String.format("OTH=%s", rs.getString("OTH"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (iCount > 0) {
			sCnt14 = String.format("CNT14:%2d", iCount);
		}
		return sCnt14;
	}

	public static String getCustomerRecord(String sNum) {
		String sCr = "";
		ResultSet rs = H2Data.executeQuery("SELECT * FROM CustomerRecord where NUM='" + sNum + "'");
		// only one record
		try {
			if (rs.next()) {
				sCr += "STAT=" + getCrStat(rs.getString("STATUS")) + ",APP=app";
				sCr += ":IEC=" + rs.getString("IEC");
				sCr += ":IAC=" + rs.getString("IAC");
				sCr += ":ABN=" + rs.getString("ABN");
				sCr += ",DAU=" + rs.getString("DAU");
				sCr += ",DAT=" + rs.getString("DAT");
				sCr += ",RCC=rcc";
				sCr += ",DD=" + rs.getString("DD");
				sCr += ",HDD=" + rs.getString("HDD");
				sCr += ",LI=" + rs.getString("LI");
				sCr += ",RAO=" + rs.getString("RAO");
				sCr += ",SO=" + rs.getString("SO");
				sCr += ",TELCO=" + rs.getString("TELCO");
				sCr += ",NCON=" + rs.getString("NCON");
				sCr += ",CTEL=" + rs.getString("CTEL");
				sCr += ",RED=RED,RET=RET";
				sCr += ":AAC=" + rs.getString("AAC");
				sCr += ":ASTA=" + rs.getString("ASTA");
				// cnt8
				String sCnt8 = getCnt8Record(sNum);
				if (!sCnt8.equals("")) {
					sCr += ":" + sCnt8;
				}
				// cnt9
				String sCnt9 = getCnt9Record(sNum);
				if (!sCnt9.equals("")) {
					sCr += ":" + sCnt9;
				}
				// node
				sCr += ":NODE=" + rs.getString("NODE");
				// cnt11
				String sCnt11 = getCnt11Record(sNum);
				if (!sCnt11.equals("")) {
					sCr += ":" + sCnt11;
				}
				// cnt12
				String sCnt12 = getCnt12Record(sNum);
				if (!sCnt12.equals("")) {
					sCr += ":" + sCnt12;
				}
				// cnt14
				String sCnt14 = getCnt14Record(sNum);
				if (!sCnt14.equals("")) {
					sCr += ":" + sCnt14;
				}
				sCr += ":Z=z,DS=ds";
				sCr += ":PEC=" + rs.getString("PEC");
				sCr += ":PAC=" + rs.getString("PAC");
				// sCr += ";";

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return sCr;
	}

	public static String getCrStat(String sStatus) {
		if (sStatus.equals("SAVED")) {
			return "01";
		} else if (sStatus.equals("PENDING")) {
			return "02";
		} else if (sStatus.equals("SENDING")) {
			return "03";
		} else if (sStatus.equals("ACTIVE")) {
			return "04";
		} else if (sStatus.equals("OLD")) {
			return "05";
		} else if (sStatus.equals("INVALID")) {
			return "06";
		} else if (sStatus.equals("DISCONNECT")) {
			return "07";
		} else if (sStatus.equals("MUST CHECK")) {
			return "08";
		} else if (sStatus.equals("FAILED")) {
			return "09";
		} else if (sStatus.equals("HOLD")) {
			return "10";
		} else {
			return sStatus;
		}
	}

	public static String getCrStatForScp(String sNum) {
		if (sNum.equals("")) {
			return "04";
		}
		ResultSet rs = H2Data.executeQuery("SELECT STATUS FROM CustomerRecord where NUM='" + sNum + "'");
		String sCr = "";
		try {
			if (rs.next()) {
				sCr = rs.getString("STATUS");
				if (sCr.equals("SENDING")) {
					return "01";
				} else if (sCr.equals("ACTIVE")) {
					return "02";
				} else if (sCr.equals("DISCONNECT")) {
					return "03";
				} else if (sCr.equals("FAILED")) {
					return "04";
				} else {
					return "04";
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "04";
	}

	public static String getRstatValue(String sRstat) {
		if (sRstat.equals("03")) {
			return "SENDING";
		} else if (sRstat.equals("04")) {
			return "ACTIVE";
		} else if (sRstat.equals("07")) {
			return "DISCONNECT";
		} else {
			return sRstat;
		}
	}

}