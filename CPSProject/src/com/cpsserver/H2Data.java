package com.cpsserver;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;
import org.h2.tools.Server;

public class H2Data {
	// 锟斤拷锟捷匡拷锟斤拷锟斤拷URL锟斤拷通锟斤拷使锟斤拷TCP/IP锟侥凤拷锟斤拷锟斤拷模式锟斤拷远锟斤拷锟斤拷锟接ｏ拷锟斤拷锟斤拷前锟斤拷锟接碉拷锟斤拷锟节达拷锟斤拷锟斤拷锟絞acl锟斤拷锟捷匡拷
	private static final String JDBC_URL = "jdbc:h2:tcp://localhost:9001/mem:gacl";
	// 锟斤拷锟斤拷锟斤拷锟捷匡拷时使锟矫碉拷锟矫伙拷锟斤拷
	private static final String USER = "sa";
	// 锟斤拷锟斤拷锟斤拷锟捷匡拷时使锟矫碉拷锟斤拷锟斤拷
	private static final String PASSWORD = "";
	// 锟斤拷锟斤拷H2锟斤拷锟捷匡拷时使锟矫碉拷锟斤拷锟斤拷锟洁，org.h2.Driver锟斤拷锟斤拷锟斤拷锟斤拷锟紿2锟斤拷锟捷匡拷锟皆硷拷锟结供锟侥ｏ拷锟斤拷H2锟斤拷锟捷匡拷锟絡ar锟斤拷锟叫匡拷锟斤拷锟揭碉拷
	private static final String DRIVER_CLASS = "org.h2.Driver";
	private static Connection conn = null;
	private static Statement stmt = null;
	private static String error_str = "";
	private static Server server;

	public static boolean InitDB() {
		// 锟斤拷锟斤拷H2锟斤拷锟捷匡拷锟斤拷锟斤拷''
		try {
			try {
				// start the TCP Server
				server = Server.createTcpServer(new String[] { "-tcpPort", "9001" }).start();
			} catch (Exception e) {
				// TODO Auto-generated catch block
//				e.printStackTrace();
			}
			Class.forName(DRIVER_CLASS);
			// 锟斤拷锟斤拷锟斤拷锟斤拷URL锟斤拷锟矫伙拷锟斤拷锟斤拷锟斤拷锟斤拷锟饺★拷锟斤拷菘锟斤拷锟斤拷锟�
			conn = DriverManager.getConnection(JDBC_URL, USER, PASSWORD);
			stmt = conn.createStatement();
			// create message table
			CreateTables();
			System.out.println("InitDB successfully!\r\n");
			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			return false;
		}
	}

	// for client
	public static boolean InitClientDB() {
		// 锟斤拷锟斤拷H2锟斤拷锟捷匡拷锟斤拷锟斤拷''
		try {
			try {
				// start the TCP Server
				server = Server.createTcpServer(new String[] { "-tcpPort", "9001" }).start();
			} catch (Exception e) {
				// TODO Auto-generated catch block
//				e.printStackTrace();
			}
			Class.forName(DRIVER_CLASS);
			// 锟斤拷锟斤拷锟斤拷锟斤拷URL锟斤拷锟矫伙拷锟斤拷锟斤拷锟斤拷锟斤拷锟饺★拷锟斤拷菘锟斤拷锟斤拷锟�
			conn = DriverManager.getConnection(JDBC_URL, USER, PASSWORD);
			stmt = conn.createStatement();
			// create message table
			CreateClientTables();
			System.out.println("Init Client DB successfully!\r\n");
			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			return false;
		}
	}

	public static void CreateTables() {
		execute("DROP TABLE IF EXISTS QueMsg");
		execute("CREATE TABLE QueMsg(id IDENTITY PRIMARY KEY,tliHdr_version VARCHAR(100),tliHdr_priority VARCHAR(20)"
				+ ",ip VARCHAR(50)"
				+ ",tliHdr_messageId VARCHAR(12),tliHdr_messageCode VARCHAR(100),tliHdr_destNodeName VARCHAR(51)"
				+ ",tliHdr_srcNodeName VARCHAR(51),tliHdr_errorCode VARCHAR(20),dataAppl VARCHAR(5000)" + ")");
		execute("CREATE TABLE NumberInfo(number varchar(15) PRIMARY KEY,status VARCHAR(20),addtime datetime " + ")");
		// table CustomerRecord
		execute("DROP TABLE IF EXISTS CustomerRecord");
		execute("CREATE TABLE CustomerRecord(NUM VARCHAR(20) PRIMARY KEY,RO VARCHAR(20),ED VARCHAR(20)"
				+ ",ET VARCHAR(50),CU VARCHAR(50),IEC VARCHAR(50),IAC VARCHAR(50),ABN VARCHAR(50),DAU VARCHAR(50)"
				+ ",DAT VARCHAR(50),DD VARCHAR(50),HDD VARCHAR(50),LI VARCHAR(50),RAO VARCHAR(50),SF VARCHAR(50)"
				+ ",AGENT VARCHAR(50),CUS VARCHAR(50),LA VARCHAR(50),CBI VARCHAR(50),TELCO VARCHAR(50),NCON VARCHAR(50)"
				+ ",CTEL VARCHAR(50),ALBL VARCHAR(50),AAC VARCHAR(50),ALAT VARCHAR(50),ANET VARCHAR(50),ASTA VARCHAR(50)"
				+ ",SO VARCHAR(50),NEWRO VARCHAR(20),NOTE VARCHAR(5000),SEFD VARCHAR(50),INTERC VARCHAR(50),STATUS VARCHAR(50)"
				+ ",EINT VARCHAR(50),PEC VARCHAR(50),PAC VARCHAR(50),NODE VARCHAR(50) "
				+ ")");
		// table CRCNT3
		execute("DROP TABLE IF EXISTS CRCNT3");
		execute("CREATE TABLE CRCNT3(id IDENTITY PRIMARY KEY,NUM VARCHAR(20),ALBL VARCHAR(50)" + ")");
		// table CRCNT8
		execute("DROP TABLE IF EXISTS CRCNT8");
		execute("CREATE TABLE CRCNT8(id IDENTITY PRIMARY KEY,NUM VARCHAR(20),LN VARCHAR(50)" + ")");
		// table CRCNT9
		execute("DROP TABLE IF EXISTS CRCNT9");
		execute("CREATE TABLE CRCNT9(id IDENTITY PRIMARY KEY,NUM VARCHAR(20),TEL VARCHAR(20),LNS VARCHAR(50),CITY VARCHAR(50),FSO VARCHAR(50)"
				+ ",LSIS VARCHAR(50),LSO VARCHAR(50),SFG VARCHAR(50),STN VARCHAR(50),UTS VARCHAR(50)"
				+ ",HML VARCHAR(5000)" + ")");
		// table CRCNT11
		execute("DROP TABLE IF EXISTS CRCNT11");
		execute("CREATE TABLE CRCNT11(id IDENTITY PRIMARY KEY,NUM VARCHAR(20),V VARCHAR(400)" + ")");
		// table CRCNT12
		execute("DROP TABLE IF EXISTS CRCNT12");
		execute("CREATE TABLE CRCNT12(id IDENTITY PRIMARY KEY,NUM VARCHAR(20),`TYPE` VARCHAR(10),`SORT` VARCHAR(10),LBL VARCHAR(20)"
				+ ",`DEF` VARCHAR(100)" + ")");
		// table CRCNT14
		execute("DROP TABLE IF EXISTS CRCNT14");
		execute("CREATE TABLE CRCNT14(id IDENTITY PRIMARY KEY,NUM VARCHAR(20),OTH VARCHAR(500) " + ")");

		// table CRSCP
		execute("DROP TABLE IF EXISTS CRSCP");
		execute("CREATE TABLE CRSCP(id IDENTITY PRIMARY KEY,NUM VARCHAR(20),SCP VARCHAR(10),"
				+ "SCPSTAT varchar(10),INAOS varchar(10) " + ")");

		// table DAP
		execute("DROP TABLE IF EXISTS DAP");
		execute("CREATE TABLE DAP(id IDENTITY PRIMARY KEY,NUM VARCHAR(20),ED VARCHAR(20),ET VARCHAR(50),STATUS VARCHAR(50)"
				+ " " + ")");
		
		// table UserInfo
		execute("DROP TABLE IF EXISTS USERINFO");
		execute("CREATE TABLE USERINFO(id varchar(20) PRIMARY KEY )");
		// test
		executeUpdate("INSERT INTO USERINFO  (id)" + "  VALUES('123456789')");
		executeUpdate("INSERT INTO USERINFO  (id)" + "  VALUES('12345678900')");

		// for (int i = 0; i < 30; i++)
		// executeUpdate("INSERT INTO QueMsg
		// (ip,tliHdr_version,tliHdr_priority,tliHdr_messageId,tliHdr_messageCode,tliHdr_destNodeName,tliHdr_srcNodeName,tliHdr_errorCode,dataAppl)
		// "
		// + "
		// VALUES('127.0.0.1','1','88','ABcd1234',15,'client','server',0,'1')");
	}

	public static void CreateClientTables() {
		// table QueClientMsg
		execute("DROP TABLE IF EXISTS QueClientMsg");
		execute("CREATE TABLE QueClientMsg(id IDENTITY PRIMARY KEY,tliHdr_version VARCHAR(100),tliHdr_priority VARCHAR(20)"
				+ ",ip VARCHAR(50)"
				+ ",tliHdr_messageId VARCHAR(12),tliHdr_messageCode VARCHAR(100),tliHdr_destNodeName VARCHAR(51)"
				+ ",tliHdr_srcNodeName VARCHAR(51),tliHdr_errorCode VARCHAR(20),dataAppl VARCHAR(5000)" + ")");
	}

	public static void addNumber(String number, String status) {
		execute("delete from  NumberInfo where number='" + number + "'");
		execute("INSERT INTO NumberInfo (number,status,addtime) VALUES('" + number + "','" + status
				+ "',CURRENT_TIMESTAMP())");
		// test
		// for (int i = 0; i < 30; i++)
		// executeUpdate("INSERT INTO QueClientMsg
		// (ip,tliHdr_version,tliHdr_priority,tliHdr_messageId) "
		// + " VALUES('127.0.0.1','1.2.3.4','88','ABcd1234')");
	}

	public static boolean CloseDB() {
		// 锟酵凤拷锟斤拷源
		try {
			stmt.close();
			// 锟截憋拷锟斤拷锟斤拷
			conn.close();
			System.out.println("CloseDB successfully!\r\n");
			// stop the TCP Server
			server.stop();
			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	public static boolean execute(String sql) {
		error_str = "";
		boolean bOk = false;
		try {
			stmt.execute(sql);
			bOk = true;
		} catch (Exception ex) {
			error_str = "execute " + sql + " get error:" + ex.toString();
			System.out.println("----" + error_str);
		}
		return bOk;
	}

	public static boolean executeUpdate(String sql) {
		error_str = "";
		boolean bOk = false;
		try {
			stmt.executeUpdate(sql);
			bOk = true;
		} catch (Exception ex) {
			error_str = "executeUpdate " + sql + " get error:" + ex.toString();
		}
		return bOk;
	}

	public static ResultSet executeQuery(String sql) {
		error_str = "";
		ResultSet resultSet = null;
		try {
			resultSet = stmt.executeQuery(sql);
			// //锟斤拷锟斤拷锟斤拷锟斤拷锟�
			// while (rs.next()) {
			// System.out.println(rs.getString("id") + "," +
			// rs.getString("name")+ "," + rs.getString("sex"));
			// }
		} catch (Exception ex) {
			error_str = "executeQuery " + sql + " get error:" + ex.toString();
		}
		return resultSet;
	}

	public static String ExecuteScalar(String sql) {
		error_str = "";
		ResultSet resultSet = null;
		try {
			resultSet = stmt.executeQuery(sql);
			if (resultSet.next()) {
//				int ii = resultSet.getInt(0);
//				return String.valueOf(ii);
				return resultSet.getString(1);
			} else {
				return "";
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			return "";
		}
	}

}
