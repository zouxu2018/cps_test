package com.cpsserver;

import com.cpsserver.handle.ReqDap;
import com.cpsserver.handle.UnsNsr;
import com.turkcelltech.jac.ASN1Integer;
import com.turkcelltech.jac.OctetString;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import java.io.PrintStream;
import java.net.InetSocketAddress;
import java.sql.ResultSet;
import java.util.Date;
import java.util.UUID;

public class NettyClientHandler extends ChannelInboundHandlerAdapter {
	private YlDcmMsg obj;
	private String srcNodeName = "";
	private String destNodeName = "";
	private YlDcmMsg lastSendMessage;

	public NettyClientHandler(String src_node_name,String dest_node_name) {
		this.destNodeName = dest_node_name;
		this.srcNodeName = src_node_name;
	}

	public void channelRegistered(ChannelHandlerContext ctx) throws Exception {
		System.out.println("==============channel--register==============");
	}

	public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {
		System.out.println("==============channel--unregistered==============");
	}

	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		System.out.println("==============channelInactive==============");
	}

	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		try {
			// send all queue messages
			InetSocketAddress insocket = (InetSocketAddress) ctx.channel().remoteAddress();
			String clientIP = insocket.getAddress().getHostAddress();
			ResultSet rs = H2Data.executeQuery("SELECT * FROM QueClientMsg where ip='" + clientIP + "'");
			// 锟斤拷锟斤拷锟斤拷锟斤拷锟�
			while (rs.next()) {
				obj = new YlDcmMsg();
				obj.tliHdr.version.setValue(Long.parseLong(rs.getString("tliHdr_version")));
				obj.tliHdr.priority.setValue(Integer.parseInt(rs.getString("tliHdr_priority")));
				obj.tliHdr.messageId.setValue(rs.getString("tliHdr_messageId").getBytes());
				obj.tliHdr.messageCode.setValue(Long.parseLong(rs.getString("tliHdr_messageCode")));
				obj.tliHdr.destNodeName.setValue(rs.getString("tliHdr_destNodeName").getBytes());
				obj.tliHdr.srcNodeName.setValue(rs.getString("tliHdr_srcNodeName").getBytes());
				obj.tliHdr.errorCode.setValue(Long.parseLong(rs.getString("tliHdr_errorCode")));

				obj.dataAppl.setValue(rs.getString("dataAppl").getBytes());
				try {
					ctx.write(obj);
					ctx.flush();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			H2Data.executeUpdate("DELETE FROM QueClientMsg where ip='" + clientIP + "'");
			System.out.println("----write queuing messages");
		} catch (Exception eee) {
		}
		System.out.println("==============channelActive==============");
	}

	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		this.lastSendMessage = null;

		YlDcmMsg result = (YlDcmMsg) msg;

		if (result.tliHdr.messageCode.getValue() == 1L) {
			String desc_node_name = new String(result.tliHdr.srcNodeName.getValue());
			String src_node_name = new String(result.tliHdr.destNodeName.getValue());
			System.out
					.println("----read asn.1 GD message srcNodeName:" + new String(result.tliHdr.srcNodeName.getValue()) 
							+ " destNodeName:" + new String(result.tliHdr.destNodeName.getValue()));

			if (new String(result.tliHdr.destNodeName.getValue()).equals("")) {
				result.tliHdr.srcNodeName.setValue(this.srcNodeName.getBytes());
				result.tliHdr.destNodeName.setValue(this.destNodeName.getBytes());
				try {
					// this.lastSendMessage = ((YlDcmMsg) obj.clone());
					ctx.write(result);
					ctx.flush();
					System.out.println("----write asn.1 message:GD");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} else if (result.tliHdr.messageCode.getValue() == 5L) {
			System.out
			.println("----read asn.1 GNT message srcNodeName:" + new String(result.tliHdr.srcNodeName.getValue()) 
					+ " destNodeName:" + new String(result.tliHdr.destNodeName.getValue()));
		} else if (result.tliHdr.messageCode.getValue() == 19L) {
			String dataAppl = new String(result.dataAppl.getValue());
			String[] dataList = dataAppl.split(":");
			String sNumList = "";
			int count = 1;
			// get Num list
			for (int m = 0; m < dataList.length; m++) {
				String s0 = dataList[m];
				String[] dataInfo0 = s0.split("=");
				if (s0.indexOf("CNT=") == 0) {
					count = Integer.parseInt(dataInfo0[1]);
				} else if (s0.indexOf("NUM=") == 0) {
					sNumList = sNumList + dataInfo0[1] + ",";
				}
			}
			if (!sNumList.equals(""))
				sNumList = sNumList.substring(0, sNumList.length() - 1);

			if ((dataList[0].equals("RSP-TEST")) && (dataList.length >= 7)) {
				System.out.println("----read asn.1 message from " + new String(result.tliHdr.destNodeName.getValue())
						+ ":RSP-TEST");

				if (NettyClient.TestCaseNumber == 200) {
					NettyClient.TestCaseNumber = 0;
				} else if (NettyClient.TestCaseNumber == 300) {
					NettyClient.TestCaseNumber = 301;

					dataAppl = String.format("REQ-TEST:,%s,%s::%s:DENIED,01::\"%s\"",
							new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()),
									dataList[3], "send S2S-RSP" });
					result.dataAppl.setValue(dataAppl.getBytes());
					ctx.writeAndFlush(result);
				} else if (NettyClient.TestCaseNumber == 301) {
					NettyClient.TestCaseNumber = 302;
					dataAppl = String.format("REQ-TEST:invalid UAL header", new Object[0]);
					result.dataAppl.setValue(dataAppl.getBytes());
					ctx.writeAndFlush(result);
				} else if (NettyClient.TestCaseNumber != 303) {
					if (NettyClient.TestCaseNumber == 400) {
						NettyClient.TestCaseNumber = 401;

						dataAppl = String.format("TRA:,%s,%s::%s:DENIED,01::\"%s\"",
								new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()),
										dataList[3], "send TRA" });
						result.dataAppl.setValue(dataAppl.getBytes());
						// this.lastSendMessage = ((YlDcmMsg) obj.clone());
						ctx.writeAndFlush(result);
					} else if (NettyClient.TestCaseNumber == 500) {
						NettyClient.TestCaseNumber = 0;
					} else if (NettyClient.TestCaseNumber == 700) {
						NettyClient.TestCaseNumber = 0;
					} else {
						// dataAppl =
						// String.format("REQ-TEST:,%s,%s::%s:DENIED,01::\"%s\"",
						// new Object[] { YlUtil.dateToString(new Date()),
						// YlUtil.timeToString(new Date()),
						// dataList[3], "send S2S-RSP" });
						// result.dataAppl.setValue(dataAppl.getBytes());
						// // this.lastSendMessage = ((YlDcmMsg) obj.clone());
						// ctx.writeAndFlush(result);
					}
				}
				return;
			} else if (dataList[0].equals("UNS-NSR")) {
				UnsNsr unsnsr = new UnsNsr();
				unsnsr.Handle(ctx, obj, dataAppl, dataList, count);
			} else if ((dataList[0].equals("UNR")) && (NettyClient.TestCaseNumber == 302)) {
				NettyClient.TestCaseNumber = 303;

				dataAppl = String.format("REQ-TEST:,%s,%s::%s:DENIED,01::\"%s\"",
						new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), dataList[3],
								"send S2S-RSP" });
				result.dataAppl.setValue(dataAppl.getBytes());
				// this.lastSendMessage = ((YlDcmMsg) obj.clone());
				ctx.writeAndFlush(result);
			}
			System.out.println(
					"----read asn.1 message from " + new String(result.tliHdr.srcNodeName.getValue()) + ":RSP-TEST");
		}
	}

	public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
		System.out.println("==============channelReadComplete==============");
		ctx.flush();
	}

	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		cause.printStackTrace();
		InetSocketAddress insocket = (InetSocketAddress) ctx.channel().remoteAddress();
		String clientIP = insocket.getAddress().getHostAddress();
		System.out.println("test:saved Queuing Messages When " + clientIP + " Link is Down");
		// save to memory table
		if (this.lastSendMessage != null) {
			if (String.valueOf(this.lastSendMessage.dataAppl.getValue()).indexOf("Queuing=Yes") >= 0) {
				H2Data.executeUpdate(
						"INSERT INTO QueClientMsg		 (ip,tliHdr_version,tliHdr_priority,tliHdr_messageId,tliHdr_messageCode"
								+ ",tliHdr_destNodeName,tliHdr_srcNodeName,tliHdr_errorCode,dataAppl)		 "
								+ " VALUES('" + clientIP + "','"
								+ String.valueOf(this.lastSendMessage.tliHdr.version.getValue()) + "','"
								+ String.valueOf(this.lastSendMessage.tliHdr.priority.getValue()) + "','"
								+ String.valueOf(this.lastSendMessage.tliHdr.messageId.getValue()) + "','"
								+ String.valueOf(this.lastSendMessage.tliHdr.messageCode.getValue()) + "','"
								+ String.valueOf(this.lastSendMessage.tliHdr.destNodeName.getValue()) + "','"
								+ String.valueOf(this.lastSendMessage.tliHdr.srcNodeName.getValue()) + "','"
								+ String.valueOf(this.lastSendMessage.tliHdr.errorCode.getValue()) + "','"
								+ String.valueOf(this.lastSendMessage.dataAppl.getValue()) + "')");
			}
		}
		ctx.close();
	}
}