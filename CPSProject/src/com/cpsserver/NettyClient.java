package com.cpsserver;

import com.chaosinmotion.asn1.BerOutputStream;
import com.turkcelltech.jac.ASN1Integer;
import com.turkcelltech.jac.OctetString;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.timeout.IdleStateHandler;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.text.SimpleDateFormat;
import java.util.Date;

public class NettyClient implements Runnable {
	private int local_port = 8021;
	private int remote_port = 8021;
	private String local_host = "208.76.98.20";
	private String remote_host = "192.4.124.157";
//	private String local_host = "127.0.0.1";
//	private String remote_host = "127.0.0.1";
	private String srcNodeName="YNWXXXYNW01TEST01";
	private String destNodeName = "BCL2LN01E00ysA703";
	private String sHostName ="SMS01";
	
	private String req_nsr_message_id = "XQG01000";
	private String req_nsr_message_ro = "XQG01";
	private String test_npa ="833";
	private String test_number="8332561234";
	private String test_number1="83325*1234";
	
	private ChannelFuture f;
	public static int TestCaseNumber = 0;

	private static final int READ_IDEL_TIME_OUT = 190;
	private static final int WRITE_IDEL_TIME_OUT = 240;
	private static final int ALL_IDEL_TIME_OUT = 290;
	
	
	public NettyClient() {
	}

	public static String timeStamp() {
		long time = System.currentTimeMillis();
		String t = String.valueOf(time / 100);
		return t.substring(3, 11);
	}
	
	public String getCorrelationID() {
		return timeStamp();
	}

	public void run() {
		H2Data.InitClientDB();
		EventLoopGroup group = new NioEventLoopGroup();

		int MAX_FRAME_LENGTH = 2147483647;
		int LENGTH_FIELD_LENGTH = 4;
		int LENGTH_FIELD_OFFSET = 4;
		int LENGTH_ADJUSTMENT = 0;
		int INITIAL_BYTES_TO_STRIP = 8;
		try {
			Bootstrap b = new Bootstrap();
			b.group(group).channel(NioSocketChannel.class).option(ChannelOption.TCP_NODELAY, true)
					.handler(new ChannelInitializer<SocketChannel>() {
						@Override
						public void initChannel(SocketChannel ch) throws Exception {
							ch.pipeline()
									.addLast(new LengthFieldBasedFrameDecoder(MAX_FRAME_LENGTH, LENGTH_FIELD_OFFSET,
											LENGTH_FIELD_LENGTH, LENGTH_ADJUSTMENT, INITIAL_BYTES_TO_STRIP, false));
							ch.pipeline().addLast(new Asn1Decode());
							ch.pipeline().addLast(new Asn1Encode());
							ch.pipeline().addLast(new ChannelHandler[] { new IdleStateHandler(READ_IDEL_TIME_OUT,
									WRITE_IDEL_TIME_OUT, ALL_IDEL_TIME_OUT, TimeUnit.SECONDS) });
							ch.pipeline().addLast(new NettyClientHandler(srcNodeName,destNodeName));
							ch.pipeline().addLast(new ChannelHandler[] { new TimeoutHandler() });
						}
					});
//			b.connect(remoteAddress, localAddress)
//			SocketAddress localAddress = new InetSocketAddress("208.76.98.20",8021);
//			int local_port = getNotUsedPort();
			SocketAddress localAddress = new InetSocketAddress(local_host,local_port);
			SocketAddress remoteAddress = new InetSocketAddress(remote_host,remote_port);
//			this.f = b.connect(this.host, this.port).sync();
			try {
				this.f = b.connect(remoteAddress,localAddress).sync();
			} catch (Exception e1) {
				System.out.println("----connect failed  to:"+remote_host+":"+remote_port);
				System.out.println("----run exception:"+e1.toString());
			} finally {
				
			}
			
			System.out.println("----connect successfully from:"+local_host+":"+local_port
					+" to:"+remote_host+":"+remote_port);
			this.f.channel().closeFuture().sync();
		} catch (Exception localException) {
			System.out.println("----connect failed  to:"+remote_host+":"+remote_port);
			System.out.println("----run exception:"+localException.toString());
		} finally {
			group.shutdownGracefully();
		}
	}
	
	/***
	 *  true:already in using  false:not using 
	 * @param host
	 * @param port
	 * @throws UnknownHostException 
	 */
	public static int getNotUsedPort()  {
		boolean flag = false;
		int lPort =8021;
		while (flag && lPort<9000)
		try {
			DatagramSocket ds=new DatagramSocket(lPort);
			flag=true;
		} catch (Exception e) {
			System.out.println("----get not used port exception:"+e.toString());
			lPort++;
		}
		return lPort;
	}
	

	public static void main(String[] args) throws Exception {
		int port = 8989;
		String host = "127.0.0.1";
		//somos host and port
		port=8021;
		host="192.4.124.157";
		if ((args != null) && (args.length > 0)) {
			try {
				port = Integer.valueOf(args[0]).intValue();
				host = args[1];
			} catch (NumberFormatException localNumberFormatException) {
			}
		}
		NettyClient client = new NettyClient();
		new Thread(client, ">>>this thread 0").start();

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String message = br.readLine();

		while (!message.equals("quit")) {
			if (message.equals("testGD"))
				client.TestGD();
			else if (message.equals("testID")) {
				System.out.println("----test ID:"+client.getCorrelationID());
			} 
			else if (message.equals("test1")) {
				client.TestCase1();
			} 
			else if (message.equals("test2"))
				client.TestCase2();
			else if (message.equals("test3"))
				client.TestCase3();
			else if (message.equals("test4"))
				client.TestCase4();
			else if (message.equals("test5"))
				client.TestCase5();
			else if (message.equals("test6"))
				client.TestCase6();
			else if (message.equals("test7"))
				client.TestCase7();
			else if (message.equals("test15"))
				client.TestCase15();
			else if (message.equals("test16"))
				client.TestCase16();
			else if (message.equals("test17"))
				client.TestCase17();
			else if (message.equals("test18"))
				client.TestCase18();
			else if (message.equals("test19"))
				client.TestCase19();

			else if (message.equals("test20"))
				client.TestCase20();
			else if (message.equals("test21"))
				client.TestCase21();
			else if (message.equals("test22"))
				client.TestCase22();
			else if (message.equals("test23"))
				client.TestCase23();
			else if (message.equals("test24"))
				client.TestCase24();
			else if (message.equals("test25"))
				client.TestCase25();
			else if (message.equals("test26"))
				client.TestCase26();
			else if (message.equals("test27"))
				client.TestCase27();
			else if (message.equals("test28"))
				client.TestCase28();
			else if (message.equals("test29"))
				client.TestCase29();
			else if (message.equals("test30"))
				client.TestCase30();
			else if (message.equals("test31"))
				client.TestCase31();
			else if (message.equals("test32"))
				client.TestCase32();
			else if (message.equals("test33"))
				client.TestCase33();
			else if (message.equals("test34"))
				client.TestCase34();
			else if (message.equals("test35"))
				client.TestCase35();
			else if (message.equals("test36"))
				client.TestCase36();
			else if (message.equals("test37"))
				client.TestCase37();
			else if (message.equals("test38"))
				client.TestCase38();
			else if (message.equals("test39"))
				client.TestCase39();
			else if (message.equals("test40"))
				client.TestCase40();
			else if (message.equals("test41"))
				client.TestCase41();
			else if (message.equals("test42"))
				client.TestCase42();
			else if (message.equals("test43"))
				client.TestCase43();
			else if (message.equals("test44"))
				client.TestCase44();
			else if (message.equals("test45"))
				client.TestCase45();
			else if (message.equals("test46"))
				client.TestCase46();
			else if (message.equals("test47"))
				client.TestCase47();
			else if (message.equals("test48"))
				client.TestCase48();
			else if (message.equals("test49"))
				client.TestCase49();
			else if (message.equals("test50"))
				client.TestCase50();
			else if (message.equals("test51"))
				client.TestCase51();
			else if (message.equals("test52"))
				client.TestCase52();
			else if (message.equals("test53"))
				client.TestCase53();
			else if (message.equals("test54"))
				client.TestCase54();
			else if (message.equals("test55"))
				client.TestCase55();
			else if (message.equals("test56"))
				client.TestCase56();
			else if (message.equals("test57"))
				client.TestCase57();
			else if (message.equals("test58"))
				client.TestCase58();
			else if (message.equals("test59"))
				client.TestCase59();
			else if (message.equals("test60"))
				client.TestCase60();
			else if (message.equals("test61"))
				client.TestCase61();
			else if (message.equals("test62"))
				client.TestCase62();
			else if (message.equals("test63"))
				client.TestCase63();
			else if (message.equals("test64"))
				client.TestCase64();
			else if (message.equals("test65"))
				client.TestCase65();
			else if (message.equals("test66"))
				client.TestCase66();
			else if (message.equals("test67"))
				client.TestCase67();
			else if (message.equals("test68"))
				client.TestCase68();
			else if (message.equals("test69"))
				client.TestCase69();
			else if (message.equals("test70"))
				client.TestCase70();
			else if (message.equals("test71"))
				client.TestCase71();
			else if (message.equals("test72"))
				client.TestCase72();
			else if (message.equals("test73"))
				client.TestCase73();
			else if (message.equals("test76"))
				client.TestCase76();
			else if (message.equals("test77"))
				client.TestCase77();
			else if (message.equals("test78"))
				client.TestCase78();
			else if (message.equals("test79"))
				client.TestCase79();
			else if (message.equals("test80"))
				client.TestCase80();
			else if (message.equals("test81"))
				client.TestCase81();
			else if (message.equals("test82"))
				client.TestCase82();
			else if (message.equals("test83"))
				client.TestCase83();
			else if (message.equals("test84"))
				client.TestCase84();
			else if (message.equals("test85"))
				client.TestCase85();
			else if (message.equals("test86"))
				client.TestCase86();
			else if (message.equals("test87"))
				client.TestCase87();
			else if (message.equals("test88"))
				client.TestCase88();
			else if (message.equals("test89"))
				client.TestCase89();
			else if (message.equals("test90"))
				client.TestCase90();
			else if (message.equals("test91"))
				client.TestCase91();
//			else if (message.equals("test92"))
//				client.TestCase92();
//			else if (message.equals("test93"))
//				client.TestCase93();
			else if (message.equals("test94"))
				client.TestCase94();
			else if (message.equals("test95"))
				client.TestCase95();
			else if (message.equals("test96"))
				client.TestCase96();
			else if (message.equals("test97"))
				client.TestCase97();
			else if (message.equals("test98"))
				client.TestCase98();
			else if (message.equals("test99"))
				client.TestCase99();
			else if (message.equals("test100"))
				client.TestCase100();
			else if (message.equals("test101"))
				client.TestCase101();
			else if (message.equals("test102"))
				client.TestCase102();
			else if (message.equals("test103"))
				client.TestCase103();
			else if (message.equals("test104"))
				client.TestCase104();
			else if (message.equals("test105"))
				client.TestCase105();
			else if (message.equals("test106"))
				client.TestCase106();
			else if (message.equals("test107"))
				client.TestCase107();
			else if (message.equals("test108"))
				client.TestCase108();
			else if (message.equals("test109"))
				client.TestCase109();
			else if (message.equals("test110"))
				client.TestCase110();
			else if (message.equals("test111"))
				client.TestCase111();
			else if (message.equals("test112"))
				client.TestCase112();
			else if (message.equals("test113"))
				client.TestCase113();
			else if (message.equals("test114"))
				client.TestCase114();
			else if (message.equals("test115"))
				client.TestCase115();
			else if (message.equals("test116"))
				client.TestCase116();
			else if (message.equals("test117"))
				client.TestCase117();
			else if (message.equals("test118"))
				client.TestCase118();
			else if (message.equals("test119"))
				client.TestCase119();
			else if (message.equals("test120"))
				client.TestCase120();
			else if (message.equals("test121"))
				client.TestCase121();
			else if (message.equals("test122"))
				client.TestCase122();
			else if (message.equals("test123"))
				client.TestCase123();
			else if (message.equals("test124"))
				client.TestCase124();
			else if (message.equals("test125"))
				client.TestCase125();
			else if (message.equals("test126"))
				client.TestCase126();
			else if (message.equals("test127"))
				client.TestCase127();
			else if (message.equals("test128"))
				client.TestCase128();
			else if (message.equals("test129"))
				client.TestCase129();
			else if (message.equals("test130"))
				client.TestCase130();
			else if (message.equals("test131"))
				client.TestCase131();
			else if (message.equals("test132"))
				client.TestCase132();
			else if (message.equals("test133"))
				client.TestCase133();
			else if (message.equals("test134"))
				client.TestCase134();
			else if (message.equals("test135"))
				client.TestCase135();
			else if (message.equals("test136"))
				client.TestCase136();
			else if (message.equals("test137"))
				client.TestCase137();
			else if (message.equals("test138"))
				client.TestCase138();
			else if (message.equals("test139"))
				client.TestCase139();
			else if (message.equals("test140"))
				client.TestCase140();
			else if (message.equals("test141"))
				client.TestCase141();
			else if (message.equals("test142"))
				client.TestCase142();
			else if (message.equals("test143"))
				client.TestCase143();
			else if (message.equals("test144"))
				client.TestCase144();
			else if (message.equals("test145"))
				client.TestCase145();
			else if (message.equals("test146"))
				client.TestCase146();
			/*
			
			else if (message.equals("test147"))
				client.TestCase147();
			else if (message.equals("test148"))
				client.TestCase148();
			else if (message.equals("test149"))
				client.TestCase149();
			else if (message.equals("test150"))
				client.TestCase150();
			else if (message.equals("test151"))
				client.TestCase151();
			else if (message.equals("test152"))
				client.TestCase152();
			else if (message.equals("test153"))
				client.TestCase153();
			else if (message.equals("test154"))
				client.TestCase154();
			else if (message.equals("test155"))
				client.TestCase155();
			else if (message.equals("test156"))
				client.TestCase156();
			else if (message.equals("test157"))
				client.TestCase157();
			else if (message.equals("test158"))
				client.TestCase158();
			else if (message.equals("test159"))
				client.TestCase159();
			else if (message.equals("test160"))
				client.TestCase160();
			else if (message.equals("test161"))
				client.TestCase161();
			else if (message.equals("test162"))
				client.TestCase162();
			else if (message.equals("test163"))
				client.TestCase163();
			else if (message.equals("test164"))
				client.TestCase164();
			else if (message.equals("test165"))
				client.TestCase165();
			else if (message.equals("test166"))
				client.TestCase166();
			else if (message.equals("test167"))
				client.TestCase167();
			else if (message.equals("test168"))
				client.TestCase168();
			else if (message.equals("test169"))
				client.TestCase169();
			else if (message.equals("test170"))
				client.TestCase170();
			else if (message.equals("test171"))
				client.TestCase171();
			else if (message.equals("test172"))
				client.TestCase172();
			else if (message.equals("test173"))
				client.TestCase173();
			else if (message.equals("test174"))
				client.TestCase174();
			else if (message.equals("test175"))
				client.TestCase175();
			else if (message.equals("test176"))
				client.TestCase176();
			else if (message.equals("test177"))
				client.TestCase177();
			else if (message.equals("test178"))
				client.TestCase178();
			else if (message.equals("test179"))
				client.TestCase179();
			else if (message.equals("test180"))
				client.TestCase180();
			else if (message.equals("test181"))
				client.TestCase181();
			else if (message.equals("test182"))
				client.TestCase182();
			else if (message.equals("test183"))
				client.TestCase183();
			else if (message.equals("test184"))
				client.TestCase184();
			else if (message.equals("test185"))
				client.TestCase185();
			else if (message.equals("test186"))
				client.TestCase186();
			else if (message.equals("test187"))
				client.TestCase187();
			else if (message.equals("test188"))
				client.TestCase188();
			else if (message.equals("test189"))
				client.TestCase189();
			else if (message.equals("test190"))
				client.TestCase190();
			else if (message.equals("test191"))
				client.TestCase191();
			else if (message.equals("test192"))
				client.TestCase192();
			else if (message.equals("test193"))
				client.TestCase193();
			else if (message.equals("test194"))
				client.TestCase194();
			else if (message.equals("test195"))
				client.TestCase195();
			else if (message.equals("test196"))
				client.TestCase196();
			else if (message.equals("test197"))
				client.TestCase197();
			else if (message.equals("test198"))
				client.TestCase198();
			else if (message.equals("test199"))
				client.TestCase199();
			else if (message.equals("test200"))
				client.TestCase200();
			else if (message.equals("test201"))
				client.TestCase201();
			else if (message.equals("test202"))
				client.TestCase202();
			else if (message.equals("test203"))
				client.TestCase203();
			else if (message.equals("test204"))
				client.TestCase204();
			else if (message.equals("test205"))
				client.TestCase205();
			else if (message.equals("test206"))
				client.TestCase206();
			else if (message.equals("test207"))
				client.TestCase207();
			else if (message.equals("test208"))
				client.TestCase208();
			else if (message.equals("test209"))
				client.TestCase209();
			else if (message.equals("test210"))
				client.TestCase210();
			else if (message.equals("test211"))
				client.TestCase211();
			else if (message.equals("test212"))
				client.TestCase212();
			else if (message.equals("test213"))
				client.TestCase213();
			else if (message.equals("test214"))
				client.TestCase214();
			else if (message.equals("test215"))
				client.TestCase215();
			else if (message.equals("test216"))
				client.TestCase216();
			else if (message.equals("test217"))
				client.TestCase217();
			else if (message.equals("test218"))
				client.TestCase218();
			else if (message.equals("test219"))
				client.TestCase219();
			else if (message.equals("test220"))
				client.TestCase220();
			else if (message.equals("test221"))
				client.TestCase221();
			else if (message.equals("test222"))
				client.TestCase222();
			else if (message.equals("test223"))
				client.TestCase223();
			else if (message.equals("test224"))
				client.TestCase224();
			else if (message.equals("test225"))
				client.TestCase225();
			else if (message.equals("test226"))
				client.TestCase226();
			else if (message.equals("test227"))
				client.TestCase227();
			else if (message.equals("test228"))
				client.TestCase228();
			else if (message.equals("test229"))
				client.TestCase229();
			else if (message.equals("test230"))
				client.TestCase230();
			else if (message.equals("test231"))
				client.TestCase231();
			else if (message.equals("test232"))
				client.TestCase232();
			else if (message.equals("test233"))
				client.TestCase233();
			else if (message.equals("test234"))
				client.TestCase234();
			else if (message.equals("test235"))
				client.TestCase235();
			else if (message.equals("test236"))
				client.TestCase236();
			else if (message.equals("test237"))
				client.TestCase237();
			else if (message.equals("test238"))
				client.TestCase238();
			else if (message.equals("test239"))
				client.TestCase239();
			else if (message.equals("test240"))
				client.TestCase240();
			else if (message.equals("test241"))
				client.TestCase241();
			else if (message.equals("test242"))
				client.TestCase242();
			else if (message.equals("test243"))
				client.TestCase243();
			else if (message.equals("test244"))
				client.TestCase244();
			else if (message.equals("test245"))
				client.TestCase245();
			else if (message.equals("test246"))
				client.TestCase246();
			else if (message.equals("test247"))
				client.TestCase247();
			else if (message.equals("test248"))
				client.TestCase248();
			else if (message.equals("test249"))
				client.TestCase249();
			else if (message.equals("test250"))
				client.TestCase250();
			else if (message.equals("test251"))
				client.TestCase251();
			else if (message.equals("test252"))
				client.TestCase252();
			else if (message.equals("test253"))
				client.TestCase253();
			else if (message.equals("test254"))
				client.TestCase254();
			else if (message.equals("test255"))
				client.TestCase255();
			else if (message.equals("test256"))
				client.TestCase256();
			else if (message.equals("test257"))
				client.TestCase257();
			else if (message.equals("test258"))
				client.TestCase258();
			else if (message.equals("test259"))
				client.TestCase259();
			else if (message.equals("test260"))
				client.TestCase260();
			else if (message.equals("test261"))
				client.TestCase261();
			else if (message.equals("test262"))
				client.TestCase262();
			else if (message.equals("test263"))
				client.TestCase263();
			else if (message.equals("test264"))
				client.TestCase264();
			else if (message.equals("test265"))
				client.TestCase265();
			else if (message.equals("test266"))
				client.TestCase266();
			else if (message.equals("test267"))
				client.TestCase267();
			else if (message.equals("test268"))
				client.TestCase268();
			else if (message.equals("test269"))
				client.TestCase269();
			else if (message.equals("test270"))
				client.TestCase270();
			else if (message.equals("test271"))
				client.TestCase271();
			else if (message.equals("test272"))
				client.TestCase272();
			else if (message.equals("test273"))
				client.TestCase273();
			else if (message.equals("test274"))
				client.TestCase274();
			else if (message.equals("test275"))
				client.TestCase275();
			else if (message.equals("test276"))
				client.TestCase276();
			else if (message.equals("test277"))
				client.TestCase277();
			else if (message.equals("test278"))
				client.TestCase278();
			else if (message.equals("test278"))
				client.TestCase279();
			else if (message.equals("test280"))
				client.TestCase280();
			else if (message.equals("test281"))
				client.TestCase281();
			else if (message.equals("test282"))
				client.TestCase282();
			else if (message.equals("test283"))
				client.TestCase283();
			else if (message.equals("test284"))
				client.TestCase284();
			else if (message.equals("test285"))
				client.TestCase285();
			else if (message.equals("test286"))
				client.TestCase286();
			else if (message.equals("test287"))
				client.TestCase287();
			else if (message.equals("test288"))
				client.TestCase288();
			else if (message.equals("test289"))
				client.TestCase289();
			else if (message.equals("test290"))
				client.TestCase290();
			else if (message.equals("test291"))
				client.TestCase291();
			else if (message.equals("test292"))
				client.TestCase292();
			else if (message.equals("test293"))
				client.TestCase293();
			else if (message.equals("test294"))
				client.TestCase294();
			else if (message.equals("test295"))
				client.TestCase295();
			else if (message.equals("test296"))
				client.TestCase296();
			else if (message.equals("test297"))
				client.TestCase297();
			else if (message.equals("test298"))
				client.TestCase298();
			else if (message.equals("test299"))
				client.TestCase299();
			else if (message.equals("test300"))
				client.TestCase300();
			else if (message.equals("test301"))
				client.TestCase301();
			else if (message.equals("test302"))
				client.TestCase302();
			else if (message.equals("test303"))
				client.TestCase303();
			else if (message.equals("test304"))
				client.TestCase304();
			else if (message.equals("test305"))
				client.TestCase305();
			else if (message.equals("test306"))
				client.TestCase306();
			else if (message.equals("test307"))
				client.TestCase307();
			else if (message.equals("test308"))
				client.TestCase308();
			else if (message.equals("test309"))
				client.TestCase309();
			else if (message.equals("test310"))
				client.TestCase310();
			else if (message.equals("test311"))
				client.TestCase311();
			else if (message.equals("test312"))
				client.TestCase312();
			else if (message.equals("test313"))
				client.TestCase313();
			else if (message.equals("test314"))
				client.TestCase314();
			else if (message.equals("test315"))
				client.TestCase315();
			else if (message.equals("test316"))
				client.TestCase316();
			else if (message.equals("test317"))
				client.TestCase317();
			else if (message.equals("test318"))
				client.TestCase318();
			else if (message.equals("test319"))
				client.TestCase319();
			else if (message.equals("test320"))
				client.TestCase320();
			else if (message.equals("test321"))
				client.TestCase321();
			else if (message.equals("test322"))
				client.TestCase322();
			else if (message.equals("test323"))
				client.TestCase323();
			else if (message.equals("test324"))
				client.TestCase324();
			else if (message.equals("test325"))
				client.TestCase325();
			else if (message.equals("test326"))
				client.TestCase326();
			else if (message.equals("test327"))
				client.TestCase327();
			else if (message.equals("test328"))
				client.TestCase328();
			else if (message.equals("test329"))
				client.TestCase329();
			else if (message.equals("test330"))
				client.TestCase330();
			else if (message.equals("test331"))
				client.TestCase331();
			else if (message.equals("test332"))
				client.TestCase332();
			else if (message.equals("test333"))
				client.TestCase333();
			else if (message.equals("test334"))
				client.TestCase334();
			else if (message.equals("test335"))
				client.TestCase335();
			else if (message.equals("test336"))
				client.TestCase336();
			else if (message.equals("test337"))
				client.TestCase337();
			else if (message.equals("test338"))
				client.TestCase338();
			else if (message.equals("test339"))
				client.TestCase339();
			else if (message.equals("test340"))
				client.TestCase340();
			else if (message.equals("test341"))
				client.TestCase341();
			else if (message.equals("test342"))
				client.TestCase342();
			else if (message.equals("test343"))
				client.TestCase343();
			else if (message.equals("test344"))
				client.TestCase344();
			else if (message.equals("test345"))
				client.TestCase345();
			else if (message.equals("test346"))
				client.TestCase346();
			else if (message.equals("test347"))
				client.TestCase347();
			else if (message.equals("test348"))
				client.TestCase348();
			else if (message.equals("test349"))
				client.TestCase349();
			else if (message.equals("test350"))
				client.TestCase350();
			else if (message.equals("test351"))
				client.TestCase351();
			else if (message.equals("test352"))
				client.TestCase352();
			else if (message.equals("test353"))
				client.TestCase353();
			else if (message.equals("test354"))
				client.TestCase354();
			else if (message.equals("test355"))
				client.TestCase355();
			else if (message.equals("test356"))
				client.TestCase356();
			else if (message.equals("test357"))
				client.TestCase357();
			else if (message.equals("test358"))
				client.TestCase358();
			else if (message.equals("test359"))
				client.TestCase359();
			else if (message.equals("test360"))
				client.TestCase360();
			else if (message.equals("test361"))
				client.TestCase361();
			else if (message.equals("test362"))
				client.TestCase362();
			else if (message.equals("test363"))
				client.TestCase363();
			else if (message.equals("test364"))
				client.TestCase364();
			else if (message.equals("test365"))
				client.TestCase365();
			else if (message.equals("test366"))
				client.TestCase366();
			else if (message.equals("test367"))
				client.TestCase367();
			else if (message.equals("test368"))
				client.TestCase368();
			else if (message.equals("test369"))
				client.TestCase369();
			else if (message.equals("test370"))
				client.TestCase370();
			else if (message.equals("test371"))
				client.TestCase371();
			else if (message.equals("test372"))
				client.TestCase372();
			else if (message.equals("test373"))
				client.TestCase373();
			else if (message.equals("test374"))
				client.TestCase374();
			else if (message.equals("test375"))
				client.TestCase375();
			else if (message.equals("test376"))
				client.TestCase376();
			else if (message.equals("test377"))
				client.TestCase377();
			else if (message.equals("test378"))
				client.TestCase378();
			else if (message.equals("test379"))
				client.TestCase379();
			else if (message.equals("test380"))
				client.TestCase380();
//			else if (message.equals("test381"))
//				client.TestCase381();
//			else if (message.equals("test382"))
//				client.TestCase382();
//			else if (message.equals("test383"))
//				client.TestCase383();
//			else if (message.equals("test384"))
//				client.TestCase384();
//			else if (message.equals("test385"))
//				client.TestCase385();
//			else if (message.equals("test386"))
//				client.TestCase386();
//			else if (message.equals("test387"))
//				client.TestCase387();
//			else if (message.equals("test388"))
//				client.TestCase388();
//			else if (message.equals("test389"))
//				client.TestCase389();
//			else if (message.equals("test390"))
//				client.TestCase390();
//			else if (message.equals("test391"))
//				client.TestCase391();
//			else if (message.equals("test392"))
//				client.TestCase392();
//			else if (message.equals("test393"))
//				client.TestCase393();
//			else if (message.equals("test394"))
//				client.TestCase394();
//			else if (message.equals("test395"))
//				client.TestCase395();
//			else if (message.equals("test396"))
//				client.TestCase396();
			
			
			
//			else if (message.equals("test397"))
//				client.TestCase397();
//			else if (message.equals("test398"))
//				client.TestCase398();
//			else if (message.equals("test399"))
//				client.TestCase399();
//			else if (message.equals("test400"))
//				client.TestCase400();
 * */
 
			message = br.readLine();
			System.out.println("----type testXXX to start Test Case XXX");
			System.out.println("----type testGD to start Test GD");
			System.out.println("----type quit to exit program");
		}

		System.exit(0);
	}
	public void TestGD() {
		System.out.println("----Start Test GD");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(1L);
//		obj.tliHdr.destNodeName.setValue(destNodeName.getBytes());
		obj.tliHdr.destNodeName.setValue("".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);

		obj.dataAppl.setValue("".getBytes());
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:GD");

	}

	public YlDcmMsg NewUplMessage(String upl_data) {
		return NewUplMessage(upl_data,"TSA");
	}	
	public YlDcmMsg NewUplMessage(String upl_data,String drc) {
		YlDcmMsg obj = new YlDcmMsg();
		String message_id = Function.randomUUID10().toUpperCase();//10 byte Messsage ID
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.messageId.setValue(message_id.getBytes());
		obj.tliHdr.messageCode.setValue(1L);
		obj.tliHdr.destNodeName.setValue(destNodeName.getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);

		obj.dataAppl.setValue("".getBytes());
		
		message_id = Function.randomUUID10().toUpperCase();//10 byte Messsage ID
		YlUpl upl = new YlUpl();
		String confirmation_flag = "3";//‘0’-none, ‘3’-Application-toApplication
		//Destination Routing Code (DRC) TSA - Application Confirmation 	TSS - Site-to-Site Confirmation
		String error_code = " ";//values are C (confirmation flag),	M (correlation ID), O (source node name), or D (DRC). Default is blank.
		String upl_header = confirmation_flag+"OC"+getCorrelationID()+"O"+srcNodeName.substring(0,11)+drc+error_code;
		upl.uplHeader.setValue(upl_header.getBytes());
		
		obj.tliHdr.messageId.setValue(message_id.getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		
		upl.upl.setValue(upl_data.getBytes());
		
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		BerOutputStream outBs = new BerOutputStream(outStream);
		try {
			upl.encode(outBs);
			obj.dataAppl.setValue(outStream.toByteArray());
//			obj.dataAppl.writeElement(outBs);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return obj;
	}	

	public YlDcmMsg NewMgiMessage(long  message_code,String dest_node_name) {
		YlDcmMsg obj = new YlDcmMsg();
		String message_id = Function.randomUUID10().toUpperCase();//10 byte Messsage ID
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.messageId.setValue(message_id.getBytes());
		obj.tliHdr.messageCode.setValue(message_code);
		obj.tliHdr.destNodeName.setValue(dest_node_name.getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);

		obj.dataAppl.setValue("".getBytes());
		return obj;
	}	
	public YlDcmMsg NewGDMessage(boolean bReply) {
		long  message_code=1L;//Good Day
		String dest_node_name=destNodeName;
		if (bReply) {
			dest_node_name="";
		}
		YlDcmMsg obj = NewMgiMessage(message_code,dest_node_name);
		return obj;
	}	
	public YlDcmMsg NewGNMessage() {
		long  message_code=5L;//Good Night
		String dest_node_name=destNodeName;
		YlDcmMsg obj = NewMgiMessage(message_code,dest_node_name);
		return obj;
	}	
	public YlDcmMsg NewGBMessage() {
		long  message_code=6L;//Good Bye, Disconnect Request
		String dest_node_name=destNodeName;
		YlDcmMsg obj = NewMgiMessage(message_code,dest_node_name);
		return obj;
	}	
	public void TestCase1() {
		System.out.println("----Start Test Case 1");
		long  message_code=1L;
		String dest_node_name=destNodeName;
		YlDcmMsg obj = NewMgiMessage(message_code,dest_node_name);
		
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:GD");
		
		String upl_data = String.format("REQ-TEST:,%s,%s::%s:::\"TEST DATAAPPL\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "S10001" });
		obj = NewUplMessage(upl_data);

		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-TEST");
	}


	public void TestCase2() {
		String upl_data = String.format("REQ-TEST:,%s,%s::%s:::\"TestCase2\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "S10002" });
		YlDcmMsg obj = NewUplMessage(upl_data);
		System.out.println("----Start Test Case 2");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-TEST");
	}

	public void TestCase3() {
		String upl_data = String.format("REQ-TEST:,%s,%s::%s:::\"TestCase3\"",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "S10003" });
		YlDcmMsg obj = NewUplMessage(upl_data,"TSS");
		System.out.println("----Start Test Case 3");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);

		System.out.println("----write asn.1 message:REQ-TEST");
	}

	public void TestCase4() {
		String upl_data = String.format("REQ-TEST:,%s,%s::%s:::\"TestCase4\"",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "S10004" });
		YlDcmMsg obj = NewUplMessage(upl_data);
		System.out.println("----Start Test Case 4");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);

		System.out.println("----write asn.1 message:REQ-TEST");
	}

	public void TestCase5() {
		String dataLong = "";
		for (int i = 0; i < 5001; i++)
			dataLong = dataLong + "z";
		String upl_data = String.format("REQ-TEST:,%s,%s::%s:::\"%s\"",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "S10005", dataLong });
		YlDcmMsg obj = NewUplMessage(upl_data);
		System.out.println("----Start Test Case 5");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);

		System.out.println("----write asn.1 message:REQ-TEST");
	}
	public void TestCase6() {
		System.out.println("----Start Test Case 6");
		String dataLong = "";
		for (int i = 0; i < 10; i++)
			dataLong = dataLong + "z";
		String upl_data = String.format("REQ-TEST:,%s,%s::%s:::\"%s\"",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "S10006", dataLong });
		YlDcmMsg obj = NewUplMessage(upl_data);
		System.out.println("----Start Test Case 6");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);

		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-TEST");
	}

	public void TestCase7() {
		System.out.println("----Start Test Case 7");

		String dataLong = "";
		for (int i = 0; i < 150001; i++)
			dataLong = dataLong + "x";
		String upl_data = String.format("REQ-TEST:,%s,%s::%s:::\"%s\"",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "S10007", dataLong });
		YlDcmMsg obj = NewUplMessage(upl_data);
		System.out.println("----Start Test Case 6");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);

		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-TEST");
	}

	public void TestCase15() {
		System.out.println("----Start Test Case 15");

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,NPA=%s,AC=S;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro,test_npa });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case 15");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);

		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase16() {
		System.out.println("----Start Test Case 16");

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,QT=05,AC=S;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case 16");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);

		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase17() {
		System.out.println("----Start Test Case 17");

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=S,NPA=833,NXX=220,QT=10,CONT=\"Y\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case 17");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);

		System.out.println("----write asn.1 message:REQ-NSR");

	}

	public void TestCase18() {
		System.out.println("----Start Test Case 18");

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=S,QT=02,NUM=\"800*******\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case 18");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);

		System.out.println("----write asn.1 message:REQ-NSR");
		
		
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 18");
	}

	public void TestCase19() {
		System.out.println("----Start Test Case 19");

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=S,QT=07,NUM=\"**********\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case 19");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);

		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase20() {
		System.out.println("----Start Test Case 20");

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=S,QT=04,NPA=321, NXX=433,CONT=\"N\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case 20");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);

		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase21() {
		System.out.println("----Start Test Case 21");

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=S,QT=09,NXX=433,CONT=\"Y\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case 21");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);

		System.out.println("----write asn.1 message:REQ-NSR");

		TestCaseNumber = 0;
		System.out.println("----Start Test Case 21");

	}

	public void TestCase22() {
		System.out.println("----Start Test Case 22");

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=S,QT=03,NPA=603,NXX=433,LINE=5060,CONT=\"N\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case 22");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);

		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase23() {
		System.out.println("----Start Test Case 23");

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=S,QT=06,NXX=433,LINE=5060,CONT=\"N\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case 23");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);

		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase24() {
		System.out.println("----Start Test Case 24");

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=S,QT=01,NPA=%s,LINE=5060;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro,test_npa });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case 24");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);

		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase25() {
		System.out.println("----Start Test Case 25");

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=S,QT=08,LINE=5060,CONT=\"Y\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case 25");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);

		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase26() {
		System.out.println("----Start Test Case 26");

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=S,QT=07,NUM=\"****561234\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case 26");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);

		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase27() {
		System.out.println("----Start Test Case 27");

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=S,QT=07,NUM=\"%s\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro,test_number1 });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case 27");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);

		System.out.println("----write asn.1 message:REQ-NSR");
	}
	
	//NUMBER QUERY (REQ-NSR)

	public void TestCase28() {
		String sNo = "28";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=Q,NUM=\"%s\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro,test_number });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);

		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase29() {
		String sNo = "29";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=Q,NUM=\"%s\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro ,test_number});
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);

		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase30() {
		String sNo = "30";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=Q,NUM=\"3333230000\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);

		System.out.println("----write asn.1 message:REQ-NSR");
		
	}

	public void TestCase31() {
		String sNo = "31";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=Q,NUM=\"3333230000\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);

		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase32() {
		String sNo = "32";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=Q,NUM=\"3333230000\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);

		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase33() {
		String sNo = "33";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=Q,NUM=\"3333230000\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);

		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase34() {
		String sNo = "34";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=Q,NUM=\"3333230000\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);

		System.out.println("----write asn.1 message:REQ-NSR");

	}

	public void TestCase35() {
		String sNo = "35";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=Q,NUM=\"3333230000\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);

		System.out.println("----write asn.1 message:REQ-NSR");

	}

	public void TestCase36() {
		String sNo = "36";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=R,NCON=ncon,CTEL=1234567890;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);

		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase37() {
		String sNo = "37";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=R,NPA=221,NCON=ncon,CTEL=1234567890, NOTES=notes;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);

		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase38() {
		String sNo = "38";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=R,QT=10,CONT=\"Y\",NCON=ncon,CTEL=1234567890,NOTES=notes;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);

		System.out.println("----write asn.1 message:REQ-NSR");

	}

	public void TestCase39() {
		String sNo = "39";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=R,QT=02,NCON=ncon,CTEL=1234567890,NOTES=notes:NUM=\"805***0000\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);

		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase40() {
		String sNo = "40";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=R,QT=09,NCON=ncon,CTEL=1234567890,NOTES=notes:NUM=\"**********\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);

		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase41() {
		String sNo = "41";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=R,NCON=ncon,CTEL=1234567890,NOTES=notes:QT=03:NUM=6112345690:NUM=6112345691:NUM=6112345692;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);

		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase42() {
		String sNo = "42";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=R,NCON=ncon,CTEL=1234567890,NOTES=notes:QT=08:NUM=6112345690:NUM=6112345691:NUM=6112345692:NUM=6112345693:NUM=6112345694:NUM=6112345695:NUM=6112345696:NUM=6112345697;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);

		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase43() {
		String sNo = "43";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=R,NCON=ncon,CTEL=1234567890,NOTES=notes,NPA=211,NXX=308,QT=04;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);

		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase44() {
		String sNo = "44";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=R,NCON=ncon,CTEL=1234567890,CONT=\"Y\",NXX=211,QT=07;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);

		System.out.println("----write asn.1 message:REQ-NSR");
		
		
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 44");
	}

	public void TestCase45() {
		String sNo = "45";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=R,NPA=202,NXX=303,LINE=0088,NCON=ncon,CTEL=1234567890,CONT=\"N\",QT=05;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);

		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase46() {
		String sNo = "46";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=R,NXX=203,LINE=0808,QT=06,CONT=\"Y\",CON=ncon,CTEL=ctel,NOTES=notes;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase47() {
		String sNo = "47";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=R,NPA=805,LINE=0089,QT=02,CONT=\"N\",NCON=ncon,CTEL=ctel,NOTES=notes;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase48() {
		String sNo = "48";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=R,LINE=0805,QT=07,CONT=\"Y\", NCON=ncon,CTEL=ctel;\",",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-NSR");

	}

	public void TestCase49() {
		String sNo = "49";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=S,LINE=0805,QT=10,NXX=999,LINE=9995, CONT=\"N\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase50() {
		String sNo = "50";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=R,QT=10,NUM=\"8000*00000\", NCON=ncon,CTEL=ctel;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-NSR");
		
		
	}

	public void TestCase51() {
		String sNo = "51";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=R,QT=10,CONT=\"Y\",NCON=ncon,CTEL=ctel,NOTES=notes;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase52() {
		String sNo = "52";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=R,QT=10,CONT=\"Y\",NCON=ncon,CTEL=ctel,NOTES=notes;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase53() {
		String sNo = "53";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=S,QT=11,CONT=\"X\"",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase54() {
		String sNo = "54";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=R,QT=00,CONT=\"Z\",NCON=\"\",CTEL=\"BADNUMBER\",NOTES=notes,NXX=BAD,LINE=GLOP;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase55() {
		String sNo = "55";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=R,QT=10,CONT=\"Y\",NCON=ncon,CTEL=ctel,NOTES=notes;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase56() {
		String sNo = "56";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=S,QT=5,NXX=999;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase57() {
		String sNo = "57";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSC:,%s,%s:::::ID=%s,RO=%s,AC=C,NUM=2013330001,RU=ru,NEWRO=newro,NCON=ncon,CTEL=ctel,NOTES=notes;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSC");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-NSC");
	}

	public void TestCase58() {
		String sNo = "58";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSC:,%s,%s:::::ID=%s,RO=%s,AC=C,NUM=2013330001,RU=newru,NEWRO=newro;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSC");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-NSC");
	}

	public void TestCase59() {
		String sNo = "59";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSC:,%s,%s:::::ID=%s,RO=%s,AC=C,NUM=2013330001,NCON=newncon,CTEL=newctel,NOTES=notes;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSC");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-NSC");
	}

	public void TestCase60() {
		String sNo = "60";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSC:,%s,%s:::::ID=%s,RO=%s,AC=S,NUM=2013330001;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSC");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-NSC");
	}

	public void TestCase61() {
		String sNo = "61";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSC:,%s,%s:::::ID=%s,RO=%s,AC=S,NUM=2013330001;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSC");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-NSC");
		
	}

	public void TestCase62() {
		String sNo = "62";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSC:,%s,%s:::::ID=%s,RO=%s,AC=C,NUM=2013330001,NCON=ncon;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSC");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-NSC");
	}

	public void TestCase63() {
		String sNo = "63";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSC:,%s,%s:::::ID=%s,RO=%s,AC=C,NUM=2013330001,CTEL=ctel;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSC");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-NSC");
	}

	public void TestCase64() {
		String sNo = "64";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSC:,%s,%s:::::ID=%s,RO=%s,AC=C,NUM=2013330001,NOTES=notes;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSC");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-NSC");
		
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 64");
	}

	public void TestCase65() {
		String sNo = "65";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSC:,%s,%s:::::ID=%s,RO=%s,AC=C,NUM=2013330001,RU=ru,NEWRO=newro;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSC");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-NSC");
		
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 64");
	}

	public void TestCase66() {
		String sNo = "66";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSC:,%s,%s:::::ID=%s,RO=%s,AC=S,NUM=锟斤拷800ABCDEFG锟斤拷;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSC");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-NSC");
	}

	public void TestCase67() {
		String sNo = "67";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSC:,%s,%s:::::ID=%s,RO=%s,AC=S,NUM=\"2013330001\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSC");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-NSC");
	}

	public void TestCase68() {
		String sNo = "68";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSC:,%s,%s:::::ID=%s,RO=%s,AC=S,NUM=\"2013330001\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSC");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-NSC");
	}

	public void TestCase69() {
		String sNo = "69";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSC:,%s,%s:::::ID=%s,RO=%s,AC=S,NUM=\"2013330001\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSC");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-NSC");
	}

	public void TestCase70() {
		String sNo = "70";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=S,NUM=\"&&&NNNNNNN\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase71() {
		String sNo = "71";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=S,NUM=\"&&&NNNNNNN\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase72() {
		String sNo = "72";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=S,QT=01,NUM=\"&&&N&&NN*N\",NPA=208;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase73() {
		String sNo = "73";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=S,QT=01,NUM=\"201&NNNNNN\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-NSR");
	}
	public void TestCase74() {
		String sNo = "74";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=S,QT=10,NUM=\"800550709*\",CONT=\"Y\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-NSR");
	}
	public void TestCase75() {
		String sNo = "75";
		System.out.println("----Start Test Case "+sNo);

		//Send a REQ-NSR transaction to reserve 8001144443
		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=R:QT=01:NUM=\"8001144443\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		
		//Submit another REQ-NSR transaction with 800&&44443 in the NUM field and 10 in the QTY field.
		upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=S,QT=10,NUM=\"800&&44443\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		obj = NewUplMessage(upl_data,"CSR");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase76() {
		String sNo = "76";
		System.out.println("----Start Test Case "+sNo);

		//1. Randomly search 10 numbers.
		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=S,QT=10;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		
		//2. Reserve the first number returned.
		upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=R,QT=1,NUM=\"8005544443\": NCON=ncon,CTEL=ctel;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		obj = NewUplMessage(upl_data,"CSR");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		//3. Reserve the 10 returned numbers from Step 1
		upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=R,QT=10,NUM=\"8005144443\":NUM=\"8005244443\":" + 
				"NUM=\"8005344443\":NUM=\"8005444443\":NUM=\"8005544443\":NUM=\"8005644443\":NUM=\"8005744443\":NUM=\"8005844443\":NUM=\"8005944443\":NUM=\"8005044443\":" + 
				"NCON=ncon,CTEL=ctel;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		obj = NewUplMessage(upl_data,"CSR");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);

		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-NSR");
		
	}
	public void TestCase77() {
		String sNo = "77";
		System.out.println("----Start Test Case "+sNo);

		//1. Randomly search 10 numbers.
		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=S,QT=10;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		
		//2. Reserve the third number returned.
		upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=R,QT=1,NUM=\"8005524443\": NCON=ncon,CTEL=ctel;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		obj = NewUplMessage(upl_data,"CSR");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		//3. Reserve the 10 returned numbers from Step 1
		upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=R,QT=10,NUM=\"8005144443\":NUM=\"8005244443\":" + 
				"NUM=\"8005344443\":NUM=\"8005444443\":NUM=\"8005544443\":NUM=\"8005644443\":NUM=\"8005744443\":NUM=\"8005844443\":NUM=\"8005944443\":NUM=\"8005044443\":" + 
				"NCON=ncon,CTEL=ctel;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		obj = NewUplMessage(upl_data,"CSR");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);

		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-NSR");
		
	}
	public void TestCase78() {
		String sNo = "78";
		System.out.println("----Start Test Case "+sNo);

		//1. Randomly search 10 numbers.
		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=S,QT=10;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		
		//2. Reserve the tenth number returned.
		upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=R,QT=1,NUM=\"8005044443\": NCON=ncon,CTEL=ctel;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		obj = NewUplMessage(upl_data,"CSR");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		//3. Reserve the 10 returned numbers from Step 1
		upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=R,QT=10,NUM=\"8005144443\":NUM=\"8005244443\":" + 
				"NUM=\"8005344443\":NUM=\"8005444443\":NUM=\"8005544443\":NUM=\"8005644443\":NUM=\"8005744443\":NUM=\"8005844443\":NUM=\"8005944443\":NUM=\"8005044443\":" + 
				"NCON=ncon,CTEL=ctel;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		obj = NewUplMessage(upl_data,"CSR");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);

		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-NSR");
		
	}
	public void TestCase79() {
		String sNo = "79";
		System.out.println("----Start Test Case "+sNo);

		//1. Randomly search 10 numbers.
		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=S,QT=10;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		
		//2. Reserve the second, third and ninth numbers returned.
		upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=R,QT=3,NUM=\"8005244443\":NUM=\"8005344443\":NUM=\"8005944443\": NCON=ncon,CTEL=ctel;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		obj = NewUplMessage(upl_data,"CSR");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		//3. Reserve the 10 returned numbers from Step 1
		upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=R,QT=10,NUM=\"8005144443\":NUM=\"8005244443\":" + 
				"NUM=\"8005344443\":NUM=\"8005444443\":NUM=\"8005544443\":NUM=\"8005644443\":NUM=\"8005744443\":NUM=\"8005844443\":NUM=\"8005944443\":NUM=\"8005044443\":" + 
				"NCON=ncon,CTEL=ctel;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		obj = NewUplMessage(upl_data,"CSR");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);

		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-NSR");
		
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 76");

	}

	public void TestCase80() {
		String sNo = "80";
		System.out.println("----Start Test Case "+sNo);

		//1. Randomly search 10 numbers.
		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=S,QT=10;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		
		//2. Reserve the first through ninth numbers returned.
		upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=R,QT=9,NUM=\"8005144443\":NUM=\"8005244443\":" + 
				"NUM=\"8005344443\":NUM=\"8005444443\":NUM=\"8005544443\":NUM=\"8005644443\":NUM=\"8005744443\":NUM=\"8005844443\":NUM=\"8005944443\":" + 
				"NCON=ncon,CTEL=ctel;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		obj = NewUplMessage(upl_data,"CSR");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		//3. Reserve the 10 returned numbers from Step 1
		upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=R,QT=10,NUM=\"8005144443\":NUM=\"8005244443\":" + 
				"NUM=\"8005344443\":NUM=\"8005444443\":NUM=\"8005544443\":NUM=\"8005644443\":NUM=\"8005744443\":NUM=\"8005844443\":NUM=\"8005944443\":NUM=\"8005044443\":" + 
				"NCON=ncon,CTEL=ctel;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		obj = NewUplMessage(upl_data,"CSR");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);

		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-NSR");
		
	}
	public void TestCase81() {
		String sNo = "81";
		System.out.println("----Start Test Case "+sNo);

		//1. Randomly search 10 numbers.
		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=S,QT=10;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		System.out.println("----Start Test Case "+sNo);
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);
		
		//2. Reserve the 10 returned numbers from Step 1.
		upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=R,QT=10,NUM=\"8005144443\":NUM=\"8005244443\":" + 
				"NUM=\"8005344443\":NUM=\"8005444443\":NUM=\"8005544443\":NUM=\"8005644443\":NUM=\"8005744443\":NUM=\"8005844443\":NUM=\"8005944443\":NUM=\"8005044443\":" + 
				"NCON=ncon,CTEL=ctel;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		obj = NewUplMessage(upl_data,"CSR");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);

		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-NSR");
		
	}
	public void TestCase82() {
		String sNo = "82";
		System.out.println("----Start Test Case "+sNo);

		//The OS sends the following message: REQ-NSR message to reserve 10 random numbers in the same NPA.
		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=R,QT=10,NUM=\"8005144443\":NUM=\"8005244443\":" + 
				"NUM=\"8005344443\":NUM=\"8005444443\":NUM=\"8005544443\":NUM=\"8005644443\":NUM=\"8005744443\":NUM=\"8005844443\":NUM=\"8005944443\":NUM=\"8005044443\":" + 
				"NCON=ncon,CTEL=ctel;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);

		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-NSR");
		
	}
	public void TestCase83() {
		String sNo = "83";
		System.out.println("----Start Test Case "+sNo);

		//The OS sends the following message: REQ-NSR message to reserve 10 random numbers in the same NPA.
		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=R,QT=10,NUM=\"8005144443\":NUM=\"8005244443\":" + 
				"NUM=\"8005344443\":NUM=\"8005444443\":NUM=\"8005544443\":NUM=\"8005644443\":NUM=\"8005744443\":NUM=\"8005844443\":NUM=\"8005944443\":NUM=\"8005044443\":" + 
				"NCON=ncon,CTEL=ctel;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);

		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-NSR");
		
	}
	public void TestCase84() {
		String sNo = "84";
		System.out.println("----Start Test Case "+sNo);

		//The OS sends the following message: REQ-NSR message to reserve 10 random numbers in the same NPA.
		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=R,QT=10,NUM=\"8005144443\":NUM=\"8005244443\":" + 
				"NUM=\"8005344443\":NUM=\"8005444443\":NUM=\"8005544443\":NUM=\"8005644443\":NUM=\"8005744443\":NUM=\"8005844443\":NUM=\"8005944443\":NUM=\"8005044443\":" + 
				"NCON=ncon,CTEL=ctel;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);

		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-NSR");
		
	}
	public void TestCase85() {
		String sNo = "85";
		System.out.println("----Start Test Case "+sNo);

		//The OS sends the following message: REQ-NSR message to reserve 10 random numbers in the same NPA.
		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=R,QT=10,NUM=\"8005144443\":NUM=\"8005244443\":" + 
				"NUM=\"8005344443\":NUM=\"8005444443\":NUM=\"8005544443\":NUM=\"8005644443\":NUM=\"8005744443\":NUM=\"8005844443\":NUM=\"8005944443\":NUM=\"8005044443\":" + 
				"NCON=ncon,CTEL=ctel;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);

		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-NSR");
		
	}
	public void TestCase86() {
		String sNo = "86";
		System.out.println("----Start Test Case "+sNo);

		//The OS sends the following message: REQ-NSR message to reserve 10 random numbers in the same NPA.
		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=R,QT=10,NUM=\"8005144443\":NUM=\"8005244443\":" + 
				"NUM=\"8005344443\":NUM=\"8005444443\":NUM=\"8005544443\":NUM=\"8005644443\":NUM=\"8005744443\":NUM=\"8005844443\":NUM=\"8005944443\":NUM=\"8005044443\":" + 
				"NCON=ncon,CTEL=ctel;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);

		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-NSR");
		
	}
	public void TestCase87() {
		String sNo = "87";
		System.out.println("----Start Test Case "+sNo);

		//REQ-NSR message to reserve 10 specific non-spared numbers	(reserved, working, transitional) in a different Resp Org in the same NPA.
		String upl_data = String.format("REQ-NSR:,%s,%s:::::ID=%s,RO=%s,AC=R,QT=10,NUM=\"8005144443\":NUM=\"8005244443\":" + 
				"NUM=\"8005344443\":NUM=\"8005444443\":NUM=\"8005544443\":NUM=\"8005644443\":NUM=\"8005744443\":NUM=\"8005844443\":NUM=\"8005944443\":NUM=\"8005044443\":" + 
				"NCON=ncon,CTEL=ctel;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CSR");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);

		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-NSR");
		
	}
	public void TestCase88() {
		String sNo = "88";
		System.out.println("----Start Test Case "+sNo);
		//No UNS-RSV message is received by the OS.
	}
	public void TestCase89() {
		String sNo = "89";
		System.out.println("----Start Test Case "+sNo);
		//No UNS-RSV message is received by the OS.
	}
	public void TestCase90() {
		String sNo = "90";
		System.out.println("----Start Test Case "+sNo);
		//received: UNS-RSV,date,time:::::RO=ro,CNT=cnt,NUM=num1;
	}
	public void TestCase91() {
		String sNo = "91";
		System.out.println("----Start Test Case "+sNo);
		//received: UNS-RSV,date,time:::::RO=ro,CNT=cnt,NUM=num1,NUM=num2,NUM=num3,NUM=num4,NUM=num5,NUM=num6,NUM=num7,NUM=num8,NUM=num9,NUM=num10;
	}
	//no test for 92 and 93
	
	public void TestCase94() {
		String sNo = "94";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-CRA:,%s,%s:::::ID=%s,RO=%s,AC=N,NUM=\"8005144443\",ED=ed,ET=et,INTERC=interc,SO=so:" 
				+ "CNT6=01:ANET=US:CNT8=01:LN=NoListing Name Provided:CNT9=01:TEL=tel,LNS=lns,CITY=city,LSO=lso;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CRU");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);

		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-CRA");
	}

	public void TestCase95() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 95");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test95".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=num,ED=\"NOW\",INTERC=interc, SO=so:CNT6=01:ANET=US:CNT8=01:LN=No Listing Name Provided: CNT9=01:TEL=tel,LNS=lns",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase96() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 96");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test96".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=num,ED=\"NOW\",INTRAC=intrac, SO=so:CNT4=01:AAC=aac:CNT8=01:LN=No Listing Name Provided: CNT9=01:TEL=tel,LNS=lns,LSO=lso",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase97() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 97");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test97".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=num,ED=ed,ET=et, INTRAC=intrac,SO=so:CNT4=01:AAC=aac:CNT8=01:LN=No Listing Name Provided:CNT9=01:TEL=tel,LNS=lns,CITY=city,LSO=lso;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase98() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 98");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test98".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=num,ED=ed,ET=et"
						+ ",INTERC=interc,INTRAC=intrac,SF=sf: CNT7=20:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:"
						+ "ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:"
						+ "ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta: CNT8=01:LN=No Listing Name Provided:CNT9=02:TEL=tel,LNS=lns,CITY=city,"
						+ "LSO=lso,UTS=uts:TEL=tel,LNS=lns,CITY=city,LSO=lso,UTS=uts;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase99() {
		String sNo = "99";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-CRA:,%s,%s:::::ID=%s,RO=%s,AC=N,NUM=\"8005144443\",ED=\"NOW\",INTERC=interc,INTRAC=intrac,HDD=N,SF=sf:" 
				+ "CNT3=04:ALBL=albl:ALBL=albl:ALBL=albl:ALBL=albl:CNT8=01:LN=No Listing Name Provided:CNT9=01:TEL=tel,LNS=lns,CITY=city,LSO=lso;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CRU");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);

		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-CRA");
	}

	public void TestCase100() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 100");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test100".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=num,ED=\"NOW\","
						+ "INTERC=interc,INTRAC=intrac,HDD=N,SF=sf: CNT3=04:ALBL=albl:ALBL=albl:ALBL=albl:ALBL=albl:"
						+ "CNT8=01:LN=No Listing Name Provided:CNT9=01:TEL=tel,LNS=lns,CITY=city,LSO=lso;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase101() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 101");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test101".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=num,ED=\"NOW\","
						+ "INTERC=interc,INTRAC=intrac,HDD=N,SF=sf: CNT3=04:ALBL=albl:ALBL=albl:ALBL=albl:ALBL=albl:"
						+ "CNT8=01:LN=No Listing Name Provided:CNT9=01:TEL=tel,LNS=lns,CITY=city,LSO=lso;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase102() {
		String sNo = "102";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-CRA:,%s,%s:::::ID=%s,RO=%s,AC=N,NUM=\"8005144443\",ED=\"NOW\",INTERC=interc,INTRAC=intrac,HDD=N,SF=sf:" 
				+ "CNT3=04:ALBL=albl:ALBL=albl:ALBL=albl:ALBL=albl:"
				+ "CNT8=01:LN=No Listing Name Provided:CNT9=01:TEL=tel,LNS=lns,CITY=city,LSO=lso;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CRU");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);

		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-CRA");
	}

	public void TestCase103() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 103");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test103".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=num,ED=\"NOW\","
						+ "INTERC=interc,INTRAC=intrac,HDD=N,SF=sf: CNT3=04:ALBL=albl:ALBL=albl:ALBL=albl:ALBL=albl:"
						+ "CNT8=01:LN=No Listing Name Provided:CNT9=01:TEL=tel,LNS=lns,CITY=city,LSO=lso;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase104() {
		String sNo = "104";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-CRA:,%s,%s:::::ID=%s,RO=%s,AC=T,NUM=\"8005144443\",ED=\"NOW\",INTERC=interc,INTRAC=intrac,HDD=N,SF=sf:" 
				+ "CNT3=04:ALBL=albl:ALBL=albl:ALBL=albl:ALBL=albl:"
				+ "CNT8=01:LN=No Listing Name Provided:CNT9=01:TEL=tel,LNS=lns,CITY=city,LSO=lso;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CRU");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);

		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-CRA");
	}

	public void TestCase105() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 105	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test105".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=num,ED=\"NOW\","
						+ "INTERC=interc,INTRAC=intrac,HDD=N,SF=sf: CNT3=04:ALBL=albl:ALBL=albl:ALBL=albl:ALBL=albl:"
						+ "CNT8=01:LN=No Listing Name Provided:CNT9=01:TEL=tel,LNS=lns,CITY=city,LSO=lso;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase106() {
		String sNo = "106";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-CRA:,%s,%s:::::ID=%s,RO=%s,AC=D,NUM=\"8005144443\",ED=\"NOW\",INTERC=interc,INTRAC=intrac,HDD=N,SF=sf:" 
				+ "CNT3=04:ALBL=albl:ALBL=albl:ALBL=albl:ALBL=albl:"
				+ "CNT8=01:LN=No Listing Name Provided:CNT9=01:TEL=tel,LNS=lns,CITY=city,LSO=lso;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CRU");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);

		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-CRA");
		
	}

	public void TestCase107() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 107	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test107".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=D,NUM=num,ED=\"NOW\","
						+ "INTERC=interc,INTRAC=intrac,HDD=N,SF=sf: CNT3=04:ALBL=albl:ALBL=albl:ALBL=albl:ALBL=albl:"
						+ "CNT8=01:LN=No Listing Name Provided:CNT9=01:TEL=tel,LNS=lns,CITY=city,LSO=lso;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase108() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 108	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test108".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=T,NUM=num,ED=\"NOW\","
						+ "INTERC=interc,INTRAC=intrac,HDD=N,SF=sf: CNT3=04:ALBL=albl:ALBL=albl:ALBL=albl:ALBL=albl:"
						+ "CNT8=01:LN=No Listing Name Provided:CNT9=01:TEL=tel,LNS=lns,CITY=city,LSO=lso;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase109() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 109	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test109".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=T,NUM=num,ED=\"NOW\","
						+ "INTERC=interc,INTRAC=intrac,HDD=N,SF=sf: CNT3=04:ALBL=albl:ALBL=albl:ALBL=albl:ALBL=albl:"
						+ "CNT8=01:LN=No Listing Name Provided:CNT9=01:TEL=tel,LNS=lns,CITY=city,LSO=lso;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase110() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 110	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test110".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=T,NUM=num,ED=\"NOW\","
						+ "INTERC=interc,INTRAC=intrac,HDD=N,SF=sf: CNT3=04:ALBL=albl:ALBL=albl:ALBL=albl:ALBL=albl:"
						+ "CNT8=01:LN=No Listing Name Provided:CNT9=01:TEL=tel,LNS=lns,CITY=city,LSO=lso;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase111() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 111	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test111".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=D,NUM=num,ED=\"NOW\","
						+ "INTERC=interc,INTRAC=intrac,HDD=N,SF=sf: CNT3=04:ALBL=albl:ALBL=albl:ALBL=albl:ALBL=albl:"
						+ "CNT8=01:LN=No Listing Name Provided:CNT9=01:TEL=tel,LNS=lns,CITY=city,LSO=lso;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase112() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 112	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test112".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=num,ED=ed,ET=et, INTERC=interc,INTRAC=intrac,SO=so:CNT4=34:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac: AAC=aac:AAC=aac:AAC=aac:"
						+ "AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:CNT5=34:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:"
						+ "ALAT=alat:ALAT=alat:ALAT=alat: ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat: ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:CNT6=08:ANET=anet:ANET=anet:ANET=anet:ANET=anet:ANET=anet:ANET=anet:"
						+ "ANET=anet:ANET=anet:CNT7=46:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:"
						+ "ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta: CNT8=01:LN=NoListing Name Provided:CNT9=01:TEL=tel,LNS=lns,CITY=city, LSO=lso;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase113() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 113	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test113".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=num,ED=ed,ET=et,CU=cu,BILL=bill,INTERC=interc,INTRAC=intrac,ABN=abn,DAU=dau,DAT=dat,DD=dd,HDD=hdd,LI=li,RAO=rao,SF=sf,NOTE=note,AGENT=agent,CUS=cus,LA=la, CBI=cbi,"
						+ "NCON=ncon,CTEL=ctel:CNT3=16:ALBL=albl:ALBL=albl:ALBL=albl:ALBL=albl:ALBL=albl:ALBL=albl:ALBL=albl:ALBL=albl:ALBL=albl:ALBL=albl:ALBL=albl:ALBL=albl:ALBL=albl:ALBL=albl:ALBL=albl:ALBL=albl:CNT4=34:"
						+ "AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:"
						+ "AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac: AAC=aac:AAC=aac:AAC=aac:AAC=aac:CNT5=34:ALAT=alat:ALAT=alat: ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat: ALAT=alat:"
						+ "ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat: ALAT=alat: ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat: ALAT=alat:"
						+ "CNT6=08:ANET=anet: ANET=anet:ANET=anet:ANET=anet:ANET=anet:ANET=anet:ANET=anet:ANET=anet:CNT7=46:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:"
						+ "ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:"
						+ "ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta: ASTA=asta:ASTA=asta:ASTA=asta: ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta: ASTA=asta:CNT8=09:LN=No Listing Name Provided:LN=No Listing Name Provided:LN=No Listing Name"
						+ "Provided:LN=No Listing Name Provided:LN=No Listing Name Provided :LN=No Listing NameProvided:LN=No Listing Name Provided:LN=No Listing Name Prov ided: LN=No Listing Name Provided:CNT9=02:TEL=tel,LNS=lns,CITY=city,FSO=fso,HML=hml,LSIS=lsis,"
						+ "LSO=lso,SFG=sfg,STN=stn,UTS=uts:TEL=tel,LNS=lns,CITY=city,FSO=fso, HML=hml,LSIS=lsis,LSO=lso,SFG=sfg,STN=stn,UTS=uts;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase114() {
		String sNo = "114";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-CRA:,%s,%s:::::ID=%s,RO=%s,AC=N,NUM=\"8005144443\",ED=ed,INTERC=interc,INTRAC=intrac,HDD=Y,SO=so:" 
				+ "CNT6=01:ANET=anet:CNT8=01: LN=No Listing Name Provided:CNT9=01:TEL=tel,LNS=lns,CITY=city,LSO=lso;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CRU");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);

		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-CRA");
	}

	public void TestCase115() {
		String sNo = "115";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-CRA:,%s,%s:::::ID=%s,RO=%s,AC=N,NUM=\"8005144443\",ED=ed,ET=et,REFER=N;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CRU");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);

		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-CRA");
	}

	public void TestCase116() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 116	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test116".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=num,ED=ed,ET=et,SEFD=sefd: CNT8=01:LN=ln;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase117() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 117	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test117".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=num,SEFD=sefd, SO=so;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase118() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 118	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test118".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=D,NUM=num,SEFD=sefd,REFER=N,EINT=eint;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase119() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 119	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test119".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=D,NUM=num,SEFD=sefd,REFER=N, SO=so,SF=\"\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase120() {
		String sNo = "120";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-CRA:,%s,%s:::::ID=%s,RO=%s,AC=D,NUM=\"8005144443\",ED=\"NOW\",REFER=N;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CRU");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);

		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-CRA");
	}

	public void TestCase121() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 121	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test121".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=num,INTERC=interc;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase122() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 122	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test122".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=num,ED=ed,ET=et,SEFD=sefd, LI=BL,NOTE=note,TELCO=telco, LA=la,CBI=cbi,NCON=ncon,CTEL=ctel:"
						+ "CNT5=20:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:"
						+ "CNT7=06:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase123() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 123	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test123".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=num,ED=ed,ET=et,SEFD=sefd,LI=LI,NOTE=note,LA=la:CNT6=01:ANET=anet:CNT7=01:ASTA=asta;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase124() {
		String sNo = "124";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-CRA:,%s,%s:::::ID=%s,RO=%s,AC=C,NUM=\"8005144443\",SEFD=sefd,NOTE=note,LA=la,SO=,SF=sf,NCON=ncon:"
				+ "CNT3=07:ALBL=albl:ALBL=albl:ALBL=albl:ALBL=albl:ALBL=albl:ALBL=albl:ALBL=albl:"
				+ "CNT9=01:TEL=tel,LNS=lns,CITY=city,LSO=lso,UTS=uts;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CRU");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);

		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-CRA");
	}

	public void TestCase125() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 125	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test125".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=num,ED=ed,ET=et,SEFD=sefd;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase126() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 126	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test126".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=num,SEFD=sefd, INTERC=interc;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase127() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 127	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test127".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=num,SEFD=sefd:CNT9=01:TEL=tel,LNS=lns,CITY=city,LSO=lso,UTS=uts;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase128() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 128	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test128".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=num,ED=ed,ET=et,CU=cu, SEFD=sefd,NEWRO=newro,INTERC=interc,INTRAC=intrac,ABN=abn,DAU=dau, DAT=dat,DD=dd,"
						+ "HDD=hdd,LI=li,RAO=rao,SF=sf,NOTE=note,AGENT=agent,CUS=cus,LA=la,CBI=cbi,NCON=ncon,CTEL=ctel:CNT3=16:ALBL=albl:ALBL=albl:ALBL=albl:ALBL=albl:ALBL=albl:ALBL=albl:ALBL=albl:ALBL=albl:ALBL=albl:"
						+ "ALBL=albl:ALBL=albl:ALBL=albl:ALBL=albl:ALBL=albl:ALBL=albl:ALBL=albl:CNT4=34:AAC=aac:AAC=aac:AAC=aac:AAC=aac: AAC=aac:AAC=aac:AAC=aac:"
						+ "AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:"
						+ "AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:AAC=aac:CNT5=34:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat: ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:"
						+ "ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:ALAT=alat:"
						+ "CNT6=08:ANET=anet:ANET=anet:ANET=anet:ANET=anet:ANET=anet:ANET=anet:ANET=anet:ANET=anet:CNT7=46:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:"
						+ "ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:"
						+ "ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta:ASTA=asta: CNT8=09:LN=NoListing Name Provided:LN=No Listing Name Provided: LN= No Listing Name Provided:LN=No Listing"
						+ "Name Provided: LN=No Listing Name Provided:LN=No Listing Name Provided: LN=NoListing Name Provided:LN=No Listing Name Prov ided: LN=No Listing Name Provided:CNT9=02:TEL=tel,LNS=lns,CITY=city,FSO=fso,"
						+ "HML=hml,LSIS=lsis,LSO=lso,SFG=sfg,STN=stn,UTS=uts:TEL=tel,LNS=lns, CITY=city,FSO=fso,HML=hml,LSIS=lsis,LSO=lso,SFG=sfg,STN=stn,UTS=uts;;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase129() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 129	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test129".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=num,SEFD=sefd,SO=so,SF=\"\",DAU=N:CNT6=02:ANET=anet:ANET=anet:CNT9=02:TEL=tel,LNS=lns,CITY=city, FSO=fso,"
						+ "HML=hml,LSIS=lsis,LSO=lso,SFG=sfg, STN=stn,UTS=uts: TEL=tel,LNS=lns,CITY=city,FSO=fso,HML=hml,LSIS=lsis,LSO=lso, SFG=sfg,STN=stn,UTS=uts;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase130() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 130	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test130".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=C,ED=\"NOW\",NUM=num, NEWRO=newro,SEFD=sefd;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase131() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 131	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test131".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=C,ED=\"NOW\",NUM=num,SEFD=sefd,INTERC=interc;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase132() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 132	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test132".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=C,ED=\"NOW\",NUM=num,SEFD=sefd,INTERC=interc;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase133() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 133	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test133".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=C,ED=\"NOW\",NUM=num,INTRAC=intrac,SO=so,CNT4=01:AAC=405:CNT8=01:LN=No List Name provided:CNT09=01:TEL=tel,LNS=lns,LSO=lso;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase134() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 134	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test134".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=C,ED=\"NOW\",NUM=num,"
						+ "INTRAC=intrac,NEWRO=newro,SO=so,CNT4=01:AAC=405:CNT8=01:LN=No Listing Name provided:CNT09=01:TEL=tel,LNS=lns,LSO=lso;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase135() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 135	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test135".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=C,ED=ed,ET=et,NUM=num, INTRAC=intrac,"
						+ "NEWRO=newro,SO=so,CNT6=01:ANET=US:CNT8=01:LN=No Listing Name"
						+ "provided:CNT09=01:TEL=tel,LNS=lns,CITY=city,LSO=lso;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase136() {
		String sNo = "136";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-CRA:,%s,%s:::::ID=%s,RO=%s,AC=T,NUM=\"8005144443\",ED=ed,ET=et;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CRU");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);

		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-CRA");
	}

	public void TestCase137() {
		String sNo = "137";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-CRA:,%s,%s:::::ID=%s,RO=%s,AC=T,NUM=\"8005144443\",ED=\"NOW\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CRU");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);

		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-CRA");
	}

	public void TestCase138() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 138	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test138".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=T,NUM=num,ED=ed,ET=et,SEFD=sefd,"
						+ "INTRAC=intrac,LA=la:CNT9=01:TEL=tel,LNS=lns,CITY=city,LSO=lso,UTS=uts;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase139() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 139	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test139".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=T,NUM=num,ED=ed,ET=et,SEFD=sefd, INTERC=interc;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase140() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 140	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test140".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=T,NUM=num,ED=ed,ET=et,SEFD=sefd, INTERC=interc;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase141() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 141	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test142".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=T,NUM=num,ED=ed,ET=et,SEFD=sefd,REFER=refer,EINT=eint;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase142() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 142	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test141".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=T,NUM=num,ED=\"NOW\",SEFD=sefd,REFER=refer,EINT=eint;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase143() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 143	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test143".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=T,NUM=num,ED=\"NOW\",SEFD=sefd, HDD=;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase144() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 144	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test144".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=R,NUM=num,ED=ed,ET=et;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase145() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 145	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test145".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=R,NUM=num,ED=ed,ET=et;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase146() {
		String sNo = "146";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-CRA:,%s,%s:::::ID=%s,RO=%s,AC=R,NUM=\"8005144443\",ED=ed,ET=et;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CRU");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);

		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-CRA");
	}

	public void TestCase147() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 147	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test147".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=X,NUM=num,ED=ed,ET=et;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase148() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 148	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test148".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=X,NUM=num,ED=ed,ET=et;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase149() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 149	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test149".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=X,NUM=num,ED=ed,ET=et;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase150() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 150	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test150".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=X,NUM=num,ED=ed,ET=et,DCSN=Y;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase151() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 151	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test151".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=X,NUM=num,ED=ed,ET=et,DCSN=N;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase152() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 152	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test152".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=MGIXXX01,RO=MGI01,AC=N,NUM=\"DIAL#\",ED=\"NOW\","
						+ "SO=N2323441,NOTE=\"BASIC RECORD\",INTERC=\"288\",INTRAC=\"OTC\":"
						+ "CNT3=01:ALBL=\"AOSMGI1\":CNT8=01:LN=\"TELCORDIA\":CNT9=002:TEL=\"DIAL#\","
						+ "LNS=800:TEL=\"POTS number\",LNS=800,LSO=NPANXX;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()) });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase153() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 153	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test153".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=num,ED=\"NOW\",INTRAC=intrac, SO=so:"
						+ "CNT4=01:AAC=405:CNT8=01:LN=No Listing Name Provided: CNT9=01:TEL=tel,LNS=lns,LSO=lso;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase154() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 154	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test154".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=num,ED=\"NOW\",INTRAC=intrac, SO=so:"
						+ "CNT4=01:AAC=aac:CNT8=01:LN=No Listing Name Provided: CNT9=01:TEL=tel,LNS=lns,LSO=lso:SO=so;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase155() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 155	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test155".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=num,ED=\"NOW\",INTRAC=intrac,"
						+ "SO=so:CNT4=01:AAC=aac:CNT8=01:LN=No Listing Name Provided:CNT9=01:TEL=tel,LNS=lns,LSO=lso;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase156() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 156	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test156".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=num,ED=01/02/96, ET=10:30A/"
						+ "C,SF=232344,INTERC=ATX,INTRAC=OTC,CU=\"NNNNNNNNN\",BILL=N,"
						+ "ABN=312345678903,DAU=N,DAT=N,DD=02/02/96,HDD=\"N\",LI=LI,"
						+ "RAO=\"123\":CNT4=01:AAC=aac: CNT8=01:LN=No Listing Name Provided:CNT9=01:TEL=tel,LNS=lns,LSO=lso;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase157() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 157	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test157".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=%s,RO=BANJ1,AC=W, NUM=\"XZZZZ35\",ED=\"11=01/96\",ET=\"NOW\","
						+ "SO=N345542323441, INTERC=\"ATX\",INTRAC=\"OTC\":CNT7=1:ASTA=AL:CNT8=1:"
						+ "LN=\"TELCORDIA\":CNT9=2:TEL=\"4053777200\",CITY=\"RRC\",LNS=1234,LSO=405377:TEL=\"8006723935\",LNS=1234;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase158() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 158	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test158".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=%s,RO=BANJ1,AC=C,NUM=\"6723938\",ED=\"11/01/96\",ET=\"10:30AC\",SF=\"242344\","
						+ "INTERC=\"OTC\",INTRAC=\"OTX\",CU=W,BILL=W,ABN=\"9999999999\", DAU=Q,DAT=R,DD=\"13/01/96\",HDD=W,LI=WW,RAO=415,NOTE=\"TEST CASE C2.10\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase159() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 159	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test159".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = "REQ-CRA:,1995-11-07,10-10-22-CST:::::ID=id,RO=ro,AC=C, NUM=\"6723938\",ED=\"11/01/96\","
				+ "ET=\"10:30A/C\",SF=\"242344\",INTERC=\"ATX\",INTRAC=\"ATX\",ABN=\"9999999999\",DAU=Y,LI=BL,RAO=415:"
				+ "CNT8=01:LN=\"TELCORDIA\":CNT9=01:TEL=\"8006723939\",LNS=1234";
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase160() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 160	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test160".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=\"ABCDEFG,ED=ed,ET=et;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase161() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 161	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test161".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=num,ED=ed,ET=et,SEFD=sefd,"
						+ "LI=XX,NOTE=note,TELCO=telco,LA=la,CBI=cbi,NCON=ncon,CTEL=ctel:CNT5=02:ALAT=AL:ALAT=AT:CNT7=06:"
						+ "ASTA=AA:ASTA=BB:ASTA=CC:ASTA=DD:ASTA=EE:ASTA=FF;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase162() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 162	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test162".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=num,ED=ed,ET=et, REFER=,EINT=\"\",SEFD=sefd;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase163() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 163	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test163".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=num,ED=\"NOW\",INTRAC=intrac,"
						+ "CNT4=01:AAC=aac:CNT8=01:LN=No Listing Name Provided: CNT9=01:TEL=tel,LNS=lns,LSO=lso;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase164() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 164	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test164".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRA:,%s,%s:::::ID=%s,ID=id,RO=ro,AC=X,NUM=\"8886723938\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase165() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 165	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test165".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=MGIXXX01,RO=MGI01,AC=N,NUM=\"DIAL#\",ED=\"NOW\","
						+ "SO=N2323441,NOTE=\"BASIC RECORD\",INTERC=\"222\",INTRAC=\"OTC\":CNT3=01:"
						+ "ALBL=\"AOSMGI1\":CNT8=01:LN=\"TELCORDIA\":CNT9=002:TEL=\"DIAL#\",LNS=800: TEL=\"POTS number\",LNS=800,LSO=NPANXX;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()) });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase166() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 166	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test166".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=num,ED=\"NOW\", SO=so,NOTE=\"BASIC RECORD\",INTERC=interc,"
						+ "INTRAC=intrac:CNT6=01: ANET=anet:CNT8=01:LN=\"LISTING NAME\":CNT9=001:TEL=\"DIAL#\",LNS=300;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase167() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 167	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test167".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRA:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=num,ED=\"NOW\", SO=so,NOTE=\"BASIC RECORD\",INTERC=interc,"
						+ "INTRAC=intrac:CNT6=01: ANET=anet:CNT8=01:LN=\"LISTING NAME\":CNT9=002:TEL=\"TEL1\",LNS=400:"
						+ "TEL=\"POTS number\",LNS=500,LSO=NPANXX;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase168() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 168	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test168".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=8008001234,ED=ed:IEC=\"CNT1=01,iec1\":SO=so:ANET=\"CNT6=01,US\":CNT8=01"
						+ ":LN=No Listing Name Provided:CNT9=01:TEL=tel,LNS=lns;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase169() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 169	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test169".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=num,ED=\"NOW\":IEC=\"CNT1=01,iec1\":SO=so:ANET=\"CNT6=01,US\""
						+ ":CNT8=01:LN=No Listing Name Provided:CNT9=01:TEL=tel,LNS=lns;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase170() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 170	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test170".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=num,ED=\"NOW\":IEC=\"CNT1=01,iec1\":SO=so:ANET=\"CNT6=01,US\""
						+ ":CNT8=01:LN=No Listing Name Provided:CNT9=01:TEL=tel,LNS=lns;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase171() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 171	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test171".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=num,ED=\"NOW\":IEC=\"CNT1=01,iec1\":SO=so:ANET=\"CNT6=01,US\""
						+ ":CNT8=01:LN=No Listing Name Provided:CNT9=01:TEL=tel,LNS=lns;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase172() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 172	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test172".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=num,ED=\"NOW\":IEC=\"CNT1=01,iec1\":SO=so:ANET=\"CNT6=01,US\""
						+ ":CNT8=01:LN=No Listing Name Provided:CNT9=01:TEL=tel,LNS=lns;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase173() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 173	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test173".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=num,ED=\"NOW\":IEC=\"CNT1=01,iec1\":SO=so:ANET=\"CNT6=01,US\""
						+ ":CNT8=01:LN=No Listing Name Provided:CNT9=01:TEL=tel,LNS=lns;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase174() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 174	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test174".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=T,NUM=num,ED=\"NOW\":IEC=\"CNT1=01,iec1\":SO=so:ANET=\"CNT6=01,US\""
						+ ":CNT8=01:LN=No Listing Name Provided:CNT9=01:TEL=tel,LNS=lns;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase175() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 175	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test175".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=num,ED=\"NOW\":IEC=\"CNT1=01,iec1\":SO=so:ANET=\"CNT6=01,US\""
						+ ":CNT8=01:LN=No Listing Name Provided:CNT9=01:TEL=tel,LNS=lns;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase176() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 176	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test176".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=D,NUM=num,ED=\"NOW\":IEC=\"CNT1=01,iec1\":SO=so:ANET=\"CNT6=01,US\""
						+ ":CNT8=01:LN=No Listing Name Provided:CNT9=01:TEL=tel,LNS=lns;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase177() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 177	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test177".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=D,NUM=num,ED=\"NOW\":IEC=\"CNT1=01,iec1\":SO=so:ANET=\"CNT6=01,US\""
						+ ":CNT8=01:LN=No Listing Name Provided:CNT9=01:TEL=tel,LNS=lns;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase178() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 178	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test178".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=num,ED=\"NOW\":IEC=\"CNT=01,iec1\":SO=so:ANET=\"CNT6=01,US\""
						+ ":CNT8=01:LN=No Listing Name Provided:CNT9=01:TEL=tel,LNS=lns;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase179() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 179	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test179".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=num,ED=\"NOW\":IEC=\"CNT1=01,iec1\":SO=so:ANET=\"CNT6=01,US\""
						+ ":CNT8=01:LN=No Listing Name Provided:CNT9=01:TEL=tel,LNS=lns;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase180() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 180	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test180".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=T,NUM=num,ED=\"NOW\":IEC=\"CNT1=01,iec1\":SO=so:ANET=\"CNT6=01,US\""
						+ ":CNT8=01:LN=No Listing Name Provided:CNT9=01:TEL=tel,LNS=lns;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase181() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 181	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test181".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=D,NUM=num,ED=\"NOW\":IEC=\"CNT1=01,iec1\":SO=so:ANET=\"CNT6=01,US\""
						+ ":CNT8=01:LN=No Listing Name Provided:CNT9=01:TEL=tel,LNS=lns;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase182() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 182	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test182".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=num,ED=\"NOW\",IAC=\"CNT2=01,iac1\":SO=so:ALAT=\"CNT5=01,alat1\":"
						+ "CNT8=01: LN=No Listing Name Provided:CNT9=01:TEL=tel,LNS=lns,LSO=lso;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase183() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 183	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test183".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=num,ED=ed,ET=et:IEC=\"CNT1=01,iec1\":IAC=\"CNT2=01,iac1\":"
						+ "SF=sf: ASTA=\"CNT7=20,asta1, . . .,asta20\":CNT8=01: LN=No Listing Name Provided:"
						+ "CNT9=02:TEL=tel,LNS=lns,CITY=city,LSO=lso,UTS=uts:TEL=tel,LNS=lns,CITY=city,LSO=lso,UTS=uts;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase184() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 184	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test184".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=num,ED=ed,ET=et,CU=cu:"
						+ "IEC=\"CNT1=01,iec1\":IAC=\"CNT2=01,iac1\":ABN=abn,DAU=dau,DAT=dat,DD=dd,"
						+ "HDD=hdd,LI=li,RAO=rao,SF=sf,SO=so,NEWRO=newro,NOTE=note,AGENT=agent,"
						+ "CUS=cus,LA=la,CBI=cbi,TELCO=telco,NCON=ncon,CTEL=ctel: ALBL=\"CNT3=16,albl1, . . . ,albl16\""
						+ ":AAC=\"CNT4=34,aac1, . . . , aac34\": ALAT=\"CNT5=34,alat1, . . . ,alat34\":"
						+ "ANET=\"CNT6=08,anet1,. . .anet8\": ASTA=\"CNT7=46,asta1, . . . ,asta46\":CNT8=09: LN=No Listing Name Provided"
						+ ":LN=No Listing Name Provided: LN=No Listing Name Provided:LN=No Listing Name Provided"
						+ ": LN=No Listing Name Provided:LN=No Listing Name Provided: LN=No Listing Name Provided:"
						+ "LN=No Listing Name Provided: LN=No Listing Name Provided:CNT9=2:TEL=tel,LNS=lns,CITY=city,"
						+ "FSO=fso,HML=hml,LSIS=lsis,LSO=lso,SFG=sfg,STN=stn,UTS=uts:TEL=tel,LNS=lns,CITY=city,FSO=fso,"
						+ "HML=hml,LSIS=lsis, LSO=lso,SFG=sfg,STN=stn,UTS=uts;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase185() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 185	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test185".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=8008001234,SO=so;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase186() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 186	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test186".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=8008001234,ED=ed,ET=et, ASTA=\"CNT7=06,asta1, . . . ,asta6\":ANET=\"CNT6=0\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase187() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 187	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test187".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=8008000187,ED=ed,ET=et,SEFD=sefd, SO=so;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase188() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 188	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test188".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=8008001234,ED=\"NOW\",ET=et,NEWRO=newro,SEFD=sefd;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase189() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 189	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test189".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=8008001234,ED=\"NOW\",ET=et,NEWRO=newro,SEFD=sefd, INTERC=interc;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase190() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 190	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test190".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=8008001234,ED=\"NOW\",ET=et,SEFD=sefd, INTERC=interc;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase191() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 191	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test191".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=8008001234,ED=\"NOW\",INTRAC=intrac,"
						+ "SO=so,CNT4=01:AAC=405:CNT8=01:LN=No Listing Name provided:CNT09=01:TEL=tel,LNS=lns,LSO=lso;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase192() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 192	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test192".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=8008001234,ED=\"NOW\",INTRAC=intrac,"
						+ "NEWRO=newro,SO=so,CNT4=01:AAC=405:CNT8=01:LN=No Listing Name:CNT09=01:TEL=tel,LNS=lns,LSO=lso;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase193() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 193	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test193".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=8008001234,ED=\"NOW\",INTRAC=intrac,"
						+ "NEWRO=newro,SO=so,CNT6=01:ANET=US:CNT8=01: LN=No Listing Name provided:CNT09=01:TEL=tel,LNS=lns,CITY=city,LSO=lso;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase194() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 194	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test194".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=D,NUM=8008000187,ED=ed,ET=et, SEFD=sefd,REFER=N;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase195() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 195	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test195".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=D,NUM=8008000187,ED=ed,ET=et, SEFD=sefd,EINT=eint;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase196() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 196	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test196".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=D,NUM=8008000187,SEFD=sefd,REFER=Y;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase197() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 197	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test197".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=D,NUM=8008000187,ED=\"NOW\", REFER=N,EINT=today's_date;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase198() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 198	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test198".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=D,NUM=8008000187,ED=ed,ET=et, SEFD=sefd,REFER=N;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase199() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 199	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test199".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=8008000187,ED=ed,ET=et, SEFD=sefd;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase200() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 200	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test200".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=T,NUM=8008000187,ED=ed,ET=et;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase201() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 201	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test201".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=T,NUM=8008000187,ED=ed,ET=et,SEFD=sefd, SO=so;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase202() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 202	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test202".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=T,NUM=8008000187,ED=\"NOW\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase203() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 203	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test203".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=T,NUM=8008000187,ED=ed,ET=et,SEFD=sefd:ASTA=\"CNT7=07,asta1, . . . , asta7\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase204() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 204	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test204".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=T,NUM=8008000187,ED=ed,ET=et,SEFD=sefd:ASTA=\"CNT7=07,asta1, . . . , asta7\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase205() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 205	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test205".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=T,NUM=8008000187,ED=\"NOW\",SEFD=sefd,REFER=Y;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-NSR");
	}

	public void TestCase206() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 206	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test206".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=R,NUM=8008000187;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase207() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 207	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test207".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=R,NUM=8008000187,ED=ed,ET=et;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase208() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 208	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test208".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=X,NUM=num,ED=ed,ET=et;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase209() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 209	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test209".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=X,NUM=num,ED=ed,ET=et;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase210() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 210	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test210".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=X,NUM=num,ED=ed,ET=et,DCSN=Y;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase211() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 211	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test211".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=X,NUM=num,ED=ed,ET=et,DCSN=N;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase212() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 212	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test212".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=D,NUM=num,ED=\"future date but before the EINT date\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase213() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 213	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test213".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=num,ED=\"NOW\":IAC=\"CNT2=01,iac1\":IAC=\"CNT2=1,iac2\":SO=so,UNREC=unrec_data:"
						+ "AAC=\"CNT4=01,aac1\":CNT8=01:LN=decimal_data:CNT9=01:TEL=tel,LNS=lns, LSO=lso;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase214() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 214	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test214".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=num,ED=\"NOW:"
						+ "IAC=\"CNT2=1,iac1\":IEC=\"CNT1=3,iec1,iec2\":SO=n1234567890123:"
						+ "AAC=\"CNT6=01,aacn\":CNT8=01:LN=No Listing Name Provided: CNT9=01:TEL=tel,LNS=lns,LSO=lso",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "123456789" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase215() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 215	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test215".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=num,ED=\"NOW\":"
						+ "IEC=\"CNT1=1,iec1\":SO=so:AAC=\"CNT4=01,aac1\":CNT8=01: LN=No Listing Name Provided:"
						+ "CNT9=01:TEL=tel,LNS=lns,LSO=lso;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase216() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 216	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test216".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=num,ED=\"NOW\":"
						+ "IEC=\"CNT1=1,iec1\":SO=so:AAC=\"CNT4=01,aac1\":CNT8=01: LN=No Listing Name Provided:CNT9=01:TEL=tel,LNS=lns,LSO=lso;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase217() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 217	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test217".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=num,ED=\"NOW\":"
						+ "IEC=\"CNT1=1,iec1\":SO=so:AAC=\"CNT4=01,aac1\":CNT8=01: LN=No Listing Name Provided:CNT9=01:TEL=tel,LNS=lns,LSO=lso;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase218() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 218	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test218".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = "REQ-CRC:,1992-05-07,10-10-22-CST:::::ID=id,RO=ro,AC=W, NUM=\"888123456Q\",ED=\"11=01/96\";";
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase219() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 219	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test219".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=num,SEFD=sefd,LI=XX,HDD=X,SF=\"()*& $\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase220() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 220	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test220".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=num,ED=\"NOW\":"
						+ "IAC=\"CNT2=01,iac1\":SO=so:AAC=\"CNT4=01,405\": CNT9=01:TEL=tel,LNS=lns,LSO=lso;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase221() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 221	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test221".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=num,ED=ed:IEC=\"CNT1=01,"
						+ "iec1\":IAC=\"CNT2=01,iac1\": SO=N12345,RAO=rao,ABN=abn:"
						+ "AAC=\"CNT4=03,908,201,609\":CNT8=01:LN=No Listing Name Provided:CNT9=04:"
						+ "TEL=\"9085727000\",LNS=0001,LSO=908572:TEL=\"9085727001\",LNS=0001,"
						+ "LSO=908572:TEL=\"9085727002\",LNS=0001,LSO=908572:"
						+ "NODE=\"CNT10=05,AC,SD,DA,TI,TE\":CNT11=05:V=\"201+908,201278+908699,M-F,09:00A-05:00P,9085727000\""
						+ ": V=\"201+908,201278+908699,M-F,OTHER,9085727001\":"
						+ "V=\"201+908,201278+908699,SA-SU,,9085727002\":"
						+ "V=\"201+908,OTHER,,,9085727003\":V=\"609,,,,9085727000\":PEC=pec,PAC=pac;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase222() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 222	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test222".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=num,ED=ed:IEC=\"CNT1=01,iec1\":IAC=\"CNT2=01,iac1\""
						+ ":SO=N12345,RAO=rao,ABN=abn:ASTA=\"CNT7=03,NJ,PA,NY\":CNT8=01:LN=No Listing Name Provided:"
						+ "CNT9=04:TEL=\"2012781000\",LNS=0001,LSO=201278: TEL=\"2012791000\",LNS=0001,LSO=201279:"
						+ "TEL=\"6092261000\",LNS=0001,LSO=609226:TEL=\"6094522222\",LNS=0001,LSO=609452:"
						+ "NODE=\"CNT10=05,LT,AC,NX,TE,AN\":CNT11=06:V=\"224,201,278+279+523,2012781000,\":"
						+ "V=\"224,201,OTHER,2012781000,\":V=\"224,OTHER,,,OBA\":V=\"222,609,452+228,6094522222,\":"
						+ "V=\"222,609,OTHER,6092261000,\":V=\"OTHER,,,,OBA\":PEC=pec,PAC=pac;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase223() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 223	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test223".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=88888888,ED=\"NOW\":"
						+ "IEC=\"CNT1=02,iec1,iec2\":IAC=\"CNT2=01,iac1\":SO=so,RAO=rao,ABN=abn:"
						+ "ASTA=\"CNT7=03,NJ,NY,PA\":CNT8=01:LN=No Listing Name Provided:"
						+ "CNT9=04:TEL=\"7035731000\",LNS=0001,LSO=703573: TEL=\"7035731001\","
						+ "LNS=0001,LSO=703573:TEL=\"7035731002\",LNS=0001,LSO=703573:TEL=num,LNS=0001:"
						+ "NODE=\"CNT10=06,ST,SW,PC,DT,CA,TE\":CNT11=07:V=\"NJ,,,,iec1,7035731000\":"
						+ "V=\"NY,ON1,25,,iec2,7035731000\":V=\"NY,ON1,25,,iec1,7035731000\":"
						+ "V=\"NY,ON1,50A,10/01-12/31,iec2,7035731002\":"
						+ "V=\"NY,ON1,50A,01/01-09/30,iec1,7035731002\":V=\"NY,OFF,,,iec2,num\":"
						+ "V=\"PA,,,,iec2,7035731002\": PEC=pec,PAC=pac;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase224() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 224	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test224".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=88888888,ED=ed:"
						+ "IEC=\"CNT1=01,iec1\":IAC=\"CNT2=01,iac1\":SO=so,RAO=rao,ABN=abn:"
						+ "AAC=\"CNT4=03,908,609,201\":CNT8=01:LN=\"ln\":CNT9=01: TEL=tel,LNS=lns,LSO=lso:CNT12=09:"
						+ "TYPE=AC,LBL=\"*NJNPAS\",DEF=\"CNT13=003,908,609,201\" DEF=\"CNT13=001,05/31-09/01\":"
						+ "TYPE=LT,LBL=\"*NJLATA\",DEF=\"CNT13=004,224,220,222,232\":TYPE=NX,LBL=\"*NXX908\","
						+ "DEF=\"CNT13=004,908,699,572,755\":TYPE=ST,LBL=\"*TRISTAT\",DEF=\"CNT13=003,NJ,PA,NY\":"
						+ "TYPE=TI,LBL=\"*BUSHRS\",DEF=\"CNT13=001,09:00A-05:00P\":TYPE=TD,LBL=\"*TENNJ\","
						+ "DEF=\"CNT13=001,9086991000\":TYPE=SD,LBL=\"*SIX908\",DEF=\"CNT13=003,908699,908572,908755\":"
						+ "TYPE=TE,LBL=\"*TELLAD1\",DEF=\"CNT13=001,9086998604\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase225() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 225	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test225".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=88888888,ED=ed:SO=so,RAO=rao,"
						+ "ABN=abn:AAC=\"CNT4=03,908,609,201\":CNT8=01:LN=\"DML\":CNT9=06:"
						+ "TEL=\"6092261000\",LNS=0001,LSO=609266: TEL=\"2012783000\",LNS=0001,LSO=201278:"
						+ "TEL=\"2012791000\",LNS=0001,LSO=201279: TEL=\"6092263000\",LNS=0001,LSO=609226:"
						+ "TEL=\"9085721000\",LNS=0001,LSO=908572:TEL=\"9086993000\",LNS=0001,LSO=908699:"
						+ "NODE=\"CNT10=03,AC,SD,TE\":CNT11=06:V=\"201,*SIX201,2012783000\":V=\"201,OTHER,2012791000\":"
						+ "V=\"609,*SIX609,6092261000\":V=\"609,OTHER,6092263000\":V=\"908,*SIX908,9085721000\":"
						+ "V=\"908,OTHER,9086993000\":PEC=pec,PAC=pac:"
						+ "CNT12=03:TYPE=SD,LBL=\"*SIX201\",DEF=\"CNT13=003,201224,201226,201228\":"
						+ "TYPE=SD,LBL=\"*SIX609\",DEF=\"CNT13=006,609226,609365,609228,609452,609368,609232\":"
						+ "TYPE=SD,LBL=\"*SIX908\",DEF=\"CNT13=003,908819,908932,908846\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase226() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 226	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test226".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=88888888,ED=ed:IEC=\"CNT1=01,iec1\":IAC=\"CNT2=01,iac1\":SO=so,RAO=rao,ABN=abn:"
						+ "AAC=\"CNT4=03,908,609,201\":CNT8=01:LN=\"DML\":CNT9=06:"
						+ "TEL=\"6092261000\",LNS=0001,LSO=609266:TEL=\"2012783000\",LNS=0001,LSO=201278:"
						+ "TEL=\"2012791000\",LNS=0001,LSO=201279:TEL=\"6092263000\",LNS=0001,LSO=609226:"
						+ "TEL=\"9085721000\",LNS=0001,LSO=908572:TEL=\"9086993000\",LNS=0001,LSO=908699:"
						+ "NODE=\"CNT10=03,AC,SD,TE\":CNT11=06:"
						+ "V=\"201,*SIX201,2012783000\":V=\"201,OTHER,*TEL201\":V=\"609,*SIX609,6092261000\":V=\"609,OTHER,*TEL609\":"
						+ "V=\"908,*SIX908,9085721000\":V=\"908,OTHER,*TEL908\": PEC=pec,PAC=pac:CNT12=06:"
						+ "TYPE=SD,LBL=\"*SIX201\",DEF=\"CNT13=003,201224,201226,201228\":TYPE=SD,LBL=\"*SIX609\",DEF=\"CNT13=006,609226,609365,609228,609452,609368,609232:"
						+ "TYPE=SD,LBL=\"*SIX908\",DEF=\"CNT13=003,908819,908932,908846\":TYPE=TE,LBL=\"*TEL908\",DEF=\"CNT13=001,9086993000\":"
						+ "TYPE=TE,LBL=\"*TEL609\",DEF=\"CNT13=001,6092263000\":TYPE=TE,LBL=\"*TEL201\",DEF=\"CNT13=001,2012791000\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase227() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 227	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test227".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=88888888,ED=ed:IEC=\"CNT1=01,iec1\":IAC=\"CNT2=01,iac1\":SO=so,RAO=rao,ABN=abn:"
						+ "AAC=\"CNT4=03,908,609,201\":CNT8=01:LN=ln:CNT9=01: TEL=tel,LNS=lns,LSO=lso:CNT12=100",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		for (int i = 1; i <= 100; i++) {
			dataAppl += ":TYPE=AC,LBL=\"*NJNPAS" + String.valueOf(i) + "\",DEF=\"CNT13=003,908,609,201\"";
		}
		dataAppl += ";";
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase228() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 228	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test228".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=88888888,ED=ed:IEC=\"CNT1=01,iec1\":IAC=\"CNT2=01,iac1\":SO=so,RAO=rao,ABN=abn:"
						+ "AAC=\"CNT4=03,908,609,201\":CNT8=01:LN=ln:CNT9=01: TEL=tel,LNS=lns,LSO=lso:CNT12=990",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		for (int i = 1; i <= 990; i++) {
			dataAppl += ":TYPE=AC,LBL=\"*NJNPAS" + String.valueOf(i) + "\",DEF=\"CNT13=003,908,609,201\"";
		}
		dataAppl += ";";
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase229() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 229	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test229".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=88888888,ED=ed:IEC=\"CNT1=01,iec1\":IAC=\"CNT2=01,iac1\":SO=so,RAO=rao,ABN=abn:"
						+ "AAC=\"CNT4=03,908,609,201\":CNT8=01:LN=ln:CNT9=01: TEL=tel,LNS=lns,LSO=lso:CNT12=999",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		for (int i = 1; i <= 999; i++) {
			dataAppl += ":TYPE=AC,LBL=\"*NJNPAS" + String.valueOf(i) + "\",DEF=\"CNT13=003,908,609,201\"";
		}
		dataAppl += ";";
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase230() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 230	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test230".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=88888888,ED=ed:IEC=\"CNT1=01,iec1\":IAC=\"CNT2=01,iac1\":SO=so,RAO=rao,ABN=abn:"
						+ "AAC=\"CNT4=03,908,609,201\":CNT8=01:LN=ln:CNT9=01: TEL=tel,LNS=lns,LSO=lso:CNT12=1000",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		for (int i = 1; i <= 1000; i++) {
			dataAppl += ":TYPE=AC,LBL=\"*NJNPAS" + String.valueOf(i) + "\",DEF=\"CNT13=003,908,609,201\"";
		}
		dataAppl += ";";
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase231() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 231	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test231".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=88888888:CNT12=03:"
						+ "TYPE=SD,LBL=\"*SIX201\",DEF=\"CNT13=009,201224,201226,201228,201316,201503,201318,201263,201265,201266\":"
						+ "TYPE=SD,LBL=\"*SIX609\",DEF=\"CNT13=006,60922,609365,609228,609452,609368,609232\":"
						+ "TYPE=SD,LBL=\"*SIX908\",DEF=\"CNT13=003,908819,908932,908846\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase232() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 232	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test232".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=88888888:CNT12=09"
						+ ":TYPE=AC,LBL=\"*NJNPAS\",DEF=\"CNT13=003,908,609,201\""
						+ ":TYPE=LT,LBL=\"*NJLATA\",DEF=\"CNT13=004,224,220,222,2 32\""
						+ ":TYPE=NX,LBL=\"*NXX908\",DEF=\"CNT13=004,908,699,572,7 55\""
						+ ":TYPE=ST,LBL=\"*TRISTAT\",DEF=\"CNT13=003,NJ,PA,NY\""
						+ ":TYPE=TI,LBL=\"*BUSHRS\",DEF=\"CNT13=001,09:00A-05:00P\""
						+ ":TYPE=TD,LBL=\"*TENNJ\",DEF=\"CNT13=001,9086991000\""
						+ ":TYPE=SD,LBL=\"*SIX908\",DEF=\"CNT13=003,908699,908572,908755\""
						+ ":TYPE=TE,LBL=\"*TELLAD1\",DEF=\"CNT13=001,num\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase233() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 233	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test233".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=88888888:CNT12=999",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		for (int i = 1; i <= 999; i++) {
			dataAppl += ":TYPE=AC,LBL=\"*NJNPAS" + String.valueOf(i) + "\",DEF=\"CNT13=003,908,609,201\"";
		}
		dataAppl += ";";
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase234() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 234	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test234".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=88888888:CNT12=1000",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		for (int i = 1; i <= 1000; i++) {
			dataAppl += ":TYPE=AC,LBL=\"*NJNPAS" + String.valueOf(i) + "\",DEF=\"CNT13=003,908,609,201\"";
		}
		dataAppl += ";";
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase235() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 235	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test235".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=88888888:CNT12=100",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		for (int i = 1; i <= 100; i++) {
			dataAppl += ":TYPE=AC,LBL=\"*NJNPAS" + String.valueOf(i) + "\",DEF=\"CNT13=003,908,609,201\"";
		}
		dataAppl += ";";
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase236() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 236	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test236".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=88888888,ED=ed, ET=et:IEC=\"CNT1=01,iec\":"
						+ "IAC=\"CNT1=01,iac1\":SO=so, RAO=rao,ABN=abn:AAC=\"CNT4=03,908,201,609\":CNT8=01:"
						+ "LN=\"name\":CNT9=06:TEL=\"num\",LNS=001,LSO=lso:TEL=\"num\",LNS=001,LSO=lso:"
						+ "TEL=\"num\",LNS=001,LSO=lso:TEL=\"num\",LNS=001,LSO=lso:NODE=\"CNT10=03,AC,SD,TE\":"
						+ "CNT11=04:V=\"201,*SIX201,number\":V=\"201,OTHER,*TEN800\":V=\"908,*SIX908,number:"
						+ "V=\"908,OTHER,*TEN908\":PEC=pec,PAC=pac:CNT12=04:TYPE=SD,SORT=N,LBL=\"*SIX201\","
						+ "DEF=\"CNT13=003,201278,201279,201798\":TYPE=SD,SORT=Y,LBL=\"*SIX908\",DEF=\"CNT13=003,908278,908279,908798\":"
						+ "TYPE=TE,LBL=\"*TEN800\",DEF=\"CNT13=001,num1\":TYPE=TE,LBL=\"*TEN908\",DEF=\"CNT13=001,9086998434\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase237() {
		// I think input data is wrong, don't make it
	}

	public void TestCase238() {
		// I think input data is wrong, don't make it
	}

	public void TestCase239() {
		// I think input data is wrong, don't make it
	}

	public void TestCase240() {
		// I think input data is wrong, don't make it
	}

	public void TestCase241() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 241	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test241".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=88888888,ED=ed,ET=et:"
						+ "IEC=\"CNT1=01,iec\":IAC=\"CNT1=01,iac1\":SO=so, RAO=rao,ABN=abn:"
						+ "AAC=\"CNT4=03,908,201,609\":CNT8=01:LN=\"name\":CNT9=06:TEL=\"num\",LNS=001,LSO=lso:"
						+ "TEL=\"num\",LNS=001,LSO=lso:TEL=\"num\",LNS=001,LSO=lso:TEL=\"num\",LNS=001,LSO=lso:"
						+ "NODE=\"CNT10=03,AC,SD,TE\":CNT11=04:V=\"201,*SIX201,number\":V=\"201,OTHER,*TEN800\":"
						+ "V=\"908,*SIX908,number:V=\"908,OTHER,*TEN908\":PEC=pec,PAC=pac:CNT12=04:"
						+ "TYPE=SD,SORT=Y,LBL=\"*SIX201\",DEF=\"CNT13=003,201798,201279,201278\":"
						+ "TYPE=SD,SORT=Y,LBL=\"*SIX908\",DEF=\"CNT13=003,908278,908798,908279\":"
						+ "TYPE=TE,LBL=\"*TEN800\" DEF=\"CNT13=001,num1\":TYPE=TE,LBL=\"*TEN908\",DEF=\"CNT13=001,9086998434\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase242() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 242	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test242".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ATX01,AC=N,NUM=88888888,ED=\"NOW\":IEC=\"CNT1=02,0288,0222\":"
						+ "IAC=\"CNT2=02,0288,0222\":SO=B2323441,ABN=\"9086990000\",DAU=N,NOTE=\"COMPLEX REC\",DAT=N,HDD=N,LI=BL,RAO=000,"
						+ "LA=\"LISTING ADDR\",CBI=SPR:CNT8=01:LN=\"TELCORDIA\":ANET=\"CNT6=1,CN:CNT9=01:TEL=\"dial#\",LNS=800:"
						+ "NODE=\"CNT10=3,TD,CA,TE\":CNT11=03:V=\"*TEND1,0288,dial#\":V=\"*TEND2,0222,dial#\":"
						+ "V=\"OTHER,0288,dial#\":CNT12=2:TYPE=TD,SORT=Y,LBL=\"*TEND1\",DEF=\"CNT13=42,4165550001,4165550002,4165550003,4165550004,4165550005,"
						+ "4165550012,4165550013,4165550014, 4165550015,4165550016,4165550017,4165550006,4165550007,4165550008, 4165550018,4165550019,4165550020,"
						+ "4165550021,4165550022,4165550023, 4165550009,4165550010,4165550011,4165550036,4165550037,4165550038, 4165550039,4165550030,4165550031,"
						+ "4165550032,4165550033,4165550034, 4165550035,4165550040,4165550041,4165550024,4165550025,4165550026, 4165550027,4165550028,4165550029,"
						+ "4165550042\":TYPE=TD,SORT=Y,LBL=\"*TEND2\",DEF=\"CNT13=05,4162320005,4162320002,4162320003,4162320001,4162320004\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase243() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 243	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test243".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ATX01,AC=N,NUM=88888888,ED=\"NOW\":IEC=\"CNT1=02,0288,0222\":IAC=\"CNT2=02,0110,0222\":"
						+ "SO=B2323441,ABN=\"9086990000\",LI=BL,RAO=000:CNT8=01:LN=\"TELCORDIA\":"
						+ "ASTA=\"CNT7=02,NJ,NY\":CNT9=05:TEL=\"dial#\",LNS=800:TEL=\"pots1\","
						+ "LNS=800,LSO=lso:TEL=\"pots2\",LNS=800,LSO=lso:TEL=\"pots3\",LNS=800,LSO=lso:"
						+ "TEL=\"pots4\",LNS=800,LSO=lso:NODE=\"CNT10=09,LT,AC,NX,SW,TD,DT,TI,CA,TE\":CNT11=009:"
						+ "V=\"224,*ACNY,*NXXNJ,ON1,*DIGIT10,*DATE1,*TIME1,0222,pots2,\":"
						+ "V=\"224,*ACNY,*NXXNJ,ON1,*DIGIT10,*DATE1,OTHER,0288,dial#\":"
						+ "V=\"224,*ACNY,*NXXNJ,ON1,*DIGIT10,OTHER,,,pots2\":"
						+ "V=\"224,*ACNY,*NXXNJ,ON1,OTHER,,,,pots3\":V=\"224,*ACNY,OTHER,ON1,,,,,锟斤拷,pots1\":"
						+ "V=\"224,201,280,,,,,0222,dial#\":V=\"224,201,OTHER,,,,,,pots2\":V=\"224,OTHER,,ON1,,,,,pots1\":"
						+ "V=\"OTHER,,,,,,,0222,pots3\":PEC=\"0288\",PAC=\"0222\":CNT12=05:TYPE=AC,SORT=Y,LBL=\"*ACNY\","
						+ "DEF=\"CNT13=008,315,212,607,518,516,914,718,716\":"
						+ "TYPE=NX,SORT=Y,LBL=\"*NXXNJ\",DEF=\"CNT13=4,908,699,232,233\":"
						+ "TYPE=DT,SORT=Y,LBL=\"*DATE1\",DEF=\"CNT13=1,01/01-01/31\":"
						+ "TYPE=TI,SORT=Y,LBL=\"*TIME1\",DEF=\"CNT13=1,08:00A-09:00A\":"
						+ "TYPE=TD,SORT=Y,LBL=\"*DIGIT10\",DEF=\"CNT13=3,W0TNXXAAAF,W0TNXXAAAC,W0TNXXAAAA\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase244() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 244	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test244".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ATX01,AC=N,NUM=88888888,ED=\"NOW\":IEC=\"CNT1=02,0288,0222\":"
						+ "IAC=\"CNT2=02,0110,0222\":SO=B2323441,ABN=\"9086990000\",DAU=N,NOTE=\"COMPLEX REC\", ANET=\"CNT6=1,XA\":"
						+ "CNT8=01:LN=\"UPDATE\":CNT9=002:TEL=\"dial#\",LNS=2334:TEL=\"5032700004\",LNS=0004,LSO=503270:"
						+ "NODE=\"CNT10=002,NX,TE\":CNT11=002:V=\"*LADNXX1,5032700004\":V=\"OTHER,dial#\":"
						+ "PEC=\"0288\",PAC=\"0110\":CNT12=01:TYPE=NX,SORT=Y,LBL=\"*LADNXX1\",DEF=\"CNT13=005,503,344,347,980,981\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase245() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 245	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test245".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ATX01,AC=C,NUM=88888888,SEFD=sefd,ED=ed,ET=et:"
						+ "NODE=\"CNT10=03,AC,SD,TE\":CNT11=05:V=\"201,,2012783000\":V=\"609,*SIX609,6092261000\":"
						+ "V=\"609,OTHER,6092263000\":V=\"908,*SIX908,9085721000\":V=\"908,OTHER,9086993000\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase246() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 246	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test246".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=88888888,SEFD=sefd,ED=ed,ET=et:"
						+ "NODE=\"CNT10=0\":CNT12=0:CNT9=1:TEL=tel,LSO=lso,LNS=1;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase247() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 247	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test247".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=88888888,ED=ed,ET=et,"
						+ "RAO=ao,ABN=abn:IEC=\"CNT1=01,iec1\":IAC=\"CNT2=01,iac1\":"
						+ "AAC=\"CNT4=03,908,609,201\":CNT9=06:TEL=\"6092261000\",LNS=0001,LSO=201266:"
						+ "TEL=\"2012783000\",LNS=0001,LSO=201278:TEL=\"20127910000\",LNS=0001,LSO=201279:"
						+ "TEL=\"60922630000\",LNS=0001,LSO=609226: TEL=\"90857210000\",LNS=0001,LSO=908572:"
						+ "TEL=\"9086993000\",LNS=0001,LSO=908699:NODE=\"CNT10=03,AC,SD,TE\":CNT11=07:"
						+ "V=\"201,*SIX201,2012783000\":V=\"201,OTHER,*TEL201\":"
						+ "V=\"609,*SIX609,6092261000\":V=\"609,OTHER,*TEL609\":"
						+ "V=\"908,*SIX908,9085721000\":V=\"908,OTHER,9086993000\":V=\"OTHER,,*TEL908\":"
						+ "PEC=pec,PAC=pac:CNT12=06:TYPE=SD,LBL=\"*SIX201\",DEF=\"CNT13=003,201224,201226,201228\""
						+ ":TYPE=SD,LBL=\"*SIX609\",DEF=\"CNT13=006,609226,609365, 609228,609452,609368,609232\""
						+ ":TYPE=SD,LBL=\"*SIX908\",DEF=\"CNT13=003,908819,908932,908846\""
						+ ":TYPE=TE,LBL=\"*TEL908\",DEF=\"CNT13=001,9086993000\""
						+ ":TYPE=TE,LBL=\"*TEL609\",DEF=\"CNT13=001,6092263000\""
						+ ":TYPE=TE,LBL=\"*TEL201\",DEF=\"CNT13=001,2012791000\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase248() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 248	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test248".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=D,NUM=num,SEFD=sefd,REFER=Y;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase249() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 249	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test249".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=D,NUM=num,SEFD=sefd,REFER=Y;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase250() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 250	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test250".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=D,NUM=num,SEFD=sefd,REFER=Y;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase251() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 251	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test251".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=D,NUM=num,ED=ed,REFER=N,EINT=eint:"
						+ "CNT9=0:NODE=\"CNT10=0\":CNT12=0;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase252() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 252	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test252".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=T,NUM=num,ED=ed,ET=et,SEFD=sefd:"
						+ "NODE=\"CNT10=003,AC,SD,TE\":CNT11=05:V=\"201,,2012783000\":"
						+ "V=\"609,609365+609232,6092261000\":V=\"609,OTHER,6092263000\":"
						+ "V=\"908,908699,9085721000\":V=\"908,OTHER,9086993000\":CNT12=0;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase253() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 253	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test253".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=T,NUM=num,ED=ed,ET=et;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase254() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 254	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test254".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		int iCnt12 = 1000;
		String dataAppl = String.format("REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=T,NUM=num,ED=ed,ET=et,CNT12=%d",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900",
						iCnt12 });
		for (int i = 1; i <= iCnt12; i++) {
			dataAppl += ":TYPE=AC,LBL=\"*NJNPAS" + String.valueOf(i) + "\",DEF=\"CNT13=003,908,609,201\"";
		}
		dataAppl += ";";
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase255() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 255	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test255".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=T,NUM=num,ED=ed,ET=et,SEFD=sefd:"
						+ "NODE=\"CNT10=003,AC,SD,TE\":CNT11=05:V=\"201,,2012783000\":"
						+ "V=\"609,609365+609232,6092261000\":V=\"609,OTHER,6092263000\":"
						+ "V=\"908,908699,9085721000\":V=\"908,OTHER,9086993000\":CNT12=0;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase256() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 256	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test256".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=T,NUM=num,ED=\"NOW\",SEFD=sefd:"
						+ "CNT9=1:TEL=tel,LSO=lso,LNS=lns:NODE=\"CNT10=0\":CNT12=0;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase257() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 257	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test257".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=R,NUM=num;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase258() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 258	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test258".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=R,NUM=num;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase259() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 259	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test259".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=R,NUM=num;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase260() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 260	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test260".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=X,NUM=num,ED=ed,ET=et;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase261() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 261	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test261".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=X,NUM=num,ED=ed,ET=et;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase262() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 262	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test262".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=num,ED=\"NOW\":"
						+ "IEC=\"CNT1=1,iec1\":IAC=\"CNT2=0\":ABN=\"abn\",DAU=dau,DAT=dat,HDD=hdd,LI=li,RAO=rao,SO=so,SF=\"\","
						+ "NOTE=\"\",AGENT=\"\",TELCO=\"rao1\",CUS=\"\",LA=\"\",CBI=,NCON=\"ncon\",CTEL=ctel:"
						+ "ALBL=\"CNT3=0\":AAC=\"CNT4=0\":ALAT=\"CNT5=0\":ANET=\"CNT6=1,anet\":ASTA=\"CNT7=0\":CNT8=1:LN=\"ln\",:CNT9=1:"
						+ "TEL=\"tel\",LNS=lns1:NODE=\"CNT10=0\":CNT11=0:CNT12=0;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	// need to make server handle2
	public void TestCase263() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 263	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test263".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=num,ED=ed,RAO=rao,ABN=abn:"
						+ "IEC=\"CNT1=01,iec1\":IAC=\"CNT2=01,iac1\":SO=so:AAC=\"CNT4=03,908,699,201\":"
						+ "CNT8=01:LN=\"DML\":CNT9=03:TEL=\"2012783000\",LNS=0001,LSO=201278:"
						+ "TEL=\"2012791000\",LNS=0001,LSO=201279: TEL=\"9086993000\",LNS=0001,LSO=908699:CNT11=06:"
						+ "V=\"201,*SIX201,2012783000\":V=\"201,OTHER,2012791000\":"
						+ "V=\"OTHER,,9086993000\":PEC=pec,PAC=pac:CNT12=01:"
						+ "LBL=\"*SIX201\",DEF=\"CNT12=06,201224,201226,201228,201263,201265,201266\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase264() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 264	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test264".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=num,ED=ed,RAO=rao,ABN=abn:"
						+ "IEC=\"CNT1=01,iec1\":IAC=\"CNT2=01,iac1\":SO=so:AAC=\"CNT4=03,908,699,201\":"
						+ "CNT8=01:LN=\"DML\":CNT9=03:TEL=\"2012783000\",LNS=0001,LSO=201278:"
						+ "TEL=\"2012791000\",LNS=0001,LSO=201279:TEL=\"9086993000\",LNS=0001,LSO=908699:NODE=\"CNT10=003,AC,ZZ,TE\":"
						+ "CNT11=03:V=\"201,*SIX201,2012783000\":V=\"201,OTHER,2012791000\":V=\"OTHER,,9086993000\":PEC=pec,PAC=pac:"
						+ "CNT12=01:TYPE=XX,LBL=\"SIX201\",DEF=\"CNT13=006,201224,201226,201228,201263,201265,201266\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase265() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 265	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test265".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=num:"
						+ "NODE=\"CNT10=04,ST,AC,SD,TE\":CNT11=03:V=\",201,*SIX201,2012783000\":"
						+ "V=\",201,OTHER,2012791000\":V=\",OTHER,,2012791000\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase266() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 266	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test266".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=num:"
						+ "NODE=\"CNT10=03,AC,SD,TE\":CNT11=07:V=\"201,*SIX201,2012783000\":"
						+ "V=\"201,OTHER,2012791000\":V=\"609,*SIX609,6092261000\":"
						+ "V=\"609,OTHER,6092263000\":V=\"908,*SIX908,9085721000\":"
						+ "V=\"908,*SIX908,9086993000\":V=\"908,OTHER,9086993000\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase267() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 267	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test267".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=num,ED=ed:"
						+ "NODE=\"CNT10=03,AC,SD,TE\":CNT11=05:V=\"201,*SIX201,2012783000\":"
						+ "V=\"201,OTHER,2012791000\":V=\"609,*SIX609,6092261000\":"
						+ "V=\"609,OTHER,6092263000\":V=\"908,*SIX908,9085721000\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase268() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 268	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test268".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=num:NODE=\"CNT10=02,AC,SD\":"
						+ "CNT11=06:V=\"201,*SIX201\":V=\"201,OTHER\":V=\"609,*SIX609\":V=\"609,OTHER\":"
						+ "V=\"908,*SIX908\":V=\"908,OTHER\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase269() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 269	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test269".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=num:CNT12=03:"
						+ "TYPE=SD,LBL=\"*SIX201\",DEF=\"CNT13=004,201224,201224,201226,201228\":"
						+ "TYPE=SD,LBL=\"*SIX609\",DEF=\"CNT13=006,609226,609365,609228,609452,609368,609232\":"
						+ "TYPE=SD,LBL=\"*SIX908\",DEF=\"CNT13=003,908819,908932,908846\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase270() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 270	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test270".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=num:CNT12=06:"
						+ "TYPE=SD,LBL=\"*SIX201\",DEF=\"CNT13=003,201224,201226,201228\":"
						+ "TYPE=SD,LBL=\"*SIX609\",DEF=\"CNT13=006,609226,609365,609228,609452,609368,609232\":"
						+ "TYPE=SD,LBL=\"*SIX908\",DEF=\"CNT13=003,908819,908932,908846\":"
						+ "TYPE=TE,LBL=\"*TEL908\",DEF=\"CNT13=001,9086993000\":"
						+ "TYPE=TE,LBL=\"*TEL609\",DEF=\"CNT13=001,6092263000\":"
						+ "TYPE=TE,LBL=\"*TEL201\",DEF=\"CNT13=001,2791000\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase271() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 271	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test271".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=num,ED=ed:"
						+ "NODE=\"CNT10=03,AC,SD,TE\":CNT11=06:V=\"201,201699,2012783000\":"
						+ "V=\"201,OTHER,2012791000\":V=\"609,609224,6092261000\":"
						+ "V=\"609,OTHER,6092263000\":V=\"908,201279,9085721000\":V=\"908,OTHER,9086993000\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase272() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 272	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test272".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=num:"
						+ "NODE=\"CNT10=03,AC,SD,TE\":CNT11=06:V=\"201,201224,2012780000\":"
						+ "V=\"201,OTHER,2012791000\":V=\"609,609224,6092261000\":"
						+ "V=\"609,OTHER,6092263000\":V=\"908,908699,9085721000\":V=\"908,OTHER,9086993000\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase273() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 273	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test273".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=num:"
						+ "NODE=\"CNT10=03,AC,SD,TE\":CNT11=07:V=\"201,*SIX201,2012783000\":"
						+ "V=\"201,OTHER,2012791000\":V=\"609,609224,6092261000\":"
						+ "V=\"609,OTHER,6092263000\":V=\"908,908699,9085721000\":"
						+ "V=\"908,*SIX9082,9085721000\":V=\"908,OTHER,9086993000\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase274() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 274	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test274".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=num:"
						+ "NODE=\"CNT10=03,AC,SD,TE\":CNT11=06:V=\"215,215843,2012783000\":"
						+ "V=\"215,OTHER,2012791000\":V=\"609,609224,6092261000\":"
						+ "V=\"609,OTHER,6092263000\":V=\"908,908699,9085721000\":V=\"908,OTHER,9086993000\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase275() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 275	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test275".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=num,ED=ed,ET=et:"
						+ "CNT12=04:TYPE=SD,LBL=\"*SIX201\",DEF=\"CNT13=012,201224,201226,201228,201316,201503,201318,201259,201261,201262,201263,201265,201266\":"
						+ "TYPE=SD,LBL=\"*SIX609\",DEF=\"CNT13=006,609226,609365,609228,609452,609368,609232\":"
						+ "TYPE=SD,LBL=\"*SIX908\",DEF=\"CNT13=006,908699,908572,908755,908819,908932,908846\":"
						+ "TYPE=SD,LBL=\"*DUP609\",DEF=\"CNT13=003,609226,609455,609456\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase276() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 276	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test276".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=C,NUM=num,ED=ed,ET=et:CNT12=04:"
						+ "TYPE=SD,LBL=\"*SIX201\",DEF=\"CNT13=012,201224,201226,201228,201316,201503,201318,201259,201261,201262,201263,201265,201266\":"
						+ "TYPE=SD,LBL=\"*SIX609\",DEF=\"CNT13=006,609226,609365,609228,609452,609368,609232\":"
						+ "TYPE=SD,LBL=\"*SIX908\",DEF=\"CNT13=006,908699,908572,908755,908819,908932,908846\":"
						+ "TYPE=SD,LBL=\"*DUP609\",DEF=\"CNT13=003,609226, 609455,609456\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase277() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 277	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test277".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=MGI01,AC=N,NUM=\"DIAL#\",ED=\"NOW\","
						+ "SO=N2323441,NOTE=\"BASIC RECORD\":IEC=\"CNT1=01,288\":IAC=\"CNT2=01,OTC\":"
						+ "ALBL=\"CNT3=01,AOSMGI1\":CNT8=01:LN=\"TELCORDIA\":CNT9=002:"
						+ "TEL=\"DIAL#\",LNS=800:TEL=\"POTS number\",LNS=800,LSO=NPANXX;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "MGIXXX01" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase278() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 278	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test278".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=MGI01,AC=N,NUM=\"DIAL#\",ED=\"NOW\","
						+ "SO=N2323441,NOTE=\"BASIC RECORD\":IEC=\"CNT1=01,288\":IAC=\"CNT2=01,OTC\":"
						+ "ALBL=\"CNT3=01,AOSMGI1\":CNT8=01:LN=\"TELCORDIA\":CNT9=002:"
						+ "TEL=\"DIAL#\",LNS=800:TEL=\"POTS number\",LNS=800,LSO=NPANXX;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "MGIXXX01" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase279() {
		// no description
	}

	public void TestCase280() {
		// no description
	}

	public void TestCase281() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 281	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test281".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=MGI01,AC=N,NUM=num,ED=\"NOW\":"
						+ "IEC=\"CNT1=01,iec\":IAC=\"CNT2=01,iac\":ABN=abn,DAU=N,DAT=N,DD=dd,HDD=N,LIBL,RAO=000,"
						+ "SO=so,TELCO=telco,LA=la,NCON=ncon,CTEL=ctel:ANET=\"CNT6=01,anet\":CNT8=01:LN=\"LISTING NAME\":CNT9=002:"
						+ "TEL=\"DIAL#\",LNS=333:TEL=\"POTS number\",LNS=200,LSO=NPANXX;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase282() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 282	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test282".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=ro,AC=N,NUM=num,ED=\"NOW\":"
						+ "IEC=\"CNT1=01,iec\":IAC=\"CNT2=01,iac\":ABN=abn,DAU=N,DAT=N,DD=dd,HDD=N,LIBL,RAO=000,SO=so,TELCO=telco,LA=la,NCON=ncon,CTEL=ctel:"
						+ "ANET=\"CNT6=01,anet\":CNT8=01:LN=\"LISTING NAME\":CNT9=003:"
						+ "TEL=\"DIAL#\",LNS=333:TEL=\"POTS number1\",LNS=200,LSO=NPANXX:TEL=\"POTS number2\",LNS=400,LSO=NPANXX;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	// Query CR
	public void TestCase283() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 283	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test283".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRQ:,%s,%s:::::ID=%s,RO=ro,NUM=num,ED=ed,ET=et;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase284() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 284	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test284".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRQ:,%s,%s:::::ID=%s,RO=ro,NUM=num,ED=ed,ET=et;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	public void TestCase285() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 285	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test285".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRQ:,%s,%s:::::ID=%s,RO=ro,NUM=num,ED=ed,ET=et;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");
	}

	// MULTI-CARRIER VIEW (REQ-CRV)
	public void TestCase286() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 286	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test286".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRV:,%s,%s:::::ID=%s,RO=ro,NUM=num,ED=ed,ET=et;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRV");
	}

	public void TestCase287() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 287	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test287".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRV:,%s,%s:::::ID=%s,RO=ro,NUM=num,RSTAT=rstat;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRV");
	}

	public void TestCase288() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 288	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test288".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRV:,%s,%s:::::ID=%s,RO=ro,NUM=num,ED=ed,ET=et;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRV");
	}

	public void TestCase289() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 289	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test289".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRV:,%s,%s:::::ID=%s,RO=ro,NUM=num,ED=ed,ET=et;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRV");
	}

	public void TestCase290() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 290	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test290".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRV:,%s,%s:::::ID=%s,RO=ro,NUM=num,RSTAT=04;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRV");
	}

	public void TestCase291() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 291	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test291".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRV:,%s,%s:::::ID=%s,RO=ro,NUM=num,ED=ed,ET=et;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRV");
	}

	public void TestCase292() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 292	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test292".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRV:,%s,%s:::::ID=%s,RO=ro,NUM=num,ED=ed,ET=et;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRV");
	}

	public void TestCase293() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 293	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test293".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRV:,%s,%s:::::ID=%s,RO=ro,NUM=num,ED=ed,ET=et;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRV");
	}

	public void TestCase294() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 294	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test294".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRV:,%s,%s:::::ID=%s,RO=ro,NUM=num,ED=ed,ET=et;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRV");
	}

	public void TestCase295() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 295	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test295".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRV:,%s,%s:::::ID=%s,RO=ro,NUM=num,ED=ed,ET=et;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRV");
	}

	public void TestCase296() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 296	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test296".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRV:,%s,%s:::::ID=%s,RO=ro,NUM=num,ED=ed,ET=et;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRV");
	}

	public void TestCase297() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 297	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test297".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRV:,%s,%s:::::ID=%s,RO=ro,NUM=num,ED=ed,ET=et;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRV");
	}

	public void TestCase298() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 298	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test298".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRV:,%s,%s:::::ID=%s,RO=ro,NUM=num,ED=ed,ET=et;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRV");
	}

	public void TestCase299() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 299	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test299".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRV:,%s,%s:::::ID=%s,RO=ro,NUM=num,ED=ed,ET=et;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRV");
	}

	public void TestCase300() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 300	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test300".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRV:,%s,%s:::::ID=%s,RO=ro,NUM=num,ED=ed,ET=et;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRV");
	}

	public void TestCase301() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 301	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test301".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRV:,%s,%s:::::ID=%s,RO=ro,NUM=num,ED=ed,ET=et;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRV");
	}

	public void TestCase302() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 302	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test302".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRV:,%s,%s:::::ID=%s,RO=ro,NUM=num,ED=ed,ET=et;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRV");
	}

	public void TestCase303() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 303	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test303".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRV:,%s,%s:::::ID=%s,RO=ro,NUM=num,ED=ed,ET=et,SIZE=Y;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRV");
	}

	public void TestCase304() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 304	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test304".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRV:,%s,%s:::::ID=%s,RO=ro,NUM=num,ED=ed,ET=et,SIZE=Y;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRV");
	}

	public void TestCase305() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 305	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test305".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRV:,%s,%s:::::ID=%s,RO=ro,NUM=num,ED=ed,ET=et,SIZE=Y;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRV");
	}

	public void TestCase306() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 306	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test306".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRV:,%s,%s:::::ID=%s,RO=ro,NUM=num,ED=ed,ET=et;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRV");
	}

	public void TestCase307() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 307	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test307".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRV:,%s,%s:::::ID=%s,RO=ro,NUM=num,ED=ed,ET=et;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRV");
	}

	public void TestCase308() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 308	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test308".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRV:,%s,%s:::::ID=%s,RO=ro,NUM=num,ED=ed,ET=et;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRV");
	}

	public void TestCase309() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 309	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test309".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRV:,%s,%s:::::ID=%s,RO=ro,NUM=num,RSTAT=rstat;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRV");
	}

	public void TestCase310() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 310	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test310".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRV:,%s,%s:::::ID=%s,RO=ro,NUM=num,ED=ed,ET=et;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRV");
	}

	public void TestCase311() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 311	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test311".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRV:,%s,%s:::::ID=%s,RO=ro,NUM=num;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRV");
	}

	public void TestCase312() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 312	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test312".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRV:,%s,%s:::::ID=%s,RO=ro,NUM=num,RSTAT=rstat;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRV");
	}

	public void TestCase313() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 313	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test313".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRV:,%s,%s:::::ID=%s,RO=ro,NUM=num,RSTAT=02;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRV");
	}

	public void TestCase314() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 314	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test314".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRV:,%s,%s:::::ID=%s,RO=ro,NUM=num,ED=ed,ET=et,RSTAT=rstat;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRV");
	}

	public void TestCase315() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 315	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test315".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRV:,%s,%s:::::RO=ro,NUM=num,EFF=\"04/20/96\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRV");
	}

	public void TestCase316() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 316	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test316".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRV:,%s,%s:::::ID=%s,RTO=rto,NUM=num;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRV");
	}

	public void TestCase317() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 317	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test317".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRV:,%s,%s:::::ID=%s,RTO=rto,NUM=num;", "14/34/62",
				YlUtil.timeToString(new Date()), "12345678900");
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRV");
	}

	public void TestCase318() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 318	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test318".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRV:,%s,%s:::::ID=%s,RO=ro,NUM=num,ED=ed,ET=et;",
				YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900");
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRV");
	}

	public void TestCase319() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 319	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test319".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-CRV:,%s,%s:::::ID=%s,RO=ro,NUM=num,ED=ed,ET=et;",
				YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900");
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRV");
	}

	public void TestCase320() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 320	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test320".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-SCP:,%s,%s:::::ID=%s,AC=Q,NUM=num,ED=ed,ET=et,CRITICAL=Y:CNT=01:SCP=scp;",
				YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900");
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-SCP");
	}

	public void TestCase321() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 321	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test321".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-SCP:,%s,%s:::::ID=%s,AC=R,NUM=num,ED=ed,ET=et,CRITICAL=Y:CNT=01:SCP=scp;",
				YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900");
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-SCP");
	}

	public void TestCase322() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 322	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test322".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-SCP:,%s,%s:::::ID=%s,AC=D,NUM=num,ED=ed,ET=et,CRITICAL=Y:CNT=01:SCP=scp;",
				YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900");
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-SCP");
	}

	public void TestCase323() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 323	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test323".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-SCP:,%s,%s:::::ID=%s,RO=ro,AC=R,NUM=num,ED=ed,ET=et,CRITICAL=Y:CNT=01:SCP=scp;",
				YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900");
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-SCP");
	}

	public void TestCase324() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 324	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test324".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-SCP:,%s,%s:::::ID=%s,RO=ro,AC=R,NUM=num,CRITICAL=Y:CNT=01:SCP=scp;",
				YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900");
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-SCP");
	}

	public void TestCase325() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 325	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test325".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-SCP:,%s,%s:::::ID=%s,RO=ro,AC=A,NUM=num,CRITICAL=Y:CNT=01:SCP=scp;",
				YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900");
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-SCP");
	}

	public void TestCase326() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 326	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test326".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-SCP:,%s,%s:::::ID=%s,RO=ro,AC=A,NUM=num,CRITICAL=Y:CNT=01:SCP=scp;",
				YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900");
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-SCP");
	}

	public void TestCase327() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 327	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test327".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-TRN:,%s,%s:::::ID=%s,RO=ro,ROTRN=rotrn;", YlUtil.dateToString(new Date()),
				YlUtil.timeToString(new Date()), "12345678900");
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-TRN");
	}

	public void TestCase328() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 328	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test328".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-TRN:,%s,%s:::::ID=%s,RO=ro:QT=01:NUM=num;",
				YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900");
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-TRN");
	}

	public void TestCase329() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 329	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test329".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-TRN:,%s,%s:::::ID=%s,RO=ro:QT=01:NUM=num;",
				YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900");
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-TRN");
	}

	public void TestCase330() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 330	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test330".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-TRN:,%s,%s:::::ID=%s,RO=ro:QT=10:NUM=num1,NUM=num2,NUM=num3,"
						+ "NUM=num4,NUM=num5,NUM=num6,NUM=num7,NUM=num8,NUM=num9,NUM=num10;",
				YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900");
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-TRN");
	}

	public void TestCase331() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 331	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test331".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-TRN:,%s,%s:::::ID=%s,RO=ro:QT=08:NUM=num1,NUM=num2,NUM=num3,"
						+ "NUM=num4,NUM=num5,NUM=num6,NUM=num7,NUM=num8;",
				YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900");
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-TRN");
	}

	public void TestCase332() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 332	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test332".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-TRN:,%s,%s:::::ID=%s,RO=ro:QT=08:NUM=num1,NUM=num2,NUM=num3," + "NUM=num4,NUM=num5;",
				YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900");
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-TRN");
	}

	public void TestCase333() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 333	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test333".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-TRN:,%s,%s:::::ID=%s,RO=ro:QT=10:NUM=num1,NUM=num2,NUM=num3,"
						+ "NUM=num4,NUM=num5,NUM=num6,NUM=num7,NUM=num8,NUM=num9,NUM=num10;",
				YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900");
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-TRN");
	}

	public void TestCase334() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 334	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test334".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-TRN:,%s,%s:::::ID=%s,RO=ro:QT=10:NUM=num1,NUM=num2,NUM=num3,"
						+ "NUM=num4,NUM=num5,NUM=num6,NUM=num7,NUM=num8,NUM=num9,NUM=num10;",
				YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900");
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-TRN");
	}

	public void TestCase335() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 335	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test335".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-TRN:,%s,%s:::::ID=%s,RO=ro:QT=10:NUM=num1,NUM=num2,NUM=num3,"
						+ "NUM=num4,NUM=num5,NUM=num6,NUM=num7,NUM=num8,NUM=num9,NUM=num10;",
				YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900");
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-TRN");
	}

	public void TestCase336() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 336	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test336".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-TRN:,%s,%s:::::ID=%s,RO=ro,ROTRN=rotrn;", YlUtil.dateToString(new Date()),
				YlUtil.timeToString(new Date()), "12345678900");
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-TRN");
	}

	public void TestCase337() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 337	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test337".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-TRN:,%s,%s:::::ID=%s,RO=ro,ROTRN=rotrn;", YlUtil.dateToString(new Date()),
				YlUtil.timeToString(new Date()), "12345678900");
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-TRN");
	}

	public void TestCase338() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 338	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test338".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-TRN:,%s,%s:::::ID=%s,RO=ro:QT=10:NUM=num1,NUM=num2,NUM=num3,"
						+ "NUM=num4,NUM=num5,NUM=num6,NUM=num7,NUM=num8,NUM=num9,NUM=num10;",
				YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900");
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-TRN");
	}

	public void TestCase339() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 339	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test339".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-TRN:,%s,%s:::::ID=%s,RO=ro:QT=11:NUM=num1,NUM=num2,NUM=num3,"
						+ "NUM=num4,NUM=num5,NUM=num6,NUM=num7,NUM=num8,NUM=num9,NUM=num10,NUM=num11;",
				YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900");
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-TRN");
	}

	public void TestCase340() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 340	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test340".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-TRN:,%s,%s:::::ID=%s,RO=ro,ROTRN=rotrn:QT=01:NUM=num1;",
				YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900");
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-TRN");
	}

	public void TestCase341() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 341	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test341".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-TRN:,%s,%s:::::ID=%s,RO=ro,NUM=num1;", YlUtil.dateToString(new Date()),
				YlUtil.timeToString(new Date()), "12345678900");
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-TRN");
	}

	public void TestCase342() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 342	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test342".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-TRN:,%s,%s:::::ID=%s,RO=ro:QT=02:NUM=num1,NUM=num2,NUM=num3;",
				YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900");
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-TRN");
	}

	public void TestCase343() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 343	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test343".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-TRN:,%s,%s:::::ID=%s,RO=ro,ROTRN=rotrn: NUM=num1,NUM=num2,NUM=num3;",
				YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900");
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-TRN");
	}

	public void TestCase344() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 344	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test344".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format("REQ-TRN:,%s,%s:::::ID=%s,RO=ro,QT=01;", YlUtil.dateToString(new Date()),
				YlUtil.timeToString(new Date()), "12345678900");
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-TRN");
	}

	public void TestCase345() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 345	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test345".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-TRN:,%s,%s:::::ID=%s,RO=ro:QT=11:" + "NUM=num1,NUM=num2,NUM=num3,NUM=num4,NUM=num5,"
						+ "NUM=num6,NUM=num7,NUM=num8,NUM=num9,NUM=num10;",
				YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900");
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-TRN");
	}

	public void TestCase346() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 346	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test346".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-TRN:,%s,%s:::::ID=%s,RO=ro:QT=00:" + "NUM=num1,NUM=num2,NUM=num3,NUM=num4,NUM=num5,"
						+ "NUM=num6,NUM=num7,NUM=num8,NUM=num9,NUM=num10;",
				YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900");
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-TRN");
	}

	public void TestCase347() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 347	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test347".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-TRN:,%s,%s:::::ID=%s,RO=ro:QT=10:" + "NUM=num1,NUM=num2,NUM=num3,NUM=num4,NUM=num5,"
						+ "NUM=num6,NUM=num7,NUM=num8,NUM=num9,NUM=num10;",
				YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "12345678900");
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-TRN");
	}

	public void TestCase348() {
		// TestCaseNumber = 0;
		// System.out.println("----Start Test Case 348 ");
		//
		// YlDcmMsg obj = new YlDcmMsg();
		// obj.tliHdr.version.setValue(1L);
		// obj.tliHdr.priority.setValue(0L);
		// obj.tliHdr.destNodeName.setValue("test348".getBytes());
		// obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		// obj.tliHdr.errorCode.setValue(0L);
		// String dataAppl =
		// String.format("REQ-TRN:,%s,%s:::::ID=%s,RO=ro:QT=10:"
		// + "NUM=num1,NUM=num2,NUM=num3,NUM=num4,NUM=num5,"
		// + "NUM=num6,NUM=num7,NUM=num8,NUM=num9,NUM=num10;",
		// YlUtil.dateToString(new Date()), YlUtil.timeToString(new
		// Date()),"12345678900");
		// obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		// obj.tliHdr.messageCode.setValue(19L);
		// obj.dataAppl.setValue(dataAppl.getBytes());
		// this.f.channel().writeAndFlush(obj);
		// System.out.println("----write asn.1 message:REQ-TRN");
	}
	public void TestCase349() {
	}
	public void TestCase350() {
	}
	public void TestCase351() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 351	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test351".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = "RTRV-ASI::::::;";
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:RTRV-ASI");
	}
	public void TestCase352() {
		
	}
	public void TestCase353() {
		
	}
	public void TestCase354() {
		
	}
	public void TestCase355() {
		
	}
	public void TestCase356() {
		
	}
	public void TestCase357() {
		
	}
	public void TestCase358() {
		
	}
	public void TestCase359() {
		
	}
	public void TestCase360() {
		
	}
	public void TestCase361() {
		
	}
	public void TestCase362() {
		
	}
	public void TestCase363() {
		
	}
	public void TestCase364() {
		
	}
	public void TestCase365() {
		
	}
	public void TestCase366() {
		
	}
	public void TestCase367() {
		
	}
	public void TestCase368() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 368	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test368".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-CRC:,%s,%s:::::ID=%s,RO=MGI01,AC=C,NUM=8008001234,ED=\"Pending Date\":"
				+ "V=\"NY+NJ,ON1,224,201,201555,2015551212,01/01-03/01, OTHER,,,0866,POTS number 7鈥�;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "MGIXXX01" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");	
		}
	public void TestCase369() {
		
	}
	public void TestCase370() {
		
	}
	public void TestCase371() {
		
	}
	public void TestCase372() {
		
	}
	public void TestCase373() {
		
	}
	public void TestCase374() {
		
	}
	public void TestCase375() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 375	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test375".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-DAP:,%s,%s:::::ID=%s,RO=MGI01,NUM=8008001234,ED=\"effective date\",ET=\"effective time\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "MGIXXX01" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");	
		
	}
	public void TestCase376() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 376	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test376".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-DAP:,%s,%s:::::ID=%s,RO=MGI01,NUM=8008001234,ED=\"effective date\",ET=\"effective time\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "MGIXXX01" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");	
		}
	public void TestCase377() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 377	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test377".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-DAP:,%s,%s:::::ID=%s,RO=MGI01,NUM=8008001234;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "MGIXXX01" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");	
		}
	public void TestCase378() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 378	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test378".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-DAP:,%s,%s:::::ID=%s,RO=MGI01,NUM=8008001234;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "MGIXXX01" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");	
		}
	public void TestCase379() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 379	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test379".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-DAP:,%s,%s:::::ID=%s,RO=MGI01,NUM=8008001234,ED=\"effective date\",ET=\"effective time\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "MGIXXX01" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");	
		}
	public void TestCase380() {
		TestCaseNumber = 0;
		System.out.println("----Start Test Case 380	");

		YlDcmMsg obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test380".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		String dataAppl = String.format(
				"REQ-DAP:,%s,%s:::::ID=%s,RO=MGI01,NUM=8008001234,ED=\"effective date\",ET=\"effective time\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "MGIXXX01" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");	
		
		obj = new YlDcmMsg();
		obj.tliHdr.version.setValue(1L);
		obj.tliHdr.priority.setValue(0L);
		obj.tliHdr.destNodeName.setValue("test380".getBytes());
		obj.tliHdr.srcNodeName.setValue(srcNodeName.getBytes());
		obj.tliHdr.errorCode.setValue(0L);
		dataAppl = String.format(
				"REQ-DAP:,%s,%s:::::ID=%s,RO=MGI01,NUM=8008001235,ED=\"effective date\",ET=\"effective time\";",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), "MGIXXX01" });
		obj.tliHdr.messageId.setValue(Function.randomUUID10().toUpperCase().getBytes());
		obj.tliHdr.messageCode.setValue(19L);
		obj.dataAppl.setValue(dataAppl.getBytes());
		this.f.channel().writeAndFlush(obj);
		System.out.println("----write asn.1 message:REQ-CRC");	
		
		}
	public void TestCase481() {
		String sNo = "481";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-GSL:,%s,%s:::::ID=%s,RO=%s,CREN=cren;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CGL");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);

		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-GSL");
	}
	public void TestCase482() {
		String sNo = "482";
		System.out.println("----Start Test Case "+sNo);

		String upl_data = String.format("REQ-GSL:,%s,%s:::::ID=%s,RO=%s,CREN=cren;",
				new Object[] { YlUtil.dateToString(new Date()), YlUtil.timeToString(new Date()), req_nsr_message_id,req_nsr_message_ro });
		YlDcmMsg obj = NewUplMessage(upl_data,"CGL");
		Asn1Decode.printYlDcmMsg(obj);
		this.f.channel().writeAndFlush(obj);

		obj = NewGDMessage(false);
		this.f.channel().writeAndFlush(obj);
		
		System.out.println("----write asn.1 message:REQ-GSL");
	}
}