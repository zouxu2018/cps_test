package com.cpsserver;

import com.turkcelltech.jac.ASN1Integer;
import com.turkcelltech.jac.OctetString;
import com.turkcelltech.jac.Sequence;
import java.io.PrintStream;

public class YlTliHdr extends Sequence
  implements Cloneable
{
  public ASN1Integer version = new ASN1Integer("version");
  public ASN1Integer priority = new ASN1Integer("priority");
  public OctetString messageId = new OctetString("messageId");
  public OctetString destNodeName = new OctetString("destNodeName");
  public OctetString srcNodeName = new OctetString("srcNodeName");
  public ErrorCode errorCode = new ErrorCode("errorCode");
  public ASN1Integer messageCode = new ASN1Integer("messageCode");

  public YlTliHdr()
  {
    setUpElements();
  }

  public YlTliHdr(String name)
  {
    super(name);
    setUpElements();
  }

  protected void setUpElements()
  {
    super.addElement(this.version);
    super.addElement(this.priority);
    super.addElement(this.messageId);
    super.addElement(this.destNodeName);
    super.addElement(this.srcNodeName);
    super.addElement(this.errorCode);
    super.addElement(this.messageCode);
  }

  public Object clone()
  {
    Object o = null;
    try
    {
      o = (YlTliHdr)super.clone();
    }
    catch (CloneNotSupportedException e)
    {
      System.out.println(e.toString());
    }
    return o;
  }
}