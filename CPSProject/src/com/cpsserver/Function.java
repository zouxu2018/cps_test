package com.cpsserver;

import java.util.Random;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Function {
	public static String[] MySplit(String ss, String cDelimiter, String include) {
		String[] s1 = ss.split(include);
		String ss_new = "";
		for (int ii = 0; ii < s1.length; ii++) {
			if ((ii % 2) == 1)
				ss_new += s1[ii].replaceAll(cDelimiter, "##########") + include;
			else
				ss_new += s1[ii] + include;
		}
		// handle end include
		if (ss.equals("") || !ss.substring(ss.length() - 1).equals(include))
			ss_new = ss_new.substring(0, ss_new.length() - 1);
		s1 = ss_new.split(cDelimiter);
		for (int ii = 0; ii < s1.length; ii++) {
			s1[ii] = s1[ii].replaceAll("##########", cDelimiter);
		}
		return s1;

	}

	public static ErrClass isDatetime(String sDate, String sTime) {
		ErrClass errClass = null;
		Pattern p = Pattern.compile(
				"^((\\d{2}(([02468][048])|([13579][26]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|"
						+ "([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])))))|"
						+ "(\\d{2}(([02468][1235679])|([13579][01345789]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|"
						+ "(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|(1[0-9])|(2[0-8]))))))"
						+ "(\\s(((0?[0-9])|([1-2][0-3]))\\:([0-5]?[0-9])((\\s)|(\\:([0-5]?[0-9])))))?$");
		if (!p.matcher(sDate).matches()) {
			errClass = new ErrClass();
			errClass.setERR("98");
			errClass.setVERR(String.format("\"%s,%s\"", sDate, sTime));
		}
		return errClass;
	}
	public static boolean isNumeric(String str) {
		Pattern pattern = Pattern.compile("[0-9]*");
		Matcher isNum = pattern.matcher(str);
		if (!isNum.matches()) {
			return false;
		}
		return true;
	}
	public static String randomUUID10() {  
        String[] chars = new String[] { "a", "b", "c", "d", "e", "f",  
                "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s",  
                "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5",  
                "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I",  
                "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",  
                "W", "X", "Y", "Z" };  
        Random r = new Random();
        StringBuffer shortBuffer = new StringBuffer();  
        String uuid = UUID.randomUUID().toString().replace("-", "");  
        for (int i = 0; i < 7; i++) {  
            String str = uuid.substring(i * 4, i * 4 + 4);  
            int x = Integer.parseInt(str, 16);  
            shortBuffer.append(chars[x % 0x3E]);  
        }  
        return shortBuffer.toString()+(r.nextInt(899)+100);  
    } 

}
