package com.cpsserver;

import com.chaosinmotion.asn1.BerInputStream;
import com.chaosinmotion.asn1.BerOutputStream;
import com.turkcelltech.jac.ASN1Integer;
import com.turkcelltech.jac.OctetString;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.CharsetUtil;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServerHandler extends SimpleChannelInboundHandler<ByteBuf>
{
  private static final Logger logger = Logger.getLogger(Server.class.getName());

  public void channelActive(ChannelHandlerContext ctx) throws Exception
  {
    ByteBufAllocator alloc1 = ctx.alloc();
    ctx.write(Unpooled.copiedBuffer("GD", CharsetUtil.UTF_8));
    System.out.println("GD!");

    MsgEncode();
  }

  public static void MsgEncode()
  {
    try {
      ByteArrayOutputStream outStream = new ByteArrayOutputStream();
      BerOutputStream out = new BerOutputStream(outStream);

      YlDcmMsg msg = new YlDcmMsg();

      msg.tliHdr.version.setValue(1L);
      msg.tliHdr.priority.setValue(0L);
      msg.tliHdr.messageId.setValue("ABCDEFG".getBytes());
      msg.tliHdr.messageCode.setValue(1L);
      msg.tliHdr.destNodeName.setValue(null);
      msg.tliHdr.srcNodeName.setValue(null);

      msg.tliHdr.errorCode.setValue(0L);

      msg.dataAppl.setValue(new byte[] { 1, 2, 3 });

      msg.encode(out);

      printYlDcmMsg(msg);
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }

  public void channelRead0(ChannelHandlerContext ctx, ByteBuf msg)
    throws Exception
  {
    byte[] req = new byte[msg.readableBytes()];
    msg.readBytes(req);
    DecodingAsn1(new ByteArrayInputStream(req));
    msg.clear();
  }

  public void channelReadComplete(ChannelHandlerContext ctx)
    throws Exception
  {
  }

  public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception
  {
    cause.printStackTrace();
    ctx.close();
  }

  public void DecodingAsn1(ByteArrayInputStream inputStream)
  {
    try {
      ByteArrayOutputStream outStream = new ByteArrayOutputStream();

      byte[] buffer = new byte[1024];

      for (int i = 0; i < 4; i++) {
        if (inputStream.available() >= 0) {
          int tmp = inputStream.read();

          if (tmp == 126)
            continue;
          System.out.println("invalid delimiter");
          return;
        }

        System.out.println("empty message");
        return;
      }

      System.out.println("valid delimiter");
      int MessageLen;
      if (inputStream.available() >= 0) {
        MessageLen = inputStream.read();
      }
      else
      {
        System.out.println("no length");
        return;
      }
      int len;
      while ((len = inputStream.read(buffer)) > -1)
      {
        outStream.write(buffer, 0, len);
      }
      

      outStream.flush();

      BerInputStream in = new BerInputStream(new ByteArrayInputStream(outStream.toByteArray()));
      printHex(outStream.toByteArray());
      try {
        YlDcmMsg msg = new YlDcmMsg();
        msg.decode(in);
        printYlDcmMsg(msg);
      } catch (Exception e2) {
        System.out.println("decode error:");
        logger.log(Level.SEVERE, null, e2.toString());
      }

    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
  }

  public void printHex(byte[] coded) {
    System.out.println("Hex :");
    String hexDigits = "0123456789ABCDEF";
    for (int i = 0; i < coded.length; i++) {
      int c = coded[i];

      if (c < 0)
        c += 256;
      int hex1 = c & 0xF;
      int hex2 = c >> 4;
      System.out.print(hexDigits.substring(hex2, hex2 + 1));
      System.out.print(hexDigits.substring(hex1, hex1 + 1) + " ");
    }
    System.out.println();
  }

  private static void printYlDcmMsg(YlDcmMsg msg) {
    System.out.println("YlDcmMsg=>");
    System.out.println("tliHdr.version=>" + msg.tliHdr.version.toString());
    System.out.println("tliHdr.priority=>" + msg.tliHdr.priority.toString());
    System.out.println("tliHdr.messageId=>" + msg.tliHdr.messageId.toString());
    System.out.println("tliHdr.messageCode=>" + msg.tliHdr.messageCode.toString());
    System.out.println("tliHdr.destNodeName=>" + msg.tliHdr.destNodeName.toString());
    System.out.println("tliHdr.srcNodeName=>" + msg.tliHdr.srcNodeName.toString());
    System.out.println("tliHdr.errorCode=>" + msg.tliHdr.errorCode.toString());
  }
}